<?php
/**
 * @package Zmsadmin
 * @copyright BerlinOnline GmbH
 **/

namespace BO\Zmsadmin;

use BO\Slim\Request;
use BO\Zmsclient\Auth;
use BO\Zmsclient\Exception as ClientException;
use BO\Zmsentities\Exception\QuickLoginFailed;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class QuickLogin extends BaseController
{
    /**
     * @SuppressWarnings(Param)
     *
     * @param RequestInterface|Request $request
     * @param ResponseInterface $response
     * @param array $args
     *
     * @return ResponseInterface
     * @throws QuickLoginFailed
     */
    public function readResponse(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        $loginData = Helper\LoginForm::fromQuickLogin();
        if ($loginData->hasFailed()) {
            throw new QuickLoginFailed();
        }
        $loginData = $loginData->getStatus();
        $userAccount = new \BO\Zmsentities\Useraccount(array(
            'id' => $loginData['loginName']['value'],
            'password' => $loginData['password']['value'],
            'departments' => [0 => ['id' => 0]] // required in schema validation
        ));

        try {
            $workstation = \App::$http
                ->readPostResult('/workstation/login/', $userAccount)->getEntity();
            if ($workstation->offsetExists('authkey')) {
                Auth::setKey($workstation['authkey']);
            }
        } catch (ClientException $exception) {
            //ignore double login exception on quick login
            if ($exception->template == 'BO\Zmsapi\Exception\Useraccount\UserAlreadyLoggedIn'
             && isset($exception->data['authkey'])
            ) {
                Auth::setKey($exception->data['authkey']);
                $workstation = \App::$http->readGetResult('/workstation/')->getEntity();
                $workstation->authkey = $exception->data['authkey'];
            } else {
                throw $exception;
            }
        }

        $workstation->scope = new \BO\Zmsentities\Scope(array('id' => $loginData['scope']['value']));
        $workstation->hint = $loginData['hint']['value'] ?? '';
        $workstation->name = $loginData['workstation']['value'] ?? '';

        \App::$http->readPostResult('/workstation/', $workstation)->getEntity();

        $basePath = $request->getBasePath();

        return $response->withRedirect($basePath .'/'. trim($loginData['redirectUrl']['value'], "/"));
    }
}
