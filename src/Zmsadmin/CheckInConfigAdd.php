<?php

declare(strict_types=1);

namespace BO\Zmsadmin;

use App;
use BO\Mellon\Validator;
use BO\Slim\Render;
use BO\Zmsclient\Exception;
use BO\Zmsentities\CheckInConfig;
use BO\Zmsentities\Config;
use BO\Zmsentities\Schema\Entity;
use Fig\Http\Message\StatusCodeInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Slim\Exception\HttpNotFoundException;

/**
 * @suppressWarnings(complexity)
 */
class CheckInConfigAdd extends BaseController
{
    protected $postUrl = '/checkinconfig/';

    public function readResponse(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        if (!App::getConfigService()->isCheckInEnabled()) {
            throw new HttpNotFoundException($request);
        }

        /** @var Workstation $workstation */
        $workstation = \App::$http->readGetResult('/workstation/', ['resolveReferences' => 1])->getEntity();
        $workstation->getUseraccount()->testRights(['scope']);

        $scopeId = Validator::value($workstation['scope']['id'] ?? null)->isNumber()->getValue();
        if (!$scopeId) {
            return Render::redirect('workstationSelect', ['error' => 'scope_missing']);
        }

        $exception = [];
        $entity    = $this->readEntityByArgs($args);
        if (!$entity) {
            throw new HttpNotFoundException($request);
        }

        if ($request->getMethod() === 'POST') {
            try {
                $entity = $this->processInput($request);

                return $this->getSuccessResponse($entity);
            } catch (Exception $clientException) {
                if ($clientException->getCode() === StatusCodeInterface::STATUS_NOT_ACCEPTABLE
                    || $clientException->getCode() === StatusCodeInterface::STATUS_BAD_REQUEST
                ) {
                    $entity = new CheckInConfig(
                        $this->parseInput($request->getParsedBody()) ?? $this->readEntityByArgs($args)->getArrayCopy()
                    );
                    $exception = [
                        'template' => Helper\TwigExceptionHandler::getExceptionTemplate($clientException),
                        'data' => $clientException->data,
                        //'include' => true,
                    ];
                } else {
                    throw $clientException;
                }
            }
        }

        /** @var Config $configuration */
        $department = App::$http->readGetResult(
            '/scope/' . $workstation->scope['id'] . '/department/',
            ['resolveReferences' => 2]
        )->getEntity();
        $scopes = $department->getScopeList();
        $languageCodes = explode(',', "" . App::$config->getEntity()->getPreference('checkin', 'languages'));

        return Render::withHtml(
            $response,
            'page/checkInConfigEdit.twig',
            [
                'title'      => 'Check-In-Schalter ersteller',
                'workstation' => $workstation,
                'edit'       => 0,
                'department' => $department,
                'scopes'     => $scopes,
                'languages'  => $languageCodes,
                'menuActive' => 'checkinconfig',
                'entity'     => $entity,
                'exception'  => $exception,
            ]
        );
    }

    /**
     * @SuppressWarnings(Param)
     */
    protected function readEntityByArgs(array $args): ?CheckInConfig
    {
        return new CheckInConfig();
    }

    protected function processInput(RequestInterface $request): Entity
    {
        $input = $this->parseInput($request->getParsedBody());

        $numberOrNull = function ($mixedValue): ?int {
            return is_numeric($mixedValue ?? '') ? (int) $mixedValue : null;
        };

        $entity = (new CheckInConfig($input))->withCleanedUpFormData();
        $entity['tooEarlyMinutes'] = $numberOrNull($input['tooEarlyMinutes']);
        $entity['toleranceMinutes'] = $numberOrNull($input['toleranceMinutes']);
        $entity['lastChange'] = '';

        return App::$http->readPostResult($this->postUrl, $entity)->getEntity();
    }

    protected function parseInput(?array $input): ?array
    {
        if (is_null($input)) {
            return null;
        }

        $idFilter = function (array $arrayWithSomeIds): array {
            return array_filter($arrayWithSomeIds, function ($value) {
                return is_numeric($value) && $value !== '0' && $value !== 0;
            });
        };
        $filterEmptyMembers = function (array $arrayWithStrings): array {
            return array_filter($arrayWithStrings, function ($value) {
                return !empty($value);
            });
        };

        $input['scopeIds'] = is_array($input['scopeIds'] ?? null) ? $idFilter($input['scopeIds']) : [];
        $input['scopeIds'] = count($input['scopeIds']) ? ':' . implode(':', $input['scopeIds']) . ':' : '';

        $languages = [];
        foreach ($input as $key => $value) {
            if (str_starts_with($key, 'msg') && is_array($value)) {
                $value = $filterEmptyMembers($value);
                $input[$key] = empty($value) ? '' : json_encode($value);
                $languages = array_merge($languages, array_keys($value));
            }
        }
        $input['languages'] = implode(',', array_unique($languages));

        return $input;
    }

    protected function getSuccessResponse(Entity $entity): ResponseInterface
    {
        $classParts = explode('\\', get_class($this));
        $msgBlock = strtolower(preg_replace('/(?<!^)[A-Z]/', '_$0', end($classParts)));

        return Render::redirect('checkInConfigCreated', ['id' => $entity->getId()], ['success' => $msgBlock]);
    }
}
