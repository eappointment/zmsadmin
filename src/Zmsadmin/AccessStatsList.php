<?php
/**
 * @copyright BerlinOnline GmbH
 **/

declare(strict_types=1);

namespace BO\Zmsadmin;

use BO\Slim\Render;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class AccessStatsList extends BaseController
{
    /**
     * @SuppressWarnings(UnusedFormalParameter)
     * @return ResponseInterface
     */
    public function readResponse(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        \App::$http->readGetResult('/workstation/', ['resolveReferences' => 1])->getEntity();

        $statsList = \App::$http->readGetResult('/accessstats/')->getCollection();
        $response  = Render::withLastModified($response, time(), '0');

        return Render::withJson($response, $statsList);
    }
}
