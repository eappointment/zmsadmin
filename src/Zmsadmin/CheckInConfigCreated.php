<?php

declare(strict_types=1);

namespace BO\Zmsadmin;

use App;
use BO\Slim\Container;
use BO\Slim\Render;
use BO\Zmsentities\Config;
use BO\Zmsentities\Workstation;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Slim\Exception\HttpNotFoundException;

class CheckInConfigCreated extends BaseController
{
    /**
     * @SuppressWarnings(Param)
     *
     * @throws HttpNotFoundException
     */
    public function readResponse(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        if (!App::getConfigService()->isCheckInEnabled()) {
            throw new HttpNotFoundException($request);
        }

        /** @var Workstation $workstation */
        $workstation  = App::$http->readGetResult('/workstation/', ['resolveReferences' => 0])->getEntity();
        $configEntity = App::getConfigService()->getEntity();

        return Render::withHtml(
            $response,
            'page/checkInConfigCreated.twig',
            [
                'appUrl'      => $configEntity->getPreference('checkin', 'baseUrl') . $args['id'] . '/',
                'workstation' => $workstation,
            ]
        );
    }
}
