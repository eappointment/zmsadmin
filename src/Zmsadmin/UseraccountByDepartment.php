<?php
/**
 * @package Zmsadmin
 * @copyright BerlinOnline GmbH
 **/

namespace BO\Zmsadmin;

use App;
use BO\Slim\Render;
use BO\Zmsadmin\Helper\GraphDefaults;
use BO\Zmsentities\Workstation;
use BO\Zmsentities\Collection\UseraccountList;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class UseraccountByDepartment extends BaseController
{
    /**
     * @SuppressWarnings(Param)
     */
    public function readResponse(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        $departmentId = $args['id'];
        $workstation = App::$http->readGetResult('/workstation/', ['resolveReferences' => 1])->getEntity();
        $department = App::$http->readGetResult("/department/$departmentId/")->getEntity();
        $useraccountList = App::$http->readGetResult("/department/$departmentId/useraccount/", [
            'resolveReferences' => 0,
            'gql' => GraphDefaults::getUserForUseraccountList()
        ])->getCollection();
        $workstationList = App::$http->readGetResult(
            "/department/$departmentId/workstation/",
            [
                'resolveReferences' => 0,
                'gql' => '{useraccount{id scope{id}}}'
            ]
        )->getCollection();
        $scopeList = App::$http->readGetResult('/scope/', ['resolveReferences' => 0])->getCollection();

        /** @var Workstation $workstationItem */
        foreach ($workstationList as $workstationItem) {
            if ($scopeId = $workstationItem->getScope()->getId()) {
                $workstationItem->scope = $scopeList->getEntity($scopeId);
            }
        }
        $ownerList = App::$http->readGetResult('/owner/', [
            'resolveReferences' => 2,
            'gql' => GraphDefaults::getOwnerForUseraccountList()
        ])->getCollection();

        return Render::withHtml(
            $response,
            'page/useraccount.twig',
            array(
                'title' => 'Nutzer',
                'menuActive' => 'useraccount',
                'workstation' => $workstation,
                'department' => $department,
                'workstationList' => $workstationList,
                'useraccountList' => ($useraccountList) ?
                    $useraccountList->sortByCustomStringKey('id') :
                    new UseraccountList(),
                'ownerlist' => $ownerList,
            )
        );
    }
}
