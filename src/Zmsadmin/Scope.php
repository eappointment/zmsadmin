<?php
/**
 *
 * @package Zmsadmin
 * @copyright BerlinOnline GmbH
 *
 */
namespace BO\Zmsadmin;

use BO\Zmsadmin\Helper\ImageUploader;
use BO\Zmsclient\Exception;
use BO\Zmsentities\Scope as Entity;
use BO\Mellon\Validator;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class Scope extends BaseController
{
    public function readResponse(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        $success = $request->getAttribute('validator')->getParameter('success')->isString()->getValue();

        $workstation = \App::$http->readGetResult('/workstation/', [
            'resolveReferences' => 1,
            'gql' => Helper\GraphDefaults::getWorkstation()
        ])->getEntity();

        $entityId = Validator::value($args['id'])->isNumber()->getValue();
        $entity = \App::$http
            ->readGetResult('/scope/' . $entityId . '/', [
                'resolveReferences' => 1,
                'accessRights' => 'scope',
                'gql' => Helper\GraphDefaults::getScope()
            ])
            ->getEntity();

        $sourceList = $this->readSourceList();
        $providerList = Helper\ProviderHandler::readProviderList($entity->getSource());
        $currentSource = $this->readCurrentSource($entity->getSource());
        
        $organisation = \App::$http->readGetResult('/scope/' . $entityId . '/organisation/')->getEntity();
        $department = \App::$http->readGetResult('/scope/' . $entityId . '/department/')->getEntity();
        $callDisplayImage = \App::$http->readGetResult('/scope/'. $entityId .'/imagedata/calldisplay/')->getEntity();

        if ($request->getMethod() === 'POST') {
            $input = $request->getParsedBody();
            $result = $this->testUpdateEntity($input, $entityId);
            if ($result instanceof Entity) {
                try {
                    $this->writeUploadedImage($request, $entityId, $input);
                    return \BO\Slim\Render::redirect('scope', ['id' => $entityId], [
                        'success' => 'scope_saved'
                    ]);
                } catch (Exception $clientException) {
                    $result = ['data' => $clientException->data];
                } catch (\InvalidArgumentException $exception) {
                    $result = ['data' => ['content' => ['messages' => [$exception->getMessage()]]]];
                }
            }

            $entity->addData($input);
        }

        return \BO\Slim\Render::withHtml(
            $response,
            'page/scope.twig',
            array(
                'title' => 'Standort',
                'menuActive' => 'owner',
                'workstation' => $workstation,
                'scope' => $entity,
                'provider' => $entity->provider,
                'organisation' => $organisation,
                'department' => $department,
                'providerList' => $providerList,
                'sourceList' => $sourceList,
                'source' => $currentSource,
                'callDisplayImage' => $callDisplayImage,
                'success' => $success,
                'exception' => (isset($result)) ? $result : null
            )
        );
    }

    protected function readSourceList()
    {
        $sourceList = \App::$http->readGetResult('/source/')->getCollection();
        return $sourceList;
    }

    protected function readCurrentSource($source)
    {
        $source = \App::$http->readGetResult('/source/'. $source .'/')->getEntity();
        return $source;
    }

    /**
     * @param array $input scope entity, if used without ID, a new scope is created
     * @param Number $entityId Might be the entity scope or department if called from DepartmentAddScope
     */
    protected function testUpdateEntity(array $input, $entityId = null)
    {
        $entity = (new Entity($input))->withCleanedUpFormData();
        try {
            if ($entity->id) {
                $entity->id = $entityId;
                $entity = \App::$http->readPostResult('/scope/' . $entity->id . '/', $entity)->getEntity();
            } else {
                $entity = \App::$http->readPostResult('/department/'. $entityId .'/scope/', $entity)
                                     ->getEntity();
            }
        } catch (\BO\Zmsclient\Exception $exception) {
            $template = Helper\TwigExceptionHandler::getExceptionTemplate($exception);
            if ('' != $exception->template
                && \App::$slim->getContainer()->get('view')->getLoader()->exists($template)
            ) {
                return [
                    'template' => $template,
                    'data' => $exception->data
                ];
            }
            throw $exception;
        }
        return $entity;
    }

    protected function writeUploadedImage(RequestInterface $request, $entityId, $input)
    {
        if (isset($input['removeImage']) && $input['removeImage']) {
            \App::$http->readDeleteResult('/scope/'. $entityId .'/imagedata/calldisplay/');
        } else {
            (new ImageUploader($request, 'uploadCallDisplayImage'))
                ->writeUploadToScope($entityId);
        }
    }
}
