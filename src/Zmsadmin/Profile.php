<?php

/**
 * @package Zmsadmin
 * @copyright BerlinOnline GmbH
 **/

namespace BO\Zmsadmin;

use App;
use BO\Slim\Render;
use BO\Zmsclient\Exception as ZmsclientException;
use BO\Zmsentities\Exception\SchemaFailedParseJsonFile;
use BO\Zmsentities\Schema\Loader;
use BO\Zmsentities\Useraccount as Entity;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Fig\Http\Message\RequestMethodInterface;

/**
 * @SuppressWarnings(Coupling)
 */
class Profile extends BaseController
{
    /**
     * @SuppressWarnings(Param)
     * @throws ContainerExceptionInterface
     * @throws ZmsclientException
     * @throws NotFoundExceptionInterface
     * @throws SchemaFailedParseJsonFile
     */
    public function readResponse(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ) {
        $workstation = \App::$http->readGetResult('/workstation/', ['resolveReferences' => 2])->getEntity();
        $confirmSuccess = $request->getAttribute('validator')->getParameter('success')->isString()->getValue();
        $error = $request->getAttribute('validator')->getParameter('error')->isString()->getValue();
        $entity = new Entity($workstation->useraccount);

        if ($request->getMethod() === RequestMethodInterface::METHOD_POST) {
            $input = $request->getParsedBody();
            $result = $this->writeUpdatedEntity($input, $entity->getId());
            if ($result instanceof Entity) {
                return Render::redirect('profile', [], [
                    'success' => 'useraccount_saved'
                ]);
            }
        }
        $allowedProviderList = array_filter(explode(',', App::$config->getEntity()->getPreference('oidc', 'provider')));
        return Render::withHtml(
            $response,
            'page/profile.twig',
            array(
                'title' => 'Nutzerprofil',
                'menuActive' => 'profile',
                'workstation' => $workstation,
                'useraccount' => $entity->getArrayCopy(),
                'success' => $confirmSuccess,
                'error' => $error,
                'exception' => (isset($result)) ? $result : null,
                'metadata' => $this->getSchemaConstraintList(Loader::asArray(Entity::$schema)),
                'isFromOidc' => (
                    count($allowedProviderList) &&
                    in_array($entity->getOidcProviderFromName(), $allowedProviderList)
                )
            )
        );
    }

    /**
     *
     * @param $input
     * @return array|\BO\Zmsentities\Schema\Entity|false|null
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     * @throws ZmsclientException
     */
    protected function writeUpdatedEntity($input)
    {
        $entity = (new Entity($input))->withCleanedUpFormData();
        $entity->setPassword($input);
        try {
            $entity = \App::$http->readPostResult('/workstation/password/', $entity)->getEntity();
        } catch (ZmsclientException $exception) {
            $template = Helper\TwigExceptionHandler::getExceptionTemplate($exception);
            if ('' != $exception->template
                && \App::$slim->getContainer()->get('view')->getLoader()->exists($template)
            ) {
                return [
                    'template' => $template,
                    'data' => $exception->data
                ];
            }
            throw $exception;
        }
        return $entity;
    }
}
