<?php

declare(strict_types=1);

namespace BO\Zmsadmin;

use BO\Slim\Render;
use BO\Zmsentities\CheckInConfig;
use BO\Zmsentities\Schema\Entity;
use Psr\Http\Message\ResponseInterface;

class CheckInConfigEdit extends CheckInConfigAdd
{
    /** @var CheckInConfig|null */
    protected $entityCache = null;

    protected function readEntityByArgs(array $args): CheckInConfig
    {
        $source = '/checkinconfig/' . $args['id'] . '/';
        $entity = $this->entityCache ?? \App::$http->readGetResult($source)->getEntity() ?? null;

        if ($entity) {
            $this->postUrl = $source;
        }

        $this->entityCache = $entity;

        return $this->entityCache;
    }

    protected function getSuccessResponse(Entity $entity): ResponseInterface
    {
        $classParts = explode('\\', get_class($this));
        $msgBlock = strtolower(preg_replace('/(?<!^)[A-Z]/', '_$0', end($classParts)));

        return Render::redirect('checkIn', [], ['success' => $msgBlock]);
    }
}
