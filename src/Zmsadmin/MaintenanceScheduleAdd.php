<?php
/**
 * @copyright BerlinOnline GmbH
 **/

declare(strict_types=1);

namespace BO\Zmsadmin;

use BO\Slim\Render;

use BO\Zmsclient\Exception;

use BO\Zmsentities\Exception\SchemaValidation as SchemaValidationException;
use BO\Zmsentities\Helper\DateTime;
use BO\Zmsentities\Schema\Loader;
use BO\Zmsentities\MaintenanceSchedule as ScheduleEntity;

use DateTimeImmutable;
use DateTimeZone;
use Fig\Http\Message\StatusCodeInterface;
use JsonSchema\Exception\ValidationException;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

use Slim\Http\Request;
use Slim\Http\Response;

class MaintenanceScheduleAdd extends BaseController
{
    /**
     * @SuppressWarnings(UnusedFormalParameter)
     * @param RequestInterface|Request $request
     * @param ResponseInterface|Response $response
     * @param array $args
     * @return ResponseInterface
     */
    public function readResponse(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        $workstation = \App::$http->readGetResult('/workstation/', ['resolveReferences' => 1])->getEntity();
        $parameters  = $this->getEntityParameters($args);

        if ($request->getMethod() === 'POST' && isset($request->getParsedBody()['preview'])) {
            $parameters['schedule'] = (new ScheduleEntity($request->getParsedBody()))->withCleanedUpFormData();
        } elseif ($request->getMethod() === 'POST') {
            try {
                $this->processInput($request);
                $classParts = explode('\\', get_class($this));
                $msgBlock = strtolower(preg_replace('/(?<!^)[A-Z]/', '_$0', end($classParts)));

                return Render::redirect('maintenance', [], ['success' => $msgBlock]);
            } catch (Exception $clientException) {
                if ($clientException->getCode() === StatusCodeInterface::STATUS_NOT_ACCEPTABLE
                    || $clientException->getCode() === StatusCodeInterface::STATUS_BAD_REQUEST
                ) {
                    $parameters['schedule']  = new ScheduleEntity(
                        $request->getParsedBody() ?? (new ScheduleEntity())->getDefaults()
                    );
                    $parameters['exception'] = [
                        'template' => Helper\TwigExceptionHandler::getExceptionTemplate($clientException),
                        'data' => $clientException->data,
                        'include' => true,
                    ];
                }
            }
        }

        $parameters['preview'] = new ScheduleEntity();
        $parameters['preview']->setStartDateTime(new DateTime());

        return Render::withHtml(
            $response,
            'page/maintenanceScheduleEdit.twig',
            array_merge($parameters, [
                'workstation' => $workstation,
                'metadata' => $this->getSchemaConstraintList(Loader::asArray(ScheduleEntity::$schema)),
            ])
        );
    }

    /**
     * @SuppressWarnings(UnusedFormalParameter)
     */
    protected function getEntityParameters(array $args): array
    {
        return ['schedule' => new ScheduleEntity()];
    }

    /**
     * @throws SchemaValidationException
     * @throws \Exception
     */

    protected function processInput(RequestInterface $request): void
    {
        $entity = (new ScheduleEntity($request->getParsedBody()))->withCleanedUpFormData();
        \App::$http->readPostResult('/maintenanceschedule/', $entity)->getEntity();
    }
}
