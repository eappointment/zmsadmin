<?php

/**
 *
 * @package Zmsadmin
 * @copyright BerlinOnline GmbH
 *
 */
namespace BO\Zmsadmin;

use BO\Zmsentities\Source as Entity;
use BO\Zmsclient\Exception as ClientException;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class SourceEdit extends BaseController
{

    /**
     * @SuppressWarnings(Param)
     */
    public function readResponse(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        $workstation = \App::$http->readGetResult('/workstation/', ['resolveReferences' => 1])->getEntity();
        $success = $request->getAttribute('validator')->getParameter('success')->isString()->getValue();
        $source  = null;
        if (!$workstation->hasSuperUseraccount()) {
            throw new Exception\NotAllowed();
        }

        if ('add' != $args['name']) {
            $source = \App::$http
                ->readGetResult('/source/'. $args['name'] .'/', ['resolveReferences' => 2])
                ->getEntity();
        }
        
        $input = $request->getParsedBody();
        if ($request->getMethod() === 'POST' && !empty($input)) {
            $result = $this->testUpdateEntity($input);
            if ($result instanceof Entity) {
                return \BO\Slim\Render::redirect('sourceEdit', ['name' => $result->getSource()], [
                    'success' => 'source_saved'
                ]);
            }
            if ('add' != $args['name']) {
                $input['lastChange'] = $source['lastChange'];
            }
            $source = new Entity($input);
        }

        return \BO\Slim\Render::withHtml(
            $response,
            'page/sourceedit.twig',
            array(
                'title' => 'Mandanten bearbeiten',
                'menuActive' => 'source',
                'workstation' => $workstation,
                'source' => $source,
                'success' => $success,
                'exception' => (isset($result)) ? $result : null
            )
        );
    }

    protected function testUpdateEntity($input)
    {
        $entity = (new Entity($input))->withCleanedUpFormData();
        try {
            $entity = \App::$http->readPostResult('/source/', $entity)->getEntity();
        } catch (ClientException $exception) {
            if ('BO\Zmsdb\Exception\Source\SourceInvalidInput' == $exception->template) {
                return [
                    'template' => Helper\TwigExceptionHandler::getExceptionTemplate($exception),
                    'data'     => [],
                ];
            } elseif ('BO\Zmsentities\Exception\SchemaValidation' == $exception->template) {
                return [
                    'template' => Helper\TwigExceptionHandler::getExceptionTemplate($exception),
                    'data'     => $exception->data,
                ];
            } else {
                throw $exception;
            }
        }
        return $entity;
    }
}
