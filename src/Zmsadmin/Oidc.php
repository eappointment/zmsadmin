<?php
/**
 * @package Zmsadmin
 * @copyright BerlinOnline GmbH
 **/

namespace BO\Zmsadmin;

use BO\Slim\Render;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class Oidc extends BaseController
{
    /**
     * @SuppressWarnings(Param)
     * @return ResponseInterface
     */
    public function readResponse(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ) {
        return Render::redirect(
            'workstationSelect',
            [],
            []
        );
    }
}
