<?php
/**
 * @package Zmsadmin
 * @copyright BerlinOnline GmbH
 **/

namespace BO\Zmsadmin;

use BO\Zmsadmin\BaseController;
use BO\Zmsentities\Collection\AvailabilityList as Collection;
use BO\Slim\Render;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class AvailabilitySlotCalculation extends BaseController
{
    /**
     * @SuppressWarnings(Param)
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function readResponse(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ) {
        \App::$http->readGetResult('/workstation/', ['resolveReferences' => 1])->getEntity();
        $validator = $request->getAttribute('validator');
        $input = $validator->getInput()->isJson()->assertValid()->getValue();
        $collection = (new Collection())->addData($input);

        $availabilityList = \App::$http->readPostResult('/availability/slots/update/', $collection)->getCollection();
        $response = Render::withLastModified($response, time(), '0');
        return Render::withJson($response, $availabilityList);
    }
}
