<?php

/**
 * @package Zmsadmin
 * @copyright BerlinOnline GmbH
 **/

namespace BO\Zmsadmin;

use App;
use BO\Slim\Render;
use BO\Zmsentities\Collection\ScopeList;
use BO\Zmsentities\RemoteCall as RemoteCallEntity;
use BO\Zmsentities\Scope as ScopeEntity;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class AppointmentForm extends BaseController
{
    /**
     * @SuppressWarnings(Param)
     * @SuppressWarnings(Cyclomatic)
     * @SuppressWarnings(Complexity)
     * @param RequestInterface  $request
     * @param ResponseInterface $response
     * @param array             $args
     *
     * @return ResponseInterface
     */
    public function readResponse(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        $validator = $request->getAttribute('validator');
        $workstation = App::$http->readGetResult('/workstation/', [
            'resolveReferences' => 2,
            'gql' => Helper\GraphDefaults::getWorkstation()
        ])->getEntity();
        $selectedScope = $validator->getParameter('selectedscope')->isNumber()->getValue();
        if ($selectedScope && $workstation->getScope()->getId() != $selectedScope) {
            $workstation->offsetSet('scope', new ScopeEntity(['id' => $selectedScope]));
        }

        $selectedProcess = Helper\AppointmentFormHelper::readSelectedProcess($request);

        $selectedDate = ($selectedProcess && $selectedProcess->hasId())
            ? $selectedProcess->getFirstAppointment()->toDateTime()->format('Y-m-d')
            : $validator->getParameter('selecteddate')->isString()->getValue();

        $selectedTime = ($selectedProcess && $selectedProcess->hasId())
            ? $selectedProcess->getFirstAppointment()->getStartTime()->format('H-i')
            : $validator->getParameter('selectedtime')->isString()->getValue();

        $selectedScope = Helper\AppointmentFormHelper::readSelectedScope($request, $workstation, $selectedProcess);

        $requestList = ($selectedScope && $selectedScope->hasId())
            ? Helper\AppointmentFormHelper::readRequestList($request, $workstation, $selectedScope)
            : null;

        $freeProcessList = ($selectedScope)
            ? Helper\AppointmentFormHelper::readFreeProcessList($request, $workstation, $selectedProcess)
            : null;

        return Render::withHtml(
            $response,
            'block/appointment/form.twig',
            array(
                'workstation' => $workstation,
                'scope' => $selectedScope,
                'cluster' => (new Helper\ClusterHelper($workstation))->getEntity(),
                'department' =>
                    App::$http->readGetResult('/scope/' . $workstation->scope['id'] . '/department/', [
                        'gql' => Helper\GraphDefaults::getDepartment()
                    ])->getEntity(),
                'selectedProcess' => $selectedProcess,
                'detailRemoteCall' => ($selectedProcess) ?
                    $selectedProcess->getRemoteCalls()->getByName(RemoteCallEntity::TYPE_DETAIL) :
                    null,
                'selectedDate' => ($selectedDate) ?: App::$now->format('Y-m-d'),
                'selectedTime' => $selectedTime,
                'freeProcessList' => $freeProcessList,
                'requestList' => $requestList
            )
        );
    }
}
