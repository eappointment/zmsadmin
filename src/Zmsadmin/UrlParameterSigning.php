<?php
/**
 * @copyright BerlinOnline GmbH
 **/

declare(strict_types=1);

namespace BO\Zmsadmin;

use BO\Mellon\Validator;
use BO\Slim\Helper;
use BO\Slim\Render;
use BO\Zmsadmin\Exception\BadRequest;
use BO\Zmsadmin\Exception\NotAllowed;
use BO\Zmsentities\Collection\ScopeList;
use BO\Zmsentities\Collection\ClusterList;
use BO\Zmsentities\Exception\UserAccountAccessRightsFailed;
use BO\Zmsentities\Helper\Property;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Slim\Http\Request as SlimRequest;

/**
 * returning Signatures for signing requests
 */
class UrlParameterSigning extends BaseController
{
    /**
     * @SuppressWarnings(UnusedFormalParameter)
     * @param SlimRequest $request
     * @return String
     */
    public function readResponse(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ) {
        $validator = $request->getAttribute('validator');
        $data = $validator->getInput()->isJson()->assertValid()->getValue();
        $this->testData($data);

        $workstation = \App::$http->readGetResult('/workstation/', ['resolveReferences' => 0])->getEntity();
        $collections = isset($data['parameters']['collections']) ? $data['parameters']['collections'] : [];

        $hasScopeList = (isset($collections['scopelist']) && strlen($collections['scopelist']) > 0);
        $hasClusterList = (isset($collections['clusterlist']) && strlen($collections['clusterlist']) > 0);
        $hasValidScopeId = (
            isset($workstation['scope']['id']) &&
            !Validator::value($workstation['scope']['id'])->isNumber()->hasFailed()
        );

        if (($hasScopeList || $hasClusterList) && $hasValidScopeId) {
            $organisation = \App::$http->readGetResult(
                '/scope/'. $workstation['scope']['id'] .'/organisation/',
                ['resolveReferences' => 3]
            )->getEntity();

            $this->testAccess($organisation, $collections);
        }

        $data['hmac'] = Helper::hashQueryParameters($data['section'], $data['parameters'], ['collections', 'queue']);
        return Render::withJson($response, $data);
    }

    private function testData($data)
    {
        if (!isset($data['section']) || !isset($data['parameters'])) {
            throw new BadRequest();
        }
    }

    private function testAccess($organisation, $collections)
    {
        $scopeList = new ScopeList();
        $clusterList = new ClusterList();

        foreach ($organisation->departments as $department) {
            $scopeList->addList($department->getScopeList()->withUniqueScopes());
            if (isset($department->clusters)) {
                $clusterList->addData($department->clusters);
            }
        }
        if (isset($collections['scopelist']) && strlen($collections['scopelist']) > 0) {
            if (count(array_diff(explode(',', $collections['scopelist']), $scopeList->getIds())) > 0) {
                throw new UserAccountAccessRightsFailed();
            }
        }
        if (isset($collections['clusterlist']) && strlen($collections['clusterlist']) > 0) {
            if (count(array_diff(explode(',', $collections['clusterlist']), $clusterList->getIds())) > 0) {
                throw new UserAccountAccessRightsFailed();
            }
        }
    }
}
