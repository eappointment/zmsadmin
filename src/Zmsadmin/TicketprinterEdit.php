<?php

declare(strict_types=1);

namespace BO\Zmsadmin;

use BO\Mellon\Validator;
use BO\Slim\Render;
use BO\Zmsentities\ApplicationRegister;
use BO\Zmsentities\Collection\DepartmentList;
use BO\Zmsentities\Department as DepartmentEntity;
use Fig\Http\Message\RequestMethodInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class TicketprinterEdit extends BaseController
{
    /**
     * @SuppressWarnings(Param)
     * @return ResponseInterface
     */
    public function readResponse(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ) {
        $workstation = \App::$http->readGetResult('/workstation/', ['resolveReferences' => 1])->getEntity();
        $workstation->getUseraccount()->testRights(['ticketprinter']);

        $scopeId = Validator::value($workstation['scope']['id'] ?? null)->isNumber()->getValue();
        if (!$scopeId) {
            return Render::redirect('workstationSelect', ['error' => 'scope_missing']);
        }
        $applicationId = Validator::value($args['id'])->isString()->getValue();
        /** @var Validator $validator */
        $validator = $request->getAttribute('validator');
        $origin = $validator->getParameter('origin')->isString()->getValue();
        $error = '';

        if ($request->getMethod() === RequestMethodInterface::METHOD_POST) {
            $postData = $request->getParsedBody();
            $parameters = [];
            $parametersString = Validator::value($postData['parameters'])->isString()->getValue();
            parse_str($parametersString, $parameters);

            if (isset($parameters['ticketprinter']['buttonlist'])) {
                try {
                    \App::$http->readPostResult(
                        '/applicationregister/' . $applicationId . '/change/',
                        ['parameters' => html_entity_decode($parametersString)]
                    );
                    return $origin ?
                        $response->withHeader('Location', $origin . '?success=asynchronous_edit') :
                        Render::redirect('ticketprinter', ['success' => 'asynchronous_edit']);
                } catch (\Exception $e) {
                    $error = 'communication_error';
                }
            } else {
                $error = 'scope_missing';
            }
        }

        /** @var ApplicationRegister $application */
        $application   = \App::$http->readGetResult('/applicationregister/' . $applicationId . '/')->getEntity();
        $configuration = \App::$config->getEntity();
        $organisation  = \App::$http->readGetResult(
            '/scope/'. $scopeId .'/organisation/',
            ['resolveReferences' => 3]
        )->getEntity();

        $parameters = [];
        parse_str($application->parameters, $parameters);
        if (isset($parameters['ticketprinter']['buttonlist'])) {
            $parameters['ticketprinter']['buttonlist'] = explode(',', $parameters['ticketprinter']['buttonlist']);
        }

        $departments = new DepartmentList();
        $scopeIds = [];

        foreach ($organisation->departments as $departmentData) {
            $department = (new DepartmentEntity($departmentData))->withCompleteScopeList();
            $departments->addEntity($department);
            $scopeIds = array_merge($scopeIds, $department->getScopeList()->getIds());
        }

        return Render::withHtml(
            $response,
            'page/ticketprinterConfig.twig',
            array(
                'title' => 'Wartenummernausgabe (ID: ' . $application->getId() . ') ändern',
                'edit'  => 1,
                'config' => $configuration->getArrayCopy(),
                'parameters'   => $parameters,
                'organisation' => $organisation->getArrayCopy(),
                'departments' => $departments->getArrayCopy(),
                'workstation' => $workstation,
                'menuActive' => 'ticketprinter',
                'sid'       => $application->getId(),
                'scopeIds' => implode(',', $scopeIds),
                'origin'  => $origin,
                'error'  => $error,
            )
        );
    }
}
