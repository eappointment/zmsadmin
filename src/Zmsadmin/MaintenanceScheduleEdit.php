<?php
/**
 * @copyright BerlinOnline GmbH
 **/

declare(strict_types=1);

namespace BO\Zmsadmin;

use BO\Mellon\Validator;
use BO\Zmsentities\MaintenanceSchedule as ScheduleEntity;
use Psr\Http\Message\RequestInterface;

class MaintenanceScheduleEdit extends MaintenanceScheduleAdd
{
    /**
     * @SuppressWarnings(ShortVariable)
     * @param array $args
     * @return array
     */
    protected function getEntityParameters(array $args): array
    {
        $id = Validator::value($args['id'])->isNumber()->getValue();
        return ['schedule' => \App::$http->readGetResult('/maintenanceschedule/'. $id .'/')->getEntity()];
    }

    protected function processInput(RequestInterface $request): void
    {
        $entity = (new ScheduleEntity($request->getParsedBody()))->withCleanedUpFormData();
        \App::$http->readPostResult('/maintenanceschedule/' . $entity['id'] . '/', $entity)->getEntity();
    }
}
