<?php

/**
 *
 * @package Zmsadmin
 * @copyright BerlinOnline GmbH
 *
 */
namespace BO\Zmsadmin;

use App;

use BO\Mellon\Validator;

use BO\Slim\Render;

use BO\Zmsentities\Collection\ScopeList;
use BO\Zmsentities\Collection\WorkstationList;
use BO\Zmsentities\Exception\WorkstationProcessMatchScopeFailed;
use BO\Zmsentities\Scope as ScopeEntity;
use BO\Zmsentities\Workstation;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * Delete a process
 */
class ProcessDelete extends BaseController
{

    /**
     * @SuppressWarnings(Param)
     * @throws WorkstationProcessMatchScopeFailed
     */
    public function readResponse(
        RequestInterface  $request,
        ResponseInterface $response,
        array             $args
    ): ResponseInterface {
        /** @var Workstation $workstation */
        $workstation = App::$http->readGetResult('/workstation/', ['resolveReferences' => 1])->getEntity();
        /** @var Validator $validator */
        $validator = $request->getAttribute('validator');
        $processId = Validator::value($args['id'])->isNumber()->getValue();
        $initiator = $validator->getParameter('initiator')->isString()->getValue();
        $selectedScope = $validator->getParameter('selectedscope')->isNumber()->getValue();
        if ($selectedScope && $workstation->getScope()->getId() != $selectedScope) {
            $workstation->offsetSet('scope', new ScopeEntity(['id' => $selectedScope]));
        }
        $process = App::$http->readGetResult('/process/'. $processId .'/')->getEntity();
        $process->status = 'deleted';
        $scopeList = (new ScopeList())->addEntity($workstation->getScope());
        $workstation->testMatchingProcessScope(
            (new Helper\ClusterHelper($workstation))->getScopeList($scopeList),
            $process
        );

        App::$http->readDeleteResult('/process/'. $process->getId() .'/', ['initiator' => $initiator])->getEntity();
        static::writeDeleteMailNotifications($process);

        return Render::withHtml(
            $response,
            'element/helper/messageHandler.twig',
            array(
                'success' => 'process_deleted',
                'selectedprocess' => $process,
            )
        );
    }

    public static function writeDeleteMailNotifications($process)
    {
        #email only for clients with appointment if email address is given
        if ($process->getFirstClient()->hasEmail() &&
            $process->isWithAppointment() &&
            $process->scope->hasEmailFrom()
        ) {
            App::$http
                ->readPostResult(
                    '/process/'. $process->getId() .'/'. $process->getAuthKey() .'/delete/mail/',
                    $process
                )->getEntity();
        }
        #sms notifications for clients with and without appointment if telephone number is given
        if ($process->scope->hasNotificationEnabled() &&
            $process->getFirstClient()->hasTelephone()
        ) {
            App::$http
                ->readPostResult(
                    '/process/'. $process->getId() .'/'. $process->getAuthKey() .'/delete/notification/',
                    $process
                )->getEntity();
        }
    }
}
