<?php

/**
 *
 * @package Zmsadmin
 * @copyright BerlinOnline GmbH
 *
 */
namespace BO\Zmsadmin;

use App;
use BO\Mellon\Validator;
use BO\Slim\Render;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

use BO\Zmsentities\Exception\WorkstationProcessMatchScopeFailed as ProcessAccessNotAllowedException;

/**
 * Summary of a process archive
 */
class ProcessArchiveSummary extends BaseController
{

    /**
     * @SuppressWarnings(Param)
     * @param RequestInterface  $request
     * @param ResponseInterface $response
     * @param array             $args
     *
     * @return ResponseInterface
     */
    public function readResponse(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ):ResponseInterface {
        $workstation = App::$http->readGetResult('/workstation/', ['resolveReferences' => 1])->getEntity();
        $processArchiveId = Validator::value($args['id'])->isString()->isBiggerThan(32)->isSmallerThan(32)->getValue();
        $hasProcessAccess = true;
        $process = App::$http
            ->readGetResult('/process/archive/'. $processArchiveId .'/', ['resolveReferences' => 1])
            ->getEntity();

        try {
            $workstation->testMatchingProcessScope($workstation->getScopeList(), $process);
        } catch (ProcessAccessNotAllowedException $exception) {
            $hasProcessAccess = false;
        }

        return Render::withHtml(
            $response,
            'page/processArchiveSummary.twig',
            [
                'title' => 'Terminarchiv',
                'process' => $process,
                'workstation' => $workstation,
                'source' => $workstation->getVariantName(),
                'hasprocessaccess' => $hasProcessAccess,
            ]
        );
    }
}
