<?php

/**
 * @package Zmsadmin
 * @copyright BerlinOnline GmbH
 **/

namespace BO\Zmsadmin;

use App;
use BO\Mellon\Exception;
use BO\Zmsadmin\Exception\BadRequest;
use BO\Zmsadmin\Exception\MailFromMissing;
use BO\Zmsadmin\Helper\Mail as MailHelper;
use BO\Mellon\Collection;
use BO\Mellon\Validator;
use Fig\Http\Message\RequestMethodInterface;
use Fig\Http\Message\StatusCodeInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use BO\Zmsentities\Collection\ProcessList;
use BO\Zmsentities\Department as DepartmentEntity;
use BO\Zmsentities\Mail as MailEntity;
use BO\Slim\Render;

/**
 * @SuppressWarnings(Coupling)
 */
class MailCancelAll extends BaseController
{
    public const MAIL_TEMPLATE = 'mail_custom_cancel_all';

    /**
     * @suppressWarnings(unused)
     * @param RequestInterface $request
     * @param ResponseInterface $response
     * @param array $args
     * @return ResponseInterface
     * @throws BadRequest
     * @throws Exception
     * @throws MailFromMissing
     */
    public function readResponse(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        $workstation = App::$http->readGetResult('/workstation/', ['resolveReferences' => 1])->getEntity();
        $workstation->getUseraccount()->testRights(['superuser']);

        $validator = $request->getAttribute('validator');
        $success = $validator->getParameter('success')->isString()->getValue();
        $error = $validator->getParameter('error')->isString()->getValue();
        $dialog = $validator->getParameter('dialog')->isNumber()->getValue();
        $scopeId = $validator->getParameter('scope')->isNumber()->getValue();
        $dateString = $validator->getParameter('date')->isString()->getValue();

        $isPost = $request->getMethod() === RequestMethodInterface::METHOD_POST;
        $validationCollection = null;

        if ($isPost) {
            if (!$scopeId || !$dateString) {
                throw new BadRequest();
            }
            $validationCollection = $this->getFormValidationCollection($request->getAttribute('validator'));
            if (!$validationCollection->hasFailed()) {
                $processList = $this->readProcessListByScopeAndDate($scopeId, $dateString);
                $department = App::$http->readGetResult('/scope/' . $scopeId . '/department/')->getEntity();
                $this->writeMailAndDelete($processList, $department, $validationCollection);

                return Render::redirect(
                    'mailCancelAll',
                    [],
                    [
                        'success' => 'mail_canceled_all',
                        'dialog' => $dialog,
                    ],
                );
            }
        }

        $status = ($validationCollection && $validationCollection->hasFailed()) ?
            StatusCodeInterface::STATUS_BAD_REQUEST :
            StatusCodeInterface::STATUS_OK;

        return Render::withHtml(
            $response,
            'page/mailCancelAll.twig',
            [
                'title' => 'Alle Termine absagen mit frei formulierbarer E-Mail',
                'menuActive' => $workstation->getVariantName(),
                'workstation' => $workstation,
                'success' => $success,
                'scopeId' => $scopeId,
                'dateString' => $dateString,
                'error' => $error,
                'dialog' => $dialog,
                'form' => $isPost ? $validationCollection->getStatus() : null,
                'redirect' => $workstation->getVariantName()
            ],
            $status
        );
    }

    /**
     * @throws MailFromMissing
     */
    protected function writeMailAndDelete(
        ProcessList $processList,
        DepartmentEntity $department,
        $validationCollection
    ): void {
        $mailHelper = new MailHelper();
         $validators = $validationCollection->getValues();
        $parameter = [
            'message' => urldecode($validators['message']->getValue()),
            'subject' => $validators['subject']->getValue(),
            'template' => self::MAIL_TEMPLATE
        ];
        foreach ($processList as $process) {
            if ($process->getFirstClient()->hasEmail()) {
                $result = $mailHelper->writeMailToQueue($process, $department, $parameter, false);
                if ($result instanceof MailEntity) {
                    App::$http
                        ->readDeleteResult('/process/' . $process->getId() . '/', [
                            'initiator' => 'admin-cancel-all'
                        ])
                        ->getEntity();
                }
            }
        }
    }

    /**
     * @throws Exception
     */
    protected function getFormValidationCollection(Validator $validator): Collection
    {
        /** @var Validator $validator */
        $collection = array();
        $collection['subject'] = $validator->getParameter('subject')->isString()
            ->isBiggerThan(2, "Es muss ein aussagekräftiger Betreff eingegeben werden");
        $collection['message'] = $validator->getParameter('message')->isString()
            ->isBiggerThan(2, "Es muss eine aussagekräftige Nachricht eingegeben werden");
        $collection['availabilities_accepted'] = $validator->getParameter('availabilities_accepted')->isNumber()
            ->isEqualTo(1, "Diese Bedingung muss zwingend ausgeführt werden");
        $collection['cancel_accepted'] = $validator->getParameter('cancel_accepted')->isNumber()
            ->isEqualTo(1, "Dieser Bedingung muss zwingend zugestimmt werden");
        return Validator::collection($collection);
    }

    protected function readProcessListByScopeAndDate(int $scopeId, string $dateString)
    {
        $processList = App::$http
            ->readGetResult(
                '/scope/' . $scopeId . '/process/' . $dateString . '/',
                [
                    'resolveReferences' => 2,
                    'gql' => Helper\GraphDefaults::getProcess()
                ]
            )
            ->getCollection();

        $queueList = $processList->toQueueList(App::$now);
        return $queueList
            ->withStatus(['confirmed', 'queued', 'reserved'])
            ->withSortedArrival()
            ->toProcessList();
    }
}
