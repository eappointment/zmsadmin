<?php
/**
 * @copyright BerlinOnline GmbH
 **/

declare(strict_types=1);

namespace BO\Zmsadmin;

use BO\Mellon\Validator;
use BO\Slim\Render;
use BO\Slim\Response;
use BO\Zmsclient\Exception;
use BO\Zmsentities\ApplicationRegister;
use Fig\Http\Message\StatusCodeInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use BO\Zmsentities\Collection\ApplicationRegisterList as ApplicationList;

class CalldisplayRegisterTable extends BaseController
{
    protected $template = 'block/list/appclients/calldisplay.twig';
    protected $appType  = ApplicationRegister::TYPE_ZMS2_CALLDISPLAY;

    /**
     * @SuppressWarnings(UnusedFormalParameter)
     * @return ResponseInterface
     */
    public function readResponse(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        \App::$http->readGetResult('/workstation/', ['resolveReferences' => 1])->getEntity();

        /** @var Validator $validator */
        $validator = $request->getAttribute('validator');
        if (!$validator->getParameter('scopeIds')->isString()->getValue()) {
            return $response->withStatus(StatusCodeInterface::STATUS_BAD_REQUEST, 'Parameter scopeIds is wrong');
        }

        $appList = $this->getAppList($validator);
        if ($appList->count() === 0) {
            return $response->withStatus(StatusCodeInterface::STATUS_NOT_FOUND, 'Nothing Found');
        }
        $scopeList = \App::$http->readGetResult('/scope/')->getCollection();

        $map = [];
        /** @var \BO\Zmsentities\Scope $scope */
        foreach ($scopeList as $scope) {
            $map[$scope->getId()] = $scope->getName() . ' ' . $scope->shortName;
        }

        return \BO\Slim\Render::withHtml(
            $response,
            $this->template,
            [
                'applicationlist' => $appList,
                'scopeNames' => $map,
                'origin' => urlencode(\App::$slim->urlFor($this->appType)),
            ]
        );
    }

    protected function getAppList(Validator $validator): ApplicationList
    {
        return \App::$http->readGetResult(
            '/applicationregister/',
            [
                'type'     => $this->appType,
                'scopeIds' => $validator->getParameter('scopeIds')->isString()->getValue(),
            ]
        )->getCollection() ?? new ApplicationList();
    }

    public function __invoke(RequestInterface $request, ResponseInterface $response, array $args)
    {
        $response = parent::__invoke($request, $response, $args);

        if ($response->getStatusCode() === StatusCodeInterface::STATUS_OK
            && !str_starts_with((string)$response->getBody(), '<div class="list')
        ) {
            return new Response(StatusCodeInterface::STATUS_SERVICE_UNAVAILABLE);
        }

        return $response;
    }
}
