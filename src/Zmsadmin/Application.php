<?php
/**
 * @copyright BerlinOnline GmbH
 **/

namespace BO\Zmsadmin;

use BO\Zmsadmin\Helper\Config as ConfigService;
use BO\Zmsclient\Http;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;

class Application extends \BO\Slim\Application
{
    /**
     * Name of the application
     *
     */
    const IDENTIFIER = 'Zmsadmin';

    const DEBUG = false;

    const TWIG_CACHE = '/cache/';

    public static $includeUrl = '/terminvereinbarung/admin';

    /**
     * allow cluster wide process calls
     */

    public static bool $allowClusterWideCall = true;

    /**
     * image preferences
     */
    public static $isImageAllowed = false;
    
    /**
     * language preferences
     */
    const MULTILANGUAGE = true;

    public static $locale = 'de';
    public static $supportedLanguages = array(
         // Default language
         'de' => array(
             'name'    => 'Deutsch',
             'locale'  => 'de_DE',
             'default' => true,
         ),
         // Other languages
         'en' => array(
             'name'    => 'English',
             'locale'  => 'en_GB',
         )
     );

    /**
    * config preferences
    */

    const CONFIG_SECURE_TOKEN = 'a9b215f1-e460-490c-8a0b-6d42c274d5e4';

    public static ?ConfigService $config = null;

    /**
     * @return ConfigService|null
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public static function getConfigService(): ?ConfigService
    {
        /** @var ConfigService $config */
        $container   = self::$slim->getContainer();
        $config = $container->has('zmsadmin.services.config') ?
            $container->get('zmsadmin.services.config') :
            null;
        if ($config && ! self::$config) {
            self::$config = $config->fetchEntity();
        }
        return self::$config;
    }

    /**
     * signature key for url signature to save query paramter with hash
     */
    public static $urlSignatureSecret = 'a9b215f1-e460-490c-8a0b-6d42c274d5e4';

    /**
     * OIDC Provider config file
     */
    public static $oidc_provider_config = "tests/mockup/mock-oidc-provider.json";

    /**
     * -----------------------------------------------------------------------
     * ZMS API access
     * @var Http $http
     */
    public static $http = null;

    public static $http_curl_config = array();

    const CLIENTKEY = '';

    const JSON_COMPRESS_LEVEL = 1;

    /**
     * HTTP url for api
     */
    const HTTP_BASE_URL = 'http://user:pass@host.tdl';
}
