<?php

/**
 *
 * @package Zmsadmin
 * @copyright BerlinOnline GmbH
 *
 */
namespace BO\Zmsadmin;

use App;

use BO\Mellon\Condition;

use BO\Slim\Render;

use BO\Zmsclient\Exception;
use BO\Zmsentities\Process;
use BO\Zmsentities\Schema\Entity as SchemaEntity;
use BO\Zmsentities\Validator\ProcessValidator;
use BO\Zmsentities\Scope as ScopeEntity;

use BO\Zmsadmin\Helper\AppointmentFormHelper;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * Queue a process from appointment formular without appointment
 */
class ProcessQueue extends BaseController
{

    /**
     * @SuppressWarnings(Param)
     * @param RequestInterface  $request
     * @param ResponseInterface $response
     * @param array             $args
     *
     * @return ResponseInterface
     */
    public function readResponse(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        $workstation = App::$http->readGetResult('/workstation/', ['resolveReferences' => 2])->getEntity();
        $validator = $request->getAttribute('validator');
        $selectedProcessId = $validator->getParameter('selectedprocess')->isNumber()->getValue();

        if ($selectedProcessId) {
            if ($getResponse = $this->returnGetResponse($request, $response, $selectedProcessId)) {
                return $getResponse;
            };
        }

        $input = $request->getParsedBody();
        $scope = AppointmentFormHelper::readSelectedScope($request, $workstation);
        $process = $this->getProcess($input, $scope);
        $validatedForm = static::getValidatedForm($validator, $process);
        if ($validatedForm['failed']) {
            return Render::withJson(
                $response,
                $validatedForm
            );
        }

        $process = $this->writeQueuedProcess($input, $process);
        return Render::withHtml(
            $response,
            'element/helper/messageHandler.twig',
            array(
                'selectedprocess' => $process,
                'success' => 'process_queued'
            )
        );
    }

    public static function getValidatedForm($validator, $process)
    {
        $processValidator = new ProcessValidator($process);
        $delegatedProcess = $processValidator->getDelegatedProcess();
        $processValidator
            ->validateName(
                $validator->getParameter('familyName'),
                $delegatedProcess->setter('clients', 0, 'familyName')
            )
            ->validateMail(
                $validator->getParameter('email'),
                $delegatedProcess->setter('clients', 0, 'email')
            )
            ->validateTelephone(
                $validator->getParameter('telephone'),
                $delegatedProcess->setter('clients', 0, 'telephone')
            )
            ->validateSurvey(
                $validator->getParameter('surveyAccepted'),
                $delegatedProcess->setter('clients', 0, 'surveyAccepted')
            )
            ->validateAmendment(
                $validator->getParameter('amendment'),
                $delegatedProcess->setter('amendment')
            )
            ->validateReminderTimestamp(
                $validator->getParameter('headsUpTime'),
                $delegatedProcess->setter('reminderTimestamp'),
                new Condition(
                    $validator->getParameter('sendReminder')->isNumber()->isNotEqualTo(1)
                )
            )

        ;
        $processValidator->getCollection()->addValid(
            $validator->getParameter('sendConfirmation')->isNumber(),
            $validator->getParameter('sendReminder')->isNumber()
        );

        $form = $processValidator->getCollection()->getStatus(null, true);
        $form['failed'] = $processValidator->getCollection()->hasFailed();
        return $form;
    }

    /**
     * @param RequestInterface  $request
     * @param ResponseInterface $response
     * @param int               $selectedProcessId
     *
     * @return ResponseInterface|void
     */
    protected function returnGetResponse(RequestInterface $request, ResponseInterface $response, int $selectedProcessId)
    {
        $validator = $request->getAttribute('validator');
        $process = $this->readSelectedProcessWithWaitingnumber($selectedProcessId);
        if ($process && $validator->getParameter('print')->isNumber()->getValue()) {
            App::$maintenance = null; // avoid maintenance info to be rendered
            return Render::withHtml(
                $response,
                'page/printWaitingNumber.twig',
                array(
                    'title' => ($process->isWithAppointment()) ? 'Vorgangsnummer drucken' : 'Wartenummer drucken',
                    'process' => $process
                )
            );
        }
    }

    /**
     * @param $selectedProcessId
     *
     * @return SchemaEntity|false|null
     */
    protected function readSelectedProcessWithWaitingnumber($selectedProcessId)
    {
        $result = null;
        if ($selectedProcessId) {
            $result = App::$http->readGetResult('/process/'. $selectedProcessId .'/')->getEntity();
        }
        return $result;
    }

    /**
     * @param $input
     * @param $scope
     *
     * @return Process
     */
    protected function getProcess($input, $scope): Process
    {
        $process = new Process();
        $notice = (! $this->isOpened($scope)) ? 'Außerhalb der Öffnungszeiten gebucht! ' : '';
        return $process->withUpdatedData($input, App::$now, $scope, $notice);
    }

    /**
     * @param $input
     * @param $process
     *
     * @return SchemaEntity|false|null
     */
    protected function writeQueuedProcess($input, $process)
    {
        $process = App::$http->readPostResult('/workstation/process/waitingnumber/', $process)->getEntity();
        AppointmentFormHelper::updateMailAndNotification($input, $process);
        return $process;
    }

    /**
     * @param ScopeEntity $scope
     *
     * @return false
     */
    protected function isOpened(ScopeEntity $scope): bool
    {
        $isOpened = false;
        if ($scope->getResolveLevel() < 1) {
            $scope =  App::$http->readGetResult('/scope/'. $scope->getId() .'/', [
                'resolveReferences' => 1,
                'gql' => Helper\GraphDefaults::getScope()
            ])
                ->getEntity();
        }
        try {
            $isOpened = App::$http
                ->readGetResult('/scope/'. $scope->getId() .'/availability/', ['resolveReferences' => 0])
                ->getCollection()
                ->withScope($scope)
                ->isOpened(App::$now);
        } catch (Exception $exception) {
            //
        }
        return $isOpened;
    }
}
