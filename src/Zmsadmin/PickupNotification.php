<?php

/**
 *
 * @package Zmsadmin
 * @copyright BerlinOnline GmbH
 *
 */
namespace BO\Zmsadmin;

use BO\Mellon\Validator;
use BO\Slim\Render;
use BO\Zmsentities\Notification as Entity;

/**
 * Delete a process
 */
class PickupNotification extends BaseController
{

    /**
     * @SuppressWarnings(Param)
     * @return String
     */
    public function readResponse(
        \Psr\Http\Message\RequestInterface $request,
        \Psr\Http\Message\ResponseInterface $response,
        array $args
    ) {
        $workstation = \App::$http->readGetResult('/workstation/', ['resolveReferences' => 1])->getEntity();
        $validator = $request->getAttribute('validator');
        $processId = $validator->getParameter('selectedprocess')->isNumber()->getValue();
        $process = \App::$http->readGetResult('/process/'. $processId .'/')->getEntity();
        $department = \App::$http
            ->readGetResult('/scope/'. $workstation->scope['id'] .'/department/')
            ->getEntity();
        $config = \App::$config->getEntity();

        if ($process->scope->hasNotificationEnabled()) {
            $notification = (new Entity)->toResolvedEntity($process, $config, $department, 'pickup');
            $notification = \App::$http->readPostResult('/notification/', $notification)->getEntity();
        }
    
        return \BO\Slim\Render::withHtml(
            $response,
            'block/pickup/notificationSent.twig',
            array(
               'process' => $process,
               'notification' => $notification ? $notification : null
            )
        );
    }
}
