<?php
/**
 * @package zmsadmin
 * @copyright BerlinOnline GmbH
 **/

namespace BO\Zmsadmin;

use BO\Slim\Render;
use BO\Zmsadmin\Helper\ImageUploader;
use BO\Zmsclient\Exception;
use BO\Zmsentities\Cluster as Entity;
use BO\Mellon\Validator;
use BO\Zmsentities\Collection\ScopeList;
use BO\Zmsentities\Mimepart;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class DepartmentAddCluster extends BaseController
{
    /**
     * @SuppressWarnings(Cyclomatic)
     * @SuppressWarnings(NPath)
     * @SuppressWarnings(Length)
     */
    public function readResponse(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        $workstation  = \App::$http->readGetResult('/workstation/', ['resolveReferences' => 1])->getEntity();
        $departmentId = Validator::value($args['departmentId'])->isNumber()->getValue();
        $department   = \App::$http
            ->readGetResult('/department/'. $departmentId .'/', ['resolveReferences' => 2])->getEntity();
        $organisation = \App::$http->readGetResult('/department/' . $departmentId . '/organisation/')->getEntity();
        $parameters   = [
            'title' => 'Cluster',
            'action' => 'add',
            'menuActive' => 'owner',
            'workstation' => $workstation,
            'scopeList' => $department->getScopeList()->withUniqueScopes(),
            'organisation' => $organisation,
            'department' => $department,
        ];

        $input = $request->getParsedBody();

        // image upload failed, but entity has already been created
        if ($createdId = $input['clusterId'] ?? false) {
            $input['id'] = $createdId;
            $parameters['clusterId'] = $input['clusterId'];
            unset($input['clusterId']);
        }
        $parameters['cluster'] = new Entity($input);

        $uploadFiles = $request->getUploadedFiles();
        $uploadedImg = $uploadFiles['uploadCallDisplayImage'] ?? null;
        $uploadedImg = ($uploadedImg === null || $uploadedImg->getFilePath() == '') ? null : new Mimepart([
            'mime'    => $uploadedImg->getClientMediaType(),
            'content' => base64_encode(file_get_contents($uploadedImg->getFilePath())),
            'base64'  => true,
        ]);
        $selectedImg = !isset($input['selectedImage']) ? null : new Mimepart([
            'content' => $input['selectedImage'],
            'mime'    => $input['selectedImageMime'],
            'base64'  => true,
        ]);

        unset($input['selectedImage']);
        unset($input['selectedImageMime']);

        if ($uploadedImg || $selectedImg) {
            $parameters['selectedImage'] = $uploadedImg ?? $selectedImg;
            $parameters['callDisplayImage'] = $uploadedImg ?? $selectedImg;
        }

        /** intended not to loop (ignore/disable IDE warnings), but to reduce complexity */
        while (is_array($input) && array_key_exists('save', $input)) {
            unset($input['save']); //prevent loop

            $entity = (new Entity($input))->withCleanedUpFormData();
            $entity->scopes = (new ScopeList($entity->scopes))->withUniqueScopes();
            $rUrl = !$entity->getId() ? '/department/'.$department->id.'/cluster/' : '/cluster/'.$entity->id.'/';
            try {
                $entity = \App::$http->readPostResult($rUrl, $entity)->getEntity();
            } catch (Exception $clientException) {
                $parameters['exception'] = [
                    'template' => Helper\TwigExceptionHandler::getExceptionTemplate($clientException),
                    'data'     => $clientException->data
                ];
                break;
            }

            if (isset($parameters['selectedImage'])) {
                try {
                    $imageUploader  = new ImageUploader($request, 'uploadCallDisplayImage');
                    $uploadFunction = $uploadedImg ? 'writeUploadToCluster' : 'writeMimepartToCluster';
                    $imageUploader->$uploadFunction($entity->id, $parameters['selectedImage']);
                } catch (Exception $clientException) {
                    $parameters['exception'] = [
                        'template' => Helper\TwigExceptionHandler::getExceptionTemplate($clientException),
                        'data'     => $clientException->data
                    ];
                    break;
                } catch (\InvalidArgumentException $exception) {
                    $parameters['exception'] = [
                        'template' => 'exception/bo/zmsentities/exception/schemavalidation.twig',
                        'data'     => ['content' => ['messages' => [$exception->getMessage()]]],
                    ];
                    break;
                }
            }

            return Render::redirect(
                'cluster',
                array(
                    'departmentId' => $department->id,
                    'clusterId'    => $entity->id
                ),
                array(
                    'success' => 'cluster_created'
                )
            );
        }

        return Render::withHtml($response, 'page/cluster.twig', $parameters);
    }
}
