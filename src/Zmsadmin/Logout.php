<?php
/**
 * @package Zmsadmin
 * @copyright BerlinOnline GmbH
 **/

namespace BO\Zmsadmin;

use BO\Slim\Render;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class Logout extends BaseController
{
    /**
     * @SuppressWarnings(Param)
     * @return String
     */
    public function readResponse(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        if (! $request->getParam('state') || $request->getParam('provider')) {
            $workstation = \App::$http->readGetResult('/workstation/', ['resolveReferences' => 0])->getEntity();
            \App::$http->readDeleteResult('/workstation/login/' . $workstation->useraccount['id'] . '/')->getEntity();
        }

        return Render::redirect(
            'index',
            array(),
            array('logout' => 1)
        );
    }
}
