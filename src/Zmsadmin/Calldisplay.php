<?php
/**
 * @package Zmsadmin
 * @copyright BerlinOnline GmbH
 **/

namespace BO\Zmsadmin;

use BO\Mellon\Validator;
use App;
use BO\Slim\Render;
use BO\Zmsentities\Department;
use BO\Zmsentities\Collection\DepartmentList;
use BO\Zmsentities\Helper\Application;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class Calldisplay extends BaseController
{
    /**
     * @SuppressWarnings(Param)
     * @return ResponseInterface
     */
    public function readResponse(
        RequestInterface $request,
        ResponseInterface                  $response,
        array                              $args
    ) {
        /** @var \BO\Zmsentities\Workstation $workstation */
        $workstation = \App::$http->readGetResult('/workstation/', ['resolveReferences' => 1])->getEntity();
        $workstation->getUseraccount()->testRights(['ticketprinter']);

        $scopeId = $workstation['scope']['id'] ?? null;
        $entityId = Validator::value($scopeId)->isNumber()->getValue();

        if (!$entityId) {
            return Render::redirect('workstationSelect', ['error' => 'scope_missing']);
        }

        $entity = \App::$http->readGetResult(
            '/scope/'. $entityId .'/organisation/',
            ['resolveReferences' => 3]
        )->getEntity();

        /** @var Validator $validator */
        $validator   = $request->getAttribute('validator');
        $departments = new DepartmentList();
        $scopeIds    = [];

        foreach ($entity->departments as $departmentData) {
            $department = (new Department($departmentData))->withCompleteScopeList();
            $departments->addEntity($department);
            $scopeIds = array_merge($scopeIds, $department->getScopeList()->getIds());
        }

        return Render::withHtml(
            $response,
            'page/calldisplay.twig',
            [
                'title'        => 'Aufrufanzeige',
                'edit'         => 0,
                'config'       => App::$config->getEntity(),
                'collections'  => null,
                'organisation' => $entity->getArrayCopy(),
                'departments'  => $departments->getArrayCopy(),
                'workstation'  => $workstation,
                'menuActive'   => 'calldisplay',
                'displayId'    => Application::getRandomHashByWorkstation($workstation),
                'scopeIds'     => implode(',', $scopeIds),
                'origin'       => null,
                'success'      => $validator->getParameter('success')->isString()->getValue(),
            ]
        );
    }
}
