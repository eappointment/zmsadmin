<?php
/**
 * @package Zmsadmin
 * @copyright BerlinOnline GmbH
 **/

namespace BO\Zmsadmin;

use BO\Mellon\Validator;
use BO\Zmsentities\Collection\DayoffList;
use \BO\Zmsentities\Scope;

class CalendarPage extends BaseController
{
    public static $legendtypes = [
        'square' => [
            'today small' => 'Heute',
            'closed' => 'Geschlossen',
            'selected' => 'Ausgewählt',
        ],
        'rectangle mark' => [
            'mark--free' => 'Freie Termine',
            'mark--destructive' => 'Ausgebucht',
            'mark--dayoff' => 'Freier Tag' ,
        ]
    ];

    /**
     * @SuppressWarnings(Param,Unused)
     * @return String
     */
    public function readResponse(
        \Psr\Http\Message\RequestInterface $request,
        \Psr\Http\Message\ResponseInterface $response,
        array $args
    ) {
        $workstation = \App::$http->readGetResult('/workstation/', ['resolveReferences' => 2])->getEntity();
        /** @var Validator $validator */
        $validator = $request->getAttribute('validator');
        $selectedDate = $validator->getParameter('selecteddate')->isString()->getValue();
        $slotType = $validator->getParameter('slotType')->isString()->getValue();
        $slotsRequired = $validator->getParameter('slotsRequired')->isNumber()->getValue();
        $selectedScopeId = $validator->getParameter('selectedscope')->isNumber()->isGreaterThan(0)->getValue();

        $scope = ($workstation->getScope() && $workstation->getScope()->getId()) ? $workstation->getScope() : null;
        $scope = Helper\AppointmentFormHelper::readSelectedScope($request, $workstation) ?? $scope;

        $calendar = new Helper\Calendar($selectedDate);

        $scopeList = ($selectedScopeId)
            ? (new \BO\Zmsentities\Collection\ScopeList)->addEntity($scope)
            : (new Helper\ClusterHelper($workstation))->getScopeList();
        $monthList = $calendar->readMonthListByScopeList($scopeList, $slotType, $slotsRequired);

        return \BO\Slim\Render::withHtml(
            $response,
            'block/calendar/calendarMonth.twig',
            array(
                'title' => 'Kalender',
                'calendar' => $calendar,
                'selectedDate' => ($selectedDate) ? $selectedDate : \App::$now->format('Y-m-d'),
                'selectedYear' => $calendar->getDateTime()->format('Y'),
                'selectedWeek' => $calendar->getDateTime()->format('W'),
                'dayoffList' => $scope ? $scope->getDayoffList() : new DayoffList(),
                'scopeList' => $scopeList,
                'monthList' => $monthList,
                'legendtypes' => static::$legendtypes,
            )
        );
    }
}
