<?php
/**
 * @package Zmsadmin
 * @copyright BerlinOnline GmbH
 **/

namespace BO\Zmsadmin;

use BO\Mellon\Validator;

use BO\Slim\Render;
use BO\Zmsentities\Department as DepartmentEntity;
use BO\Zmsentities\Collection\DepartmentList;
use BO\Zmsentities\Helper\Application;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class TicketprinterConfig extends BaseController
{
    /**
     * @SuppressWarnings(Param)
     * @return ResponseInterface
     */
    public function readResponse(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ) {
        $workstation = \App::$http->readGetResult('/workstation/', ['resolveReferences' => 1])->getEntity();
        $workstation->getUseraccount()->testRights(['ticketprinter']);

        $scopeId = Validator::value($workstation['scope']['id'] ?? null)->isNumber()->getValue();
        if (!$scopeId) {
            return Render::redirect('workstationSelect', ['error' => 'scope_missing']);
        }

        $config = \App::$config->getEntity();
        $organisation = \App::$http->readGetResult(
            '/scope/'. $scopeId .'/organisation/',
            ['resolveReferences' => 3]
        )->getEntity();

        /** @var Validator $validator */
        $validator   = $request->getAttribute('validator');
        $departments = new DepartmentList();
        $scopeIds = [];

        foreach ($organisation->departments as $departmentData) {
            $department = (new DepartmentEntity($departmentData))->withCompleteScopeList();
            $departments->addEntity($department);
            $scopeIds = array_merge($scopeIds, $department->getScopeList()->getIds());
        }

        return Render::withHtml(
            $response,
            'page/ticketprinterConfig.twig',
            array(
                'title' => 'Anmeldung an Warteschlange',
                'edit'         => 0,
                'config' => $config->getArrayCopy(),
                'parameters'   => null,
                'organisation' => $organisation->getArrayCopy(),
                'departments' => $departments->getArrayCopy(),
                'workstation' => $workstation,
                'menuActive' => 'ticketprinter',
                'sid'       => Application::getRandomHashByWorkstation($workstation),
                'scopeIds' => implode(',', $scopeIds),
                'origin'  => null,
                'success'      => $validator->getParameter('success')->isString()->getValue(),
            )
        );
    }
}
