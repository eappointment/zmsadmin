<?php
/**
 * @package Zmsadmin
 * @copyright BerlinOnline GmbH
 **/

namespace BO\Zmsadmin;

use BO\Zmsentities\Scope as ScopeEntity;

class AppointmentFormFreeProcessList extends BaseController
{
    /**
     * @SuppressWarnings(Param)
     * @return String
     */
    public function readResponse(
        \Psr\Http\Message\RequestInterface $request,
        \Psr\Http\Message\ResponseInterface $response,
        array $args
    ) {
        $workstation = \App::$http->readGetResult('/workstation/', [
            'resolveReferences' => 2,
            'gql' => Helper\GraphDefaults::getWorkstation()
        ])->getEntity();
        $validator = $request->getAttribute('validator');
        $selectedDate = $validator->getParameter('selecteddate')->isString()->getValue();
        $selectedTime = $validator->getParameter('selectedtime')->isString()->getValue();
        $selectedProcessId = $validator->getParameter('selectedprocess')->isNumber()->getValue();
        $selectedProcess = ($selectedProcessId) ?
            \App::$http->readGetResult('/process/'. $selectedProcessId .'/', [
                'gql' => Helper\GraphDefaults::getProcess()
            ])->getEntity() : null;
        $selectedScope = $validator->getParameter('selectedscope')->isNumber()->getValue();
        if ($selectedScope && $workstation->getScope()->getId() != $selectedScope) {
            $workstation->offsetSet('scope', new ScopeEntity(['id' => $selectedScope]));
        }

        $freeProcessList = Helper\AppointmentFormHelper::readFreeProcessList($request, $workstation, $selectedProcess);

        return \BO\Slim\Render::withHtml(
            $response,
            'block/appointment/freeProcessList.twig',
            array(
                'selectedDate' => $selectedDate,
                'selectedTime' => $selectedTime,
                'freeProcessList' => $freeProcessList,
                'selectedProcess' => $selectedProcess
            )
        );
    }
}
