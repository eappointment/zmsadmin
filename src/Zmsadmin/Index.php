<?php

/**
 * @package Zmsadmin
 * @copyright BerlinOnline GmbH
 **/

namespace BO\Zmsadmin;

use App;
use BO\Zmsentities\Workstation as WorkstationEntity;
use BO\Zmsentities\Useraccount as UseraccountEntity;
use BO\Zmsentities\Config as ConfigEntity;

use BO\Zmsclient\Auth;
use BO\Zmsclient\Exception;

use BO\Slim\Render;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

use BO\Zmsadmin\Helper\TwigExceptionHandler;

class Index extends BaseController
{
    /**
     * @SuppressWarnings(Param)
     * @return ResponseInterface
     */
    public function readResponse(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ) {
        try {
            $workstation = \App::$http->readGetResult('/workstation/')->getEntity();
        } catch (Exception $workstationexception) {
            $workstation = null;
            Auth::removeKey();
            Auth::removeOidcProvider();
        }
        $config = App::$config->getEntity();
        $allowedProviderList = $this->getAllowedProviderList($request, $config);

        if ($request->getMethod() === 'POST') {
            $input = $request->getParsedBody();
            $loginData = $this->testLogin($input);
            if ($loginData instanceof WorkstationEntity && $loginData->offsetExists('authkey')) {
                Auth::setKey($loginData->authkey);
                return Render::redirect('workstationSelect', array(), array());
            }
            return Render::withHtml(
                $response,
                'page/index.twig',
                array(
                'title' => 'Anmeldung gescheitert',
                'loginfailed' => true,
                'workstation' => null,
                'exception' => $loginData,
                'showloginform' => true,
                'oidcProviderList' => $allowedProviderList
                )
            );
        }
        return Render::withHtml(
            $response,
            'page/index.twig',
            array(
                'title' => 'Anmeldung',
                'config' => $config,
                'workstation' => $workstation,
                'oidcProviderList' => $allowedProviderList,
                'showloginform' => true
            )
        );
    }

    protected function testLogin(array $input)
    {
        $userAccount = new UseraccountEntity(array(
            'id' => $input['loginName'],
            'password' => $input['password'],
            'departments' => array('id' => 0) // required in schema validation
        ));
        try {
            /** @var WorkstationEntity $workstation */
            $workstation = \App::$http->readPostResult('/workstation/login/', $userAccount)->getEntity();
            return $workstation;
        } catch (Exception $exception) {
            $template = TwigExceptionHandler::getExceptionTemplate($exception);
            if ('BO\Zmsentities\Exception\SchemaValidation' == $exception->template) {
                $exceptionData = [
                  'template' => 'exception/bo/zmsapi/exception/useraccount/invalidcredentials.twig'
                ];
                $exceptionData['data']['password']['messages'] = [
                    'Der Nutzername oder das Passwort wurden falsch eingegeben'
                ];
            } elseif ('BO\Zmsapi\Exception\Useraccount\UserAlreadyLoggedIn' == $exception->template) {
                Auth::setKey($exception->data['authkey']);
                throw $exception;
            } elseif ('' != $exception->template
                && \App::$slim->getContainer()->get('view')->getLoader()->exists($template)
            ) {
                $exceptionData = [
                  'template' => $template,
                  'data' => $exception->data
                ];
            } else {
                throw $exception;
            }
        }
        return $exceptionData;
    }

    protected function getAllowedProviderList(RequestInterface $request, ConfigEntity $config)
    {
        $providerListJson = array_keys($request->getAttribute('oauthProviderList'));
        $providerListConfig = array_filter(explode(',', $config->getPreference('oidc', 'provider')));
        $allowedProviderList = array_intersect($providerListJson, $providerListConfig);
        return $allowedProviderList;
    }
}
