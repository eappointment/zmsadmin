<?php
/**
 * @copyright BerlinOnline GmbH
 **/

namespace BO\Zmsadmin;

use BO\Mellon\Validator;
use BO\Slim\Render;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class MaintenanceScheduleDelete extends BaseController
{
    /**
     * @SuppressWarnings(Param)
     * @return ResponseInterface
     */
    public function readResponse(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        $entityId = Validator::value($args['id'])->isNumber()->getValue();
        \App::$http->readDeleteResult('/maintenanceschedule/'. $entityId .'/');

        return Render::redirect('maintenance', [], ['success' => 'maintenance_schedule_delete']);
    }
}
