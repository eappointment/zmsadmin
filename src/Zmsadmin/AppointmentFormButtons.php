<?php
/**
 * @package Zmsadmin
 * @copyright BerlinOnline GmbH
 **/

namespace BO\Zmsadmin;

class AppointmentFormButtons extends BaseController
{
    /**
     * @SuppressWarnings(Param)
     * @return String
     */
    public function readResponse(
        \Psr\Http\Message\RequestInterface $request,
        \Psr\Http\Message\ResponseInterface $response,
        array $args
    ) {
        $workstation = \App::$http->readGetResult('/workstation/', [
            'resolveReferences' => 2,
            'gql' => Helper\GraphDefaults::getWorkstation()
        ])->getEntity();
        $validator = $request->getAttribute('validator');
        $selectedDate = $validator->getParameter('selecteddate')->isString()->getValue();
        $selectedTime = $validator->getParameter('selectedtime')->isString()->getValue();
        $selectedProcessId = $validator->getParameter('selectedprocess')->isNumber()->getValue();
        $selectedProcess = ($selectedProcessId) ?
            \App::$http->readGetResult('/process/'. $selectedProcessId .'/')->getEntity() : null;
        $selectedScope = $validator->getParameter('selectedscope')->isNumber()->getValue();
        $processScope  = $selectedProcess ? $selectedProcess->scope->getId() : 0;
        $selectedScope = $selectedScope ? $selectedScope : $processScope;

        $isNewAppointment = $this->isNewAppointment(
            $selectedProcess,
            $selectedDate,
            str_replace('-', ':', $selectedTime),
            $selectedScope
        );

        return \BO\Slim\Render::withHtml(
            $response,
            'block/appointment/formButtons.twig',
            array(
                'workstation' => $workstation,
                'selectedProcess' => $selectedProcess,
                'selectedScope' => $selectedScope,
                'selectedTime' => $selectedTime = ($selectedTime) ? $selectedTime : '00-00',
                'selectedDate' => ($selectedDate) ? $selectedDate : \App::$now->format('Y-m-d'),
                'isNewAppointment' => $isNewAppointment,
                'changeToSpontaneous' => $this->changesToSpontaneous($selectedProcess, $selectedTime)
            )
        );
    }

    protected function isNewAppointment($process, $selectedDate, $selectedTime, $selectedScope)
    {
        $selectedAppointment = new \BO\Zmsentities\Appointment();
        $selectedAppointment->setTime($selectedDate.' '.$selectedTime);

        if ($process
            && ($process->getFirstAppointment()->date == $selectedAppointment->date
                || $selectedTime == '00:00')
            && (!$selectedScope || $selectedScope == $process->scope->getId())
        ) {
            return false;
        }

        return true;
    }

    protected function changesToSpontaneous($process, $selectedTime)
    {
        if ($process
            && $process->getFirstAppointment()->hasTime()
            && $selectedTime == '00-00'
        ) {
            return true;
        }
        return false;
    }
}
