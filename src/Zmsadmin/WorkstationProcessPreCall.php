<?php

/**
 * @package Zmsadmin
 * @copyright BerlinOnline GmbH
 **/

namespace BO\Zmsadmin;

use App;
use BO\Mellon\Validator;
use BO\Slim\Render;
use BO\Zmsentities\RemoteCall as RemoteCallEntity;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class WorkstationProcessPreCall extends BaseController
{
    public function readResponse(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        $validator = $request->getAttribute('validator');

        $workstation = App::$http->readGetResult('/workstation/', ['resolveReferences' => 2])->getEntity();
        $workstation->testDepartmentList();
        $processId = Validator::value($args['id'])->isNumber()->getValue();
        $process = App::$http->readGetResult('/process/' . $processId . '/')->getEntity();

        $excludedIds = $validator->getParameter('exclude')->isString()->setDefault('')->getValue();
        if ($excludedIds) {
            $exclude = explode(',', $excludedIds);
        }
        $exclude[] = $process->toQueue(App::$now)->number;

        $error = $validator->getParameter('error')->isString()->getValue();
        if ($workstation->getProcess()->getId()) {
            if ($workstation->getProcess()->getId() != $processId) {
                $error = 'has_called_process';
            }
            if ('pickup' == $workstation->getProcess()->getStatus()) {
                $error = 'has_called_pickup';
            }
        }

        if ('called' == $workstation->getProcess()->getStatus()) {
            return Render::redirect(
                'workstationProcessCalled',
                ['id' => $workstation->getProcess()->getId()],
                ['error' => $error]
            );
        }

        return Render::withHtml(
            $response,
            'block/process/precall.twig',
            array(
                'title' => 'Sachbearbeiter',
                'workstation' => $workstation,
                'detailRemoteCall' => $process->getRemoteCalls()->getByName(RemoteCallEntity::TYPE_DETAIL),
                'menuActive' => 'workstation',
                'process' => $process,
                'exclude' => join(',', $exclude),
                'error' => $error
            )
        );
    }
}
