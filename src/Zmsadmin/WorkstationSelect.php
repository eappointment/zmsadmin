<?php
/**
 * @package Zmsadmin
 * @copyright BerlinOnline GmbH
 **/

namespace BO\Zmsadmin;

use BO\Zmsadmin\Helper\LoginForm;
use BO\Mellon\Validator;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class WorkstationSelect extends BaseController
{
    /**
     * @SuppressWarnings(Parameter)
     * @SuppressWarnings(CyclomaticComplexity)
     */
    public function readResponse(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        /** @var \BO\Zmsentities\Workstation $workstation */
        $workstation = \App::$http->readGetResult('/workstation/', ['resolveReferences' => 3])->getEntity();
        if (!$workstation->hasId()) {
            return \BO\Slim\Render::redirect('index', array('error' => 'login_failed'));
        }
        $workstation->testDepartmentList();

        $input = $request->getParsedBody();
        $formData = [];
        $continueSession = !Validator::param('continue')->isDeclared()->isEqualTo(true)->hasFailed();

        if (is_array($input) && (array_key_exists('scope', $input))) {
            $form = LoginForm::fromAdditionalParameters();
            $formData = $form->getStatus();
            $selectedDate = Validator::param('selectedDate')->isString()->getValue();
            $queryParams = ($selectedDate) ? array('date' => $selectedDate) : array();
            $redirect = (array_key_exists('redirect', $input)) ? $input['redirect'] : null;
            $redirectQuery = (array_key_exists('redirectQuery', $input)) ? $input['redirectQuery'] : null;
            if ($redirectQuery) {
                $queryParams['query'] = $redirectQuery;
            }
            if (! $form->hasFailed()) {
                if ($continueSession) {
                    $workstation->getUseraccount()->lastLogin = \App::$now->getTimestamp();
                }
                LoginForm::writeWorkstationUpdate($form, $workstation);
                return \BO\Slim\Render::redirect(
                    ($redirect) ? $redirect : $workstation->getVariantName(),
                    array(),
                    $queryParams
                );
            }
        }

        return \BO\Slim\Render::withHtml(
            $response,
            'page/workstationSelect.twig',
            array(
                'title' => 'Standort und Arbeitsplatz auswählen',
                'advancedData' => $formData,
                'workstation' => $workstation,
                'menuActive' => 'select',
                'today' => \App::$now->format('Y-m-d'),
                'continueSession' => $continueSession,
            )
        );
    }
}
