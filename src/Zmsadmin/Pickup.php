<?php

/**
 * @package Zmsadmin
 * @copyright BerlinOnline GmbH
 **/

namespace BO\Zmsadmin;

use App;
use BO\Slim\Render;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class Pickup extends BaseController
{
    public const DEFAULT_LIMIT = 500;

    /**
     * @SuppressWarnings(Param)
     * @param RequestInterface $request
     * @param ResponseInterface $response
     * @param array $args
     * @return ResponseInterface
     */
    public function readResponse(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        $workstation = App::$http->readGetResult('/workstation/', ['resolveReferences' => 1])->getEntity();
        $validator = $request->getAttribute('validator');
        $selectedProcess = $validator->getParameter('selectedprocess')->isString()->getValue();
        $selectedScope = $validator->getParameter('selectedscope')->isNumber()->getValue();
        $limit = $validator->getParameter('limit')->isNumber()->setDefault(self::DEFAULT_LIMIT)->getValue();
        $offset = $validator->getParameter('offset')->isNumber()->setDefault(0)->getValue();

        return Render::withHtml(
            $response,
            'page/pickup.twig',
            array(
              'title' => 'Abholer verwalten',
              'workstation' => $workstation,
              'menuActive' => 'pickup',
              'source' => 'pickup',
              'limit' => $limit,
              'offset' => $offset,
              'selectedScope' => ($selectedScope) ?: $workstation->scope['id'],
              'selectedProcess' => ($workstation->getProperty('process')->hasId()) ?
                  $workstation->getProperty('process')->getId() :
                  $selectedProcess
            )
        );
    }
}
