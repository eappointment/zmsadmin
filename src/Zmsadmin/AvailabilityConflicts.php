<?php

/**
 * @package Zmsadmin
 * @copyright BerlinOnline GmbH
 **/

namespace BO\Zmsadmin;

use App;
use BO\Slim\Render;
use BO\Zmsclient\Exception;
use BO\Zmsentities\Availability;
use BO\Zmsentities\Collection\AvailabilityList;
use DateTimeImmutable;
use DateTimeInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * Check if new Availability is in conflict with existing availability
 *
 */
class AvailabilityConflicts extends BaseController
{
    protected static DateTimeInterface $startDateTime;
    protected static DateTimeInterface $endDateTime;

    protected static Availability $selectedAvailability;

    /**
     * @SuppressWarnings(unused)
     * @throws Exception
     */
    public function readResponse(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        $validator = $request->getAttribute('validator');
        $input = $validator->getInput()->isJson()->assertValid()->getValue();
        $selectedDateTime = (new DateTimeImmutable($input['selectedDate']))->modify(App::$now->format('H:i:s'));

        static::$selectedAvailability = new Availability($input['selectedAvailability']);
        static::$startDateTime = (static::$selectedAvailability->getStartDateTime() >= App::$now) ?
            static::$selectedAvailability->getStartDateTime() : $selectedDateTime;
        static::$endDateTime = ($input['selectedAvailability']) ?
            static::$selectedAvailability->getEndDateTime() : $selectedDateTime;

        $availabilityList = static::getAffectedAvailabilityList($input);
        $conflictList = static::getConflictList($availabilityList);

        return Render::withJson(
            $response,
            $conflictList
        );
    }

    /**
     * @throws Exception
     */
    protected static function getAffectedAvailabilityList(array $input): AvailabilityList
    {
        $availabilityList = (new AvailabilityList())->addData($input['availabilityList']);
        if (isset(static::$selectedAvailability['scope']) && isset(static::$selectedAvailability['scope']['id'])) {
            $scopeId = static::$selectedAvailability->scope['id'];
            try {
                $possibleOverlaps = App::$http->readGetResult('/scope/' . $scopeId . '/availability/', [
                    'startDate' => static::$startDateTime->format('Y-m-d'),
                    'endDate' => static::$endDateTime->format('Y-m-d'),
                ])->getCollection();
                if ($possibleOverlaps->count() > 0) {
                    $availabilityList->addList($possibleOverlaps);
                }
            } catch (Exception $exception) {
                if ($exception->template != 'BO\Zmsapi\Exception\Availability\AvailabilityNotFound') {
                    throw $exception;
                }
            }
        }
        $availabilityList = static::withoutDublicates($availabilityList);
        return $availabilityList->sortByCustomStringKey('endTime');
    }

    protected static function withoutDublicates($availabilityList)
    {
        $collection = new AvailabilityList();
        foreach ($availabilityList as $entity) {
            if (! $collection->hasEntity($entity->getId())) {
                $collection->addEntity(clone $entity);
            }
        }
        return $collection;
    }

    protected static function getConflictList(
        AvailabilityList $availabilityList,
    ): array {
        $conflictedList = [];
        $conflictList = $availabilityList->getConflicts(static::$startDateTime, static::$endDateTime);
        foreach ($conflictList as $conflict) {
            $availabilityId = ($conflict->getFirstAppointment()->getAvailability()->getId()) ?
                $conflict->getFirstAppointment()->getAvailability()->getId() :
                $conflict->getFirstAppointment()->getAvailability()->tempId;
            if (! in_array($availabilityId, $conflictedList)) {
                $conflictedList[] = $availabilityId;
            }
        }

        return [
            'conflictList' => $conflictList->toConflictListByDay(),
            'conflictIdList' => (count($conflictedList)) ? $conflictedList : []
        ];
    }
}
