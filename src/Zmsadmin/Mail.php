<?php
/**
 * @package Zmsadmin
 * @copyright BerlinOnline GmbH
 **/

namespace BO\Zmsadmin;

use BO\Zmsadmin\Helper\Mail as MailHelper;

use BO\Mellon\Collection;
use BO\Mellon\Validator;

use Fig\Http\Message\RequestMethodInterface;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

use BO\Slim\Render;

use BO\Zmsclient\WorkstationRequests;

/**
 * @SuppressWarnings(Coupling)
 */
class Mail extends BaseController
{
    public static $mailHelper = null;

    /**
     * @SuppressWarnings(UnusedFormalParameter)
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function readResponse(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        $workstation = \App::$http->readGetResult('/workstation/', ['resolveReferences' => 2])->getEntity();
        $workstation->getUseraccount()->testRights(['sms']);

        $workstationRequest = new WorkstationRequests(\App::$http, $workstation);
        $department = $workstationRequest->readDepartment();
        $processId = Validator::param('selectedprocess')->isNumber()->getValue();
        $success = Validator::param('success')->isString()->getValue();
        $error = Validator::param('error')->isString()->getValue();
        $dialog = Validator::param('dialog')->isNumber()->getValue();
        $isPost = $request->getMethod() === RequestMethodInterface::METHOD_POST;
        $process = ($processId) ? \App::$http->readGetResult('/process/'. $processId .'/')->getEntity() : null;

        $validationCollection = null;
        if ($isPost) {
            $validationCollection = $this->getFormValidationCollection($request->getAttribute('validator'));
            if (!$validationCollection->hasFailed()) {
                self::$mailHelper = self::$mailHelper ?? new MailHelper();
                $query = [
                    'selectedprocess' => $process->getId(),
                    'dialog' => $dialog
                ];
                $validators = $validationCollection->getValues();
                $parameter = [
                    'message' => urldecode($validators['message']->getValue()),
                    'subject' => $validators['subject']->getValue(),
                ];
                $message = (self::$mailHelper->writeMailToQueue($process, $department, $parameter)) ?
                    ['success' => 'mail_sent'] :
                    ['error' => 'mail_failed'];

                return Render::redirect(
                    'mail',
                    [],
                    array_merge($message, $query),
                );
            }
        }

        return Render::withHtml(
            $response,
            'page/mail.twig',
            [
                'title' => 'Versand eine frei formulierbaren E-Mail',
                'menuActive' => $workstation->getVariantName(),
                'workstation' => $workstation,
                'department' => $department,
                'process' => $process,
                'success' => $success,
                'error' => $error,
                'dialog' => $dialog,
                'form' => $isPost ? $validationCollection->getStatus() : null,
                'redirect' => $workstation->getVariantName()
            ],
            ($validationCollection && $validationCollection->hasFailed()) ? 400 : 200
        );
    }

    protected function getFormValidationCollection(Validator $validator): Collection
    {
        /** @var Validator $validator */
        $collection = array();
        $collection['subject'] = $validator->getParameter('subject')->isString()
            ->isBiggerThan(2, "Es muss ein aussagekräftiger Betreff eingegeben werden");
        $collection['message'] = $validator->getParameter('message')->isString()
            ->isBiggerThan(2, "Es muss eine aussagekräftige Nachricht eingegeben werden");
        $collection = Validator::collection($collection);

        return $collection;
    }
}
