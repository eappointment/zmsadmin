<?php
/**
 * @copyright BerlinOnline GmbH
 **/

declare(strict_types=1);

namespace BO\Zmsadmin;

use App;
use BO\Slim\Render;
use BO\Zmsentities\Collection\MaintenanceScheduleList;
use BO\Zmsentities\Config;
use BO\Zmsentities\MaintenanceSchedule;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class MaintenanceList extends BaseController
{
    /**
     * @SuppressWarnings(UnusedFormalParameter)
     * @SuppressWarnings(CyclomaticComplexity)
     * @return ResponseInterface
     */
    public function readResponse(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        /** @var Config $config */
        $workstation  = \App::$http->readGetResult('/workstation/', ['resolveReferences' => 1])->getEntity();

        $scheduleList = \App::$http->readGetResult('/maintenanceschedule/')->getCollection();
        $scheduleList = $scheduleList ?? new MaintenanceScheduleList();
        $scheduleList->sortByScheduleEndTime();

        $configEntry = App::$config->getEntity()->getEntryOrDefault(Config::PROPERTY_MAINTENANCE_NEXT);
        $inSchedule = null;
        if (!empty($configEntry)) {
            $inSchedule = new MaintenanceSchedule(json_decode($configEntry, true));
        }

        $now = property_exists('\App', 'now') && isset(App::$now) ? App::$now : new \DateTime();

        foreach ($scheduleList as $key => $schedule) {
            $scheduleList[$key]['nextRun'] = null;
            if ($schedule['isRepetitive'] === true && $inSchedule
            && $inSchedule->getId() === $schedule->getId()) {
                $scheduleList[$key]['nextRun'] = $inSchedule->getStartDateTime()->format('Y-m-d, H:i');
            } elseif (!$schedule['isRepetitive']) {
                $scheduleList[$key]['nextRun'] = 'never';
                $tmpDateTime = new \DateTime($schedule['timeString']);
                if ($tmpDateTime > $now) {
                    $scheduleList[$key]['nextRun'] = $tmpDateTime->format('Y-m-d, H:i');
                }
                $scheduleList[$key]['timeString'] = $tmpDateTime->format('Y-m-d, H:i');
            }

            if ($schedule['isActive'] === true) {
                $scheduleList[$key]['nextRun'] = $scheduleList[$key]['startDateTime']->format('Y-m-d, H:i');
            }
        }

        return Render::withHtml(
            $response,
            'page/maintenanceList.twig',
            [
                'title' => 'Wartungsmodus Terminplan',
                'menuActive' => 'maintenance',
                'workstation' => $workstation,
                'scheduledMaintenance' => $scheduleList,
                'reloadInterval' => 5000,
                'success' => $request->getAttribute('validator')->getParameter('success')->isString()->getValue(),
            ]
        );
    }
}
