<?php
/**
 * @package Zmsadmin
 * @copyright BerlinOnline GmbH
 **/

namespace BO\Zmsadmin;

use BO\Zmsadmin\Helper\GraphDefaults;
use Fig\Http\Message\StatusCodeInterface;

class CounterQueueInfo extends BaseController
{
    /**
     * @SuppressWarnings(Param)
     * @return String
     */
    public function readResponse(
        \Psr\Http\Message\RequestInterface $request,
        \Psr\Http\Message\ResponseInterface $response,
        array $args
    ) {
        $validator = $request->getAttribute('validator');
        $selectedDate = $validator->getParameter('selecteddate')->isString()->getValue();
        $selectedScope = $validator->getParameter('selectedscope')->isNumber()->getValue();
        $ghostWorkstation = $validator->getParameter('ghostworkstationcount')->isNumber()->getValue();
        $workstation = \App::$http->readGetResult('/workstation/', ['resolveReferences' => 2])->getEntity();

        /** @var \BO\Zmsentities\Workstation $workstation */
        if ($selectedScope && $workstation->getScope()->getId() != $selectedScope) {
            $scope = \App::$http->readGetResult('/scope/'. $selectedScope .'/', [
                    'resolveReferences' => 1,
                    'gql' => GraphDefaults::getScope()
                ])->getEntity();
            if (!$scope) {
                return $response->withStatus(StatusCodeInterface::STATUS_NOT_FOUND, 'Not Found');
            }

            $workstation->offsetSet('scope', $scope);
        }

        if ($ghostWorkstation >= -1) {
            /** @var \BO\Zmsentities\Scope $scope */
            $scope = $workstation->getScope();
            $scope->setStatusQueue('ghostWorkstationCount', $ghostWorkstation);
            $workstation->scope = \App::$http
                ->readPostResult("/scope/$scope->id/ghostworkstation/", $scope)->getEntity();
        }
        $workstationInfo = Helper\WorkstationInfo::getInfoBoxData($workstation, $selectedDate);

        return \BO\Slim\Render::withHtml(
            $response,
            'block/queue/info.twig',
            array(
                'workstation' => $workstation,
                'workstationInfo' => $workstationInfo,
                'selectedDate' => $selectedDate
            )
        );
    }
}
