<?php

declare(strict_types=1);

namespace BO\Zmsadmin;

use App;
use BO\Mellon\Unvalidated;
use BO\Mellon\Validator;
use BO\Mellon\ValidJson;
use BO\Slim\Helper\Request;
use BO\Slim\Render;
use BO\Zmsentities\ApplicationRegister;
use BO\Zmsentities\Collection\DepartmentList;
use BO\Zmsentities\Department;
use BO\Zmsentities\Helper\Application;
use Fig\Http\Message\RequestMethodInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class CalldisplayEdit extends BaseController
{
    /**
     * @SuppressWarnings(Param)
     * @return ResponseInterface
     */
    public function readResponse(
        RequestInterface $request,
        ResponseInterface                  $response,
        array                              $args
    ) {
        /** @var \BO\Zmsentities\Workstation $workstation */
        $workstation = \App::$http->readGetResult('/workstation/', ['resolveReferences' => 1])->getEntity();
        $workstation->getUseraccount()->testRights(['ticketprinter']);

        $scopeId = Validator::value($workstation['scope']['id'] ?? null)->isNumber()->getValue();
        if (!$scopeId) {
            return Render::redirect('workstationSelect', ['error' => 'scope_missing']);
        }
        $applicationId = Validator::value($args['id'])->isString()->getValue();
        /** @var Validator $validator */
        $validator = $request->getAttribute('validator');
        $origin = $validator->getParameter('origin')->isString()->getValue();
        $error = '';

        if ($request->getMethod() === RequestMethodInterface::METHOD_POST) {
            $postData = $request->getParsedBody();
            $parameters = [];
            $parametersString = Validator::value($postData['parameters'])->isString()->getValue();
            parse_str($parametersString, $parameters);

            if (isset($parameters['collections'])) {
                try {
                    \App::$http->readPostResult(
                        '/applicationregister/' . $applicationId . '/change/',
                        ['parameters' => html_entity_decode($parametersString)]
                    );
                    return $origin ?
                        $response->withHeader('Location', $origin . '?success=asynchronous_edit') :
                        Render::redirect('calldisplay', ['success' => 'asynchronous_edit']);
                } catch (\Exception $e) {
                    $error = 'communication_error';
                }
            } else {
                $error = 'scope_missing';
            }
        }

        /** @var ApplicationRegister $application */
        $application   = \App::$http->readGetResult('/applicationregister/' . $applicationId . '/')->getEntity();
        $organisation  = \App::$http->readGetResult(
            '/scope/'. $scopeId .'/organisation/',
            ['resolveReferences' => 3]
        )->getEntity();

        $parameters = [];
        parse_str($application->parameters, $parameters);
        if (isset($parameters['collections']['scopelist'])) {
            $parameters['collections']['scopelist'] = explode(',', $parameters['collections']['scopelist']);
        }
        if (isset($parameters['collections']['clusterlist'])) {
            $parameters['collections']['clusterlist'] = explode(',', $parameters['collections']['clusterlist']);
        }

        $departments = new DepartmentList();
        $scopeIds = [];

        foreach ($organisation->departments as $departmentData) {
            $department = (new Department($departmentData))->withCompleteScopeList();
            $departments->addEntity($department);
            $scopeIds = array_merge($scopeIds, $department->getScopeList()->getIds());
        }

        return Render::withHtml(
            $response,
            'page/calldisplay.twig',
            [
                'title'        => 'Aufrufanzeige (ID: ' . $application->getId() . ') ändern -',
                'edit'         => 1,
                'config'       => App::$config->getEntity()->getArrayCopy(),
                'parameters'   => $parameters,
                'organisation' => $organisation->getArrayCopy(),
                'departments'  => $departments->getArrayCopy(),
                'workstation'  => $workstation,
                'menuActive'   => 'calldisplay',
                'displayId'    => $application->getId(),
                'scopeIds'     => implode(',', $scopeIds),
                'origin'       => $origin,
                'error'        => $error,
            ]
        );
    }
}
