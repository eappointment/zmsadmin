<?php

/**
 * @package Zmsadmin
 * @copyright BerlinOnline GmbH
 **/

namespace BO\Zmsadmin;

use App;
use BO\Mellon\Validator;
use BO\Slim\Render;
use BO\Zmsentities\RemoteCall as RemoteCallEntity;
use BO\Zmsentities\Process as ProcessesEntity;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class WorkstationProcessCalled extends BaseController
{
    public function readResponse(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        $validator = $request->getAttribute('validator');
        $workstation = App::$http->readGetResult('/workstation/', ['resolveReferences' => 2])->getEntity();
        $processId = Validator::value($args['id'])->isNumber()->getValue();
        if (! $workstation->getProcess()->hasId() && ! $workstation->getProcess()->queue->callTime) {
            $process = App::$http->readGetResult('/process/' . $args['id'] . '/')->getEntity();
            $workstation = App::$http->readPostResult('/workstation/process/called/', $process, [
                'allowClusterWideCall' => App::$allowClusterWideCall
            ])->getEntity();
        }
        $process = $workstation->getProcess();
        $excludedIds = $validator->getParameter('exclude')->isString()->setDefault('')->getValue();
        if ($excludedIds) {
            $exclude = explode(',', $excludedIds);
        }
        $exclude[] = $process->toQueue(App::$now)->number;

        $error = $validator->getParameter('error')->isString()->getValue();
        if (isset($processId) && $process->getId() != $processId) {
            $error = ('pickup' == $process->getStatus()) ?
                'has_called_pickup' :
                'has_called_process';
        }

        if ($process->getStatus() == 'processing') {
            return Render::redirect('workstationProcessProcessing', [], ['error' => $error]);
        }

        return Render::withHtml(
            $response,
            'block/process/called.twig',
            array(
                'title' => 'Sachbearbeiter',
                'workstation' => $workstation,
                'detailRemoteCall' => $process->getRemoteCalls()->getByName(RemoteCallEntity::TYPE_DETAIL),
                'menuActive' => 'workstation',
                'exclude' => join(',', $exclude),
                'error' => $error
            )
        );
    }
}
