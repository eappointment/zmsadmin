<?php
/**
 * @package zmsadmin
 * @copyright BerlinOnline GmbH
 **/

namespace BO\Zmsadmin;

use BO\Zmsclient\Exception;
use BO\Zmsentities\Scope as Entity;
use BO\Mellon\Validator;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

/**
  * Handle requests concerning services
  *
  */
class DepartmentAddScope extends Scope
{
    public function readResponse(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        $workstation = \App::$http->readGetResult('/workstation/', ['resolveReferences' => 2])->getEntity();
        $departmentId = Validator::value($args['id'])->isNumber()->getValue();
        $department = \App::$http
            ->readGetResult('/department/'. $departmentId .'/', ['resolveReferences' => 1])->getEntity();
        $organisation = \App::$http->readGetResult('/department/' . $departmentId . '/organisation/')->getEntity();
        $providerList = Helper\ProviderHandler::readProviderList($workstation->getScope()->getSource());
        $sourceList = $this->readSourceList();
        $input = $request->getParsedBody();

        if (is_array($input) && array_key_exists('save', $input)) {
            $result = $this->testUpdateEntity($input, $department->id);
            if ($result instanceof Entity) {
                try {
                    $this->writeUploadedImage($request, $result->id, $input);
                    return \BO\Slim\Render::redirect('scope', ['id' => $result->id], [
                        'success' => 'scope_created'
                    ]);
                } catch (Exception $clientException) {
                    $result = ['data' => $clientException->data];
                } catch (\InvalidArgumentException $exception) {
                    $result = ['data' => ['content' => ['messages' => [$exception->getMessage()]]]];
                }
            }
        }
        return \BO\Slim\Render::withHtml($response, 'page/scope.twig', array(
            'title' => 'Standort',
            'action' => 'add',
            'scope'  => $input,
            'menuActive' => 'owner',
            'workstation' => $workstation,
            'organisation' => $organisation,
            'department' => $department,
            'sourceList' => $sourceList,
            'exception' => (isset($result)) ? $result : null,
            'providerList' => $providerList
        ));
    }
}
