<?php
/**
 * This file is part of the eappointment/eappointment library
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 */

namespace BO\Zmsadmin\Middleware\OAuth\Interfaces;

use League\OAuth2\Client\Provider\AbstractProvider;
use League\OAuth2\Client\Token\AccessToken;

use BO\Zmsadmin\Exception\OAuth\OAuthFailed as FailedException;

use Psr\Http\Message\ServerRequestInterface;

/**
 * Classes implementing `OAuthInterface` may be used to represent
 * the service provider.
 */
interface OAuthProviderInterface
{
    /**
     * Returns the oidc provider.
     *
     * @return AbstractProvider
     */
    public function getProvider();

    /**
     * Sets the oidc provider.
     *
     * @return self
     */
    public function setProvider();

    /**
     * Returns the oidc provider.
     *
     * @return array
     */
    public function getOptions();

    /**
     * Sets the oidc provider.
     *
     * @param  array $options
     * @return self
     */
    public function setOptions(array $options);

     /**
     * set the authorization code.
     *
     * @param  ServerRequestInterface $request
     * @return self
     */
    public function setAuthCode(ServerRequestInterface $request);

     /**
     * get the authorization code.
     *
     * @return string
     */
    public function getAuthCode();

    /**
     * Returns the access token.
     *
     * @return AccessToken
     * @throws \Exception|FailedException
     */
    public function getAccessToken();

    /**
     * Requests and returns the resource owner data of given access token.
     *
     * @param  AccessToken $accessToken
     * @param  Array $grantOptions
     * @return Array
     */
    public function getResourceOwnerData(AccessToken $accessToken, array $grantOptions);
}
