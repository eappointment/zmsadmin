<?php

namespace BO\Zmsadmin\Middleware\OAuth;

use App;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Fig\Http\Message\StatusCodeInterface;

use BO\Zmsclient\Auth;
use BO\Zmsclient\Exception;
use BO\Zmsclient\SessionHandler;

use BO\Zmsadmin\Exception\OAuth\OAuthInvalid as OAuthInvalidException;
use Psr\Http\Message\ResponseInterface;

class OAuthLoginService
{
    /**
     * request variable
     *
     * @var ServerRequestInterface|null
     */
    protected $request = null;

    /**
     * providerList variable
     *
     * @var array
     */
    protected $providerList = [];

    /**
     * Set the authorizsationType attribute to request and init authorization method
     *
     * @param ServerRequestInterface $request PSR7 request
     * @param RequestHandlerInterface $next Next middleware
     *
     * @return ResponseInterface
     */
    public function __invoke(
        ServerRequestInterface $request,
        RequestHandlerInterface $next
    ) {
        $oAuthService = new OAuthService();
        $request = $oAuthService->setProviderlistToRequest($request);
        $request = $oAuthService->setSelectedOauthProvider($request);
        $oauthProvider = $request->getAttribute('oauthProvider');
        $oauthProvider->setAuthCode($request);
        $response = $next->handle($request);
        $response = (! $oauthProvider->getAuthcode() && ! Auth::getKey()) ?
            $this->handleAuthorization($response, $oauthProvider) :
            $this->handleLogin($response, $oauthProvider);
        return $response;
    }

    /**
     * handle Authorization
     *
     * @param  ResponseInterface $response
     * @param  OAuthProvider $oauthProvider
     * @return ResponseInterface $response
     * @throws OAuthInvalidException
     */
    protected function handleAuthorization(
        ResponseInterface $response,
        OAuthProvider $oauthProvider
    ): ResponseInterface {
        $authUrl = $oauthProvider->getProvider()->getAuthorizationUrl();
        Auth::setOidcProvider($oauthProvider->getOptions()['providerName']);
        Auth::setKey($oauthProvider->getProvider()->getState());
        return $response->withRedirect($authUrl, StatusCodeInterface::STATUS_FOUND);
    }

    /**
     * handle Login
     *
     * @param  ResponseInterface $response
     * @param  OAuthProvider $oauthProvider
     * @return ResponseInterface $response
     * @throws Exception
     */
    protected function handleLogin(
        ResponseInterface $response,
        OAuthProvider $oauthProvider
    ): ResponseInterface {
        $config = App::$config->getEntity();
        $accessToken = $oauthProvider->getAccessToken();
        $grantOptions = ($config->toProperty()->oidc->get()) ? $config->toProperty()->oidc->get() : [];
        $ownerInputData = $oauthProvider->getResourceOwnerData($accessToken, $grantOptions);

        $options = $oauthProvider->getOptions();
        $sessionHandler = (new SessionHandler(App::$http));
        $sessionHandler->setParams(['oidc' => 1]);
        $sessionHandler->open('/'. $options['realm'] . '/', $options['clientName']);

        try {
            $sessionHandler->destroy(Auth::getKey());
            $sessionHandler->write(Auth::getKey(), serialize($accessToken));
            App::$http
                ->readPostResult('/workstation/oauth/', $ownerInputData, ['state' => Auth::getKey()])
                ->getEntity();
        } catch (\Exception $exception) {
            $sessionHandler->destroy(Auth::getKey());
            Auth::removeKey();
            Auth::removeOidcProvider();
            if ('BO\Zmsapi\Exception\Useraccount\UserAlreadyLoggedIn' == $exception->template) {
                Auth::setKey($exception->data['authkey']);
                throw $exception;
            }
        }
        $sessionHandler->close();
        return $response;
    }
}
