<?php

namespace BO\Zmsadmin\Middleware\OAuth\Provider;

use Stevenmaguire\OAuth2\Client\Provider\Keycloak;

use BO\Zmsclient\Psr7\ClientInterface as HttpClientInterface;
use BO\Zmsclient\PSR7\Client;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

use Fig\Http\Message\StatusCodeInterface;

use League\OAuth2\Client\Token\AccessToken;

/**
 * @SuppressWarnings(unused)
 */
class KeycloakProvider extends Keycloak
{
    /**
     * Sets the HTTP client instance.
     *
     * @param  HttpClientInterface $client
     * @return parent
     */

     /**
     * Authentification options
     *
     * @var array
     */
    protected $options = [];
    
    public function __construct(array $options, $client = null)
    {
        $this->options = $options;
        $client = ((null === $client)) ? new Client() : $client;
        return parent::__construct($options, ['httpClient' => $client]);
    }
    
    /**
     * Sets the HTTP client instance.
     *
     * @param  HttpClientInterface $client
     * @return self
     */
    public function setHttpClient($client)
    {
        $this->httpClient = $client;
        return $this;
    }

    /**
     * Sends a request instance and returns a response instance.
     *
     * WARNING: This method does not attempt to catch exceptions caused by HTTP
     * errors! It is recommended to wrap this method in a try/catch block.
     *
     * @param  RequestInterface $request
     * @return ResponseInterface
     */
    public function getResponse(RequestInterface $request)
    {
        return $this->getHttpClient()->readResponse($request);
    }

     /**
     * Requests an access token using a specified grant and option set.
     *
     * @param  mixed $grant
     * @param  array $options
     * @throws IdentityProviderException
     * @return AccessTokenInterface
     */
    public function getAccessToken($grant, array $options = [])
    {
        $grant = $this->verifyGrant($grant);
        $params = [
            'client_id'     => $this->clientId,
            'client_secret' => $this->clientSecret,
            'redirect_uri'  => $this->redirectUri,
        ];

        $params   = $grant->prepareRequestParameters($params, $options);
        $request  = $this->getAccessTokenRequest($params);

        $response = $this->getParsedResponse($request);

        $prepared = $this->prepareAccessTokenResponse($response);
        $token    = $this->createAccessToken($prepared, $grant);

        return $token;
    }

    /**
     * Builds the revoke URL.
     *
     * @param ResponseInterface $response
     * @param AccessToken|null $token
     * @return ResponseInterface $response
     */
    public function getRevokeResponse(
        ResponseInterface $response,
        AccessToken $accessToken = null
    ) {
        $url = $this->getLogoutUrl(['redirect_uri' => $this->options['logoutUri']]);
        return $response->withRedirect($url, StatusCodeInterface::STATUS_FOUND);
    }

    /**
     * Get the default scopes used by this provider.
     *
     * This should not be a complete list of all scopes, but the minimum
     * required for the provider user interface!
     *
     * @return string[]
     */
    protected function getDefaultScopes()
    {
        return ['profile', 'email', 'openid'];
    }
}
