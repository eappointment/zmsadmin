<?php
namespace BO\Zmsadmin\Middleware\OAuth;

use Psr\Http\Message\ServerRequestInterface;

use League\OAuth2\Client\Provider\AbstractProvider;
use League\OAuth2\Client\Token\AccessToken;

use BO\Zmsadmin\Exception\OAuth\OAuthFailed as FailedException;
use BO\Zmsadmin\Exception\OAuth\OAuthPreconditionFailed as PreconditionFailedException;

class OAuthProvider implements Interfaces\OAuthProviderInterface
{
    /**
     * oidc provider that extends AbstractProvider from league/oauth2-client
     *
     * @var AbstractProvider|null
     */
    protected $provider = null;

    /**
     * http client
     *
     * @var HttpClientInterface|null
     */
    protected $httpClient = null;

    /**
     * authentification code
     *
     * @var string
     */
    protected $authCode = '';

    /**
     * authentification state
     *
     * @var string
     */
    protected $state = '';

    /**
     * oidc provider options like clientId, clientSecret, clientName, authServerurl, redirectUri, logoutUri, realm
     *
     * @var array
     */
    protected $options = [];

    public function __construct(array $options)
    {
        $this->setOptions($options);
        $this->setProvider();
    }

    public function getProvider()
    {
        return $this->provider;
    }

    public function setProvider(AbstractProvider $provider = null)
    {
        if ($provider === null) {
            $providerClass = $this->options['source'];
            $provider = new $providerClass($this->options);
        }
        $this->provider = $provider;
        return $this;
    }

    public function setOptions(array $options)
    {
        $this->options = $options;
        return $this;
    }

    public function getOptions()
    {
        return $this->options;
    }

    public function setAuthCode(ServerRequestInterface $request, string $authCode = '')
    {
        if ($request->getParam("code") && $authCode === '') {
            $this->authCode = $request->getParam("code");
        }
        if ($authCode !== '') {
            $this->authCode = $authCode;
        }
        return $this;
    }

    public function getAuthCode()
    {
        return $this->authCode;
    }

    public function getAccessToken()
    {
        try {
            return $this->getProvider()->getAccessToken('authorization_code', ['code' => $this->authCode]);
        } catch (\Exception $exception) {
            if ('League\OAuth2\Client\Provider\Exception\IdentityProviderException' === get_class($exception)) {
                throw new FailedException();
            }
            throw $exception;
        }
    }

    /**
     * Requests and returns a refreshed access token by existing expired access token.
     *
     * @param  array $accessToken
     * @return AccessToken|NULL $accessToken
     * @throws Exception
     */
    public function getRefreshedAccessToken(array $accessTokenData = [])
    {
        try {
            $accessToken = new AccessToken($accessTokenData);
            if ($accessToken && $accessToken->hasExpired()) {
                $refreshedAccessToken = $this->getProvider()->getAccessToken('refresh_token', [
                    'refresh_token' => $accessToken->getRefreshToken()
                ]);
            }
            return ($refreshedAccessToken) ? $refreshedAccessToken : null;
        } catch (\Exception $exception) {
            throw $exception;
        }
    }

    /**
     * Requests and returns the resource owner data of given access token.
     *
     * @param  AccessToken $accessToken
     * @param  Array $grantOptions
     * @return Array
     */
    public function getResourceOwnerData(AccessToken $accessToken, array $grantOptions = [])
    {
        $resourceOwner = $this->getProvider()->getResourceOwner($accessToken);
        $ownerData = (new OAuthResourceOwner(
            $resourceOwner,
            $this->options['providerName'],
            $grantOptions
        )
        )->getOwnerData();
        $this->testAccessByOwnerData($ownerData);
        return $ownerData;
    }

     /**
     * test ownerdata for existing data
     *
     * @param  array $ownerInputData
     * @throws PreconditionFailedException
     */
    private function testAccessByOwnerData(array $ownerInputData)
    {
        $hasMail = (\array_key_exists('email', $ownerInputData) && $ownerInputData['email'] !== null);
        if (! $hasMail) {
            throw new PreconditionFailedException();
        }
    }
}
