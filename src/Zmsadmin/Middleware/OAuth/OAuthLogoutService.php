<?php

namespace BO\Zmsadmin\Middleware\OAuth;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

use League\OAuth2\Client\Token\AccessToken;

use BO\Zmsclient\Auth;
use BO\Zmsclient\Exception;
use BO\Zmsclient\SessionHandler;

class OAuthLogoutService
{
    /**
     * Set the authorizsationType attribute to request and init authorization method
     *
     * @param ServerRequestInterface $request PSR7 request
     * @param RequestHandlerInterface $next Next middleware
     *
     * @return ResponseInterface
     */
    public function __invoke(
        ServerRequestInterface $request,
        RequestHandlerInterface $next
    ) {
        $oAuthService = new OAuthService();
        $request = $oAuthService->setProviderlistToRequest($request);
        $request = $oAuthService->setSelectedOauthProvider($request);
        $oauthProvider = $request->getAttribute('oauthProvider');
        
        $response = $next->handle($request);
        $response = ($oauthProvider && ! $request->getParam("state")) ?
            $this->handleLogout($response, $oauthProvider) :
            $response;
        return $response;
    }

    /**
     * handle logout from oidc provider
     *
     * @param  OAuthProvider $oauthProvider
     * @return ResponseInterface
     * @throws Exception
     */
    public function handleLogout(ResponseInterface $response, OAuthProvider $oauthProvider)
    {
        $options = $oauthProvider->getOptions();
        $sessionHandler = (new SessionHandler(\App::$http));
        $sessionHandler->open('/'. $options['realm'] . '/', $options['clientName']);
        $sessionHandler->setParams(['oidc' => 1]);
        $accessTokenData = unserialize($sessionHandler->read(Auth::getKey()));
        $accessTokenData = (is_array($accessTokenData)) ? $accessTokenData : [];
        $accessToken = new AccessToken($accessTokenData);
        $sessionHandler->destroy(Auth::getKey());
        $sessionHandler->close();
        $response = $oauthProvider->getProvider()->getRevokeResponse($response, $accessToken);
        return $response;
    }
}
