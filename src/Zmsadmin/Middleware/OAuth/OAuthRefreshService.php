<?php

namespace BO\Zmsadmin\Middleware\OAuth;

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

use BO\Zmsclient\Auth;
use BO\Zmsclient\SessionHandler;

class OAuthRefreshService
{
    /**
     * Set the authorizsationType attribute to request and init authorization method
     *
     * @param ServerRequestInterface $request PSR7 request
     * @param RequestHandlerInterface $next Next middleware
     *
     * @return ResponseInterface
     */
    public function __invoke(
        ServerRequestInterface $request,
        RequestHandlerInterface $next
    ) {
        $oAuthService = new OAuthService();
        $request = $oAuthService->setProviderlistToRequest($request);
        $request = $oAuthService->setSelectedOauthProvider($request);
        $oauthProvider = $request->getAttribute('oauthProvider');
        $response = $next->handle($request);

        if ($oauthProvider) {
            try {
                $this->handleRefresh($oauthProvider);
            } catch (\Exception $exception) {
                $response = (new OAuthLogoutService)->__invoke($request, $next);
            }
        }
        return $response;
    }

    /**
     * handle refresh
     *
     * @param  OAuthProvider $oauthProvider
     * @throws Exception
     */
    public function handleRefresh(OAuthProvider $oauthProvider)
    {
        $options = $oauthProvider->getOptions();
        $sessionHandler = (new SessionHandler(\App::$http));
        $sessionHandler->open('/'. $options['realm'] . '/', $options['clientName']);
        $sessionHandler->setParams(['oidc' => 1]);
        $accessTokenData = unserialize($sessionHandler->read(Auth::getKey()));
        $accessTokenData = (is_array($accessTokenData)) ? $accessTokenData : [];
        $refreshedAccessToken = $oauthProvider->getRefreshedAccessToken($accessTokenData);
        if ($refreshedAccessToken !== null) {
            $sessionHandler->destroy(Auth::getKey());
            $sessionHandler->write(Auth::getKey(), serialize($refreshedAccessToken));
        }
        $sessionHandler->close();
    }
}
