<?php
/**
 * @package Zmsadmin
 * @copyright BerlinOnline GmbH
 **/

namespace BO\Zmsadmin;

use BO\Slim\Render;

use BO\Zmsclient\Exception as ClientException;

use BO\Mellon\Validator;

use BO\Zmsentities\Process as ProcessEntity;

use BO\Zmsadmin\Exception\Process\ProcessNotPending as NotPendingException;

use Psr\Http\Message\RequestInterface;

use Psr\Http\Message\ResponseInterface;

/**
  *
  */
class PickupCall extends BaseController
{
    const TEMPLATE_PATH = 'block/pickup/called.twig';
    const EXCEPTION_TEMPLATE = 'BO\Zmsapi\Exception\Process\ProcessByQueueNumberNotFound';

    /**
     * @SuppressWarnings(Param)
     * @param RequestInterface $request
     * @param ResponseInterface $response
     * @return ResponseInterface
     */
    public function readResponse(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        $workstation = \App::$http->readGetResult('/workstation/', ['resolveReferences' => 2])->getEntity();
        $inputNumber = Validator::value($args['id'])->isNumber()->getValue();
        $process = $this->readSelectedProcess($workstation, $inputNumber);
        if (ProcessEntity::STATUS_PENDING != $process->getStatus() &&
            ProcessEntity::STATUS_PICKUP != $process->getStatus()
        ) {
            throw new NotPendingException();
        }
        $process->setCallTime(\App::$now);
        $process->queue['callCount']++;
        $workstation->process = \App::$http->readPostResult('/process/status/pickup/', $process)->getEntity();

        return Render::withHtml(
            $response,
            static::TEMPLATE_PATH,
            [
                'workstation' => $workstation,
            ]
        );
    }

    protected function readSelectedProcess($workstation, $inputNumber)
    {
        try {
            if (4 <= strlen((string)$inputNumber)) {
                $process = \App::$http
                    ->readGetResult('/process/'. $inputNumber .'/')
                    ->getEntity();
                $workstation->testMatchingProcessScope($workstation->getScopeList(), $process);
            } else {
                $process = \App::$http
                    ->readGetResult('/scope/'. $workstation->scope['id'] .'/queue/'. $inputNumber .'/')
                    ->getEntity();
            }
        } catch (ClientException $exception) {
            if ($exception->template == self::EXCEPTION_TEMPLATE) {
                $process = new ProcessEntity();
                $process->scope['id'] = $workstation->scope['id'];
                $process->queue['number'] = $inputNumber;
                $process->amendment = 'Über die direkte Nummerneingabe angelegter Abholer.';
                $process = \App::$http->readPostResult('/process/status/pickup/', $process)->getEntity();
            } else {
                throw $exception;
            }
        }
        return $process;
    }
}
