<?php

/**
 *
 * @package Zmsadmin
 * @copyright BerlinOnline GmbH
 *
 */

namespace BO\Zmsadmin;

use App;
use BO\Slim\Render;
use BO\Zmsadmin\Helper\GraphDefaults;
use BO\Zmsentities\Collection\UseraccountList;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class Useraccount extends BaseController
{
    /**
     * @SuppressWarnings(Param)
     * @param RequestInterface $request
     * @param ResponseInterface $response
     * @param array $args
     * @return ResponseInterface
     */
    public function readResponse(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        $workstation = \App::$http->readGetResult('/workstation/', ['resolveReferences' => 2])->getEntity();
        $success = $request->getAttribute('validator')->getParameter('success')->isString()->getValue();
        $ownerList = App::$http->readGetResult('/owner/', [
            'resolveReferences' => 2,
            'gql' => GraphDefaults::getOwnerForUseraccountList()
        ])->getCollection();

        if ($workstation->hasSuperUseraccount()) {
            $collection = App::$http->readGetResult("/useraccount/", [
                'resolveReferences' => 0,
                'type' => 'systemwide',
                'gql' => GraphDefaults::getUserForUseraccountList()
            ])->getCollection();
        } else {
            $departmentList = $workstation->getUseraccount()->getDepartmentList();
            $collection = new UseraccountList();
            foreach ($departmentList as $accountDepartment) {
                $accountUserList = App::$http
                    ->readGetResult("/department/$accountDepartment->id/useraccount/", [
                        "resolveReferences" => 0,
                        'gql' => GraphDefaults::getUserForUseraccountList()
                    ])->getCollection();
                if ($accountUserList) {
                    $collection = $collection->addList($accountUserList)->withoutDublicates();
                }
            }
        }

        return Render::withHtml(
            $response,
            'page/useraccount.twig',
            array(
                'title' => 'Nutzer',
                'menuActive' => 'useraccount',
                'workstation' => $workstation,
                'useraccountList' => $collection->sortByCustomStringKey('id'),
                'ownerlist' => $ownerList,
                'success' => $success,
            )
        );
    }
}
