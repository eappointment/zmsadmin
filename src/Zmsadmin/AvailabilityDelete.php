<?php
/**
 * @package Zmsadmin
 * @copyright BerlinOnline GmbH
 **/

namespace BO\Zmsadmin;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use BO\Slim\Render;

use BO\Mellon\Validator;

/**
 * Delete availability, API proxy
 *
 */
class AvailabilityDelete extends BaseController
{
    /**
     * @SuppressWarnings(UnusedFormalParameter)
     * @return ResponseInterface
     */
    public function readResponse(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        $entityId = Validator::value($args['id'])->isNumber()->getValue();
        $availability = \App::$http->readDeleteResult('/availability/' . $entityId . '/')->getEntity();
        $response = Render::withLastModified($response, time(), '0');
        return Render::withJson($response, $availability);
    }
}
