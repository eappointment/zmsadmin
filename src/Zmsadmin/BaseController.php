<?php
/**
 * @package Zmsadmin
 * @copyright BerlinOnline GmbH
 **/

namespace BO\Zmsadmin;

use App;
use BO\Slim\Render;
use BO\Zmsadmin\Helper\Maintenance;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * @SuppressWarnings(NumberOfChildren)
 *
 */
abstract class BaseController extends \BO\Slim\Controller
{
    public function __invoke(RequestInterface $request, ResponseInterface $response, array $args)
    {
        $request = $this->initRequest($request);
        $noCacheResponse = Render::withLastModified($response, time(), '0');

        App::getConfigService();
        $maintenanceResponse = $this->handleMaintenanceMode($request, $response);
        if ($maintenanceResponse instanceof ResponseInterface) {
            return $maintenanceResponse;
        }

        return $this->readResponse($request, $noCacheResponse, $args);
    }

    /**
     * @codeCoverageIgnore
     *
     */
    public function readResponse(RequestInterface $request, ResponseInterface $response, array $args)
    {
        return parent::__invoke($request, $response, $args);
    }

    public function getSchemaConstraintList($schema)
    {
        $list = [];
        $locale = \App::$language->getLocale();
        foreach ($schema->properties as $key => $property) {
            if (isset($property['x-locale'])) {
                $constraints = $property['x-locale'][$locale];
                if ($constraints) {
                    $list[$key]['description'] = $constraints['messages'];
                }
            }
        }
        return $list;
    }

    /**
     * @param RequestInterface $request
     * @param ResponseInterface $response
     * @return ResponseInterface|void
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    private function handleMaintenanceMode(RequestInterface $request, ResponseInterface $response)
    {
        /** @var Maintenance $maintenance */
        $container   = App::$slim->getContainer();
        $maintenance = $container->has('maintenance') ? $container->get('maintenance') : null;

        if ($maintenance && $maintenance->getStart()) {
            if ($maintenance->getActiveEntity()
                && $maintenance->getStart()->getTimestamp() <= time()
                && $maintenance::isBlockedRequest($request)
            ) {
                return $maintenance->renderMaintenanceResponse($response, $request);
            } else {
                App::$maintenance = $maintenance->getMaintenanceData();
            }
        }
    }

    /**
     * @return WorkstationEntity
     * @throws UserAccountMissingLogin
     * @throws UserAccountMissingRights
     */
    protected function getWorkstationWithTestedAccess(): WorkstationEntity
    {
        $resolveParams['resolveReferences'] = $this->resolveLevel;
        if ($this->gqlWorkstation) {
            $resolveParams['gql'] = GraphDefaults::defaultFormat($this->gqlWorkstation);
        }
        $workstation = App::$http->readGetResult('/workstation/', $resolveParams)->getEntity();
        $application = new ApplicationEntity(['name' => App::APP_NAME]);
        $this->testRights[] = new EntityAccess($application);
        $workstation->getUseraccount()->testRights($this->testRights);
        return $workstation;
    }
}
