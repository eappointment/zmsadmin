<?php

/**
 * @copyright BerlinOnline GmbH
 **/

declare(strict_types=1);

namespace BO\Zmsadmin\Helper;

use App;
use BO\Zmsadmin\Exception\MailFromMissing as MailFromMissingException;
use BO\Zmsentities\Department as DepartmentEntity;
use BO\Zmsentities\Mail as MailEntity;
use BO\Zmsentities\Process as ProcessEntity;
use BO\Zmsentities\Scope as ScopeEntity;

class Mail
{
    public function writeMailToQueue(
        ProcessEntity $process,
        DepartmentEntity $department,
        array $parameter,
        $copyToAdmin = true
    ): ?MailEntity {
        if (! $process->scope->hasEmailFrom()) {
            throw new MailFromMissingException();
        }
        $mailEntity = (new MailEntity)
            ->toCustomMessageEntity($process, App::$config->getEntity(), $parameter)
            ->withDepartment($department);

        if ($this->writeMail($mailEntity)) {
            if ($copyToAdmin) {
                $this->writeAdminMailCopy($process->scope, $mailEntity);
            }
            return $mailEntity;
        }
        return null;
    }

    protected function writeAdminMailCopy(ScopeEntity $scope, $mailEntity): bool
    {
        if ($scope->getContactEmail()) {
            $adminMailEntity = clone $mailEntity;
            $adminMailEntity->subject = 'Kopie - '. $mailEntity->subject;
            $adminMailEntity->process->getFirstClient()['familyName'] = $scope->getName();
            $adminMailEntity->process->getFirstClient()['email'] = $scope->getContactEmail();
            $this->writeMail($adminMailEntity, false);
        }
        return true;
    }

    protected function writeMail(MailEntity $entity, bool $countMailSent = true): ?MailEntity
    {
        try {
            $result = \App::$http->readPostResult('/mails/', $entity, ['count' => $countMailSent])->getEntity();
        } catch (\Exception $exception) {
            $result = null;
        }
        return $result;
    }
}
