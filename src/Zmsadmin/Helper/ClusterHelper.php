<?php
/**
 *
 * @package Zmsappointment
 * @copyright BerlinOnline GmbH
 *
 */
namespace BO\Zmsadmin\Helper;

use BO\Zmsentities\Collection\ScopeList;

class ClusterHelper
{
    protected static $cluster = null;

    protected static $workstation = null;

    public function __construct(\BO\Zmsentities\Workstation $workstation)
    {
        static::$workstation = $workstation;
        if ($workstation->getScope()->getId()) {
            static::$cluster = \App::$http->readGetResult(
                '/scope/' . $workstation->getScope()->getId() . '/cluster/'
            )->getEntity();
        }
    }

    public static function getEntity()
    {
        return static::$cluster;
    }

    public static function getScopeList($default = null)
    {
        if (!$default) {
            $default = new ScopeList();
        }
        if (static::$cluster === null || !static::$cluster->getId()) {
            return $default;
        }
        return static::$workstation->getScopeList(static::$cluster);
    }

    public static function getProcessList($selectedDate)
    {
        $result = null;
        if (static::isClusterEnabled() && static::$cluster) {
            $result = \App::$http
                ->readGetResult(
                    '/cluster/'. static::$cluster->id .'/process/'. $selectedDate .'/',
                    ['resolveReferences' => 1, 'gql' => GraphDefaults::getProcess()]
                );
        } elseif (static::$workstation
            && static::$workstation->getScope()
            && static::$workstation->getScope()->getId()
        ) {
            $result = \App::$http
                ->readGetResult(
                    '/scope/'. static::$workstation->getScope()->getId() .'/process/'. $selectedDate .'/',
                    ['resolveReferences' => 1, 'gql' => GraphDefaults::getProcess()]
                );
        }
        return ($result) ? $result->getCollection() : new \BO\Zmsentities\Collection\ProcessList();
    }

    public static function getNextProcess($excludedIds)
    {
        $queueList = static::getProcessList(\App::$now->format('Y-m-d'))
            ->toQueueList(\App::$now)
            ->withoutStatus(['fake','missed']);
        $excludedIds = (1 < $queueList->count()) ? $excludedIds : '';

        if (1 > $queueList->count()) {
            return new \BO\Zmsentities\Process();
        }
        if (static::isClusterEnabled()) {
            $nextProcess =  \App::$http->readGetResult(
                '/cluster/'. static::$cluster['id'] .'/queue/next/',
                [
                    'exclude' => $excludedIds,
                    'allowClusterWideCall' => \App::$allowClusterWideCall
                ]
            )->getEntity();
        } else {
            $nextProcess = \App::$http->readGetResult(
                '/scope/'. static::$workstation->scope['id'] .'/queue/next/',
                ['exclude' => $excludedIds]
            )->getEntity();
        }
        
        
        return ($nextProcess) ? $nextProcess : new \BO\Zmsentities\Process();
    }

    public static function isClusterEnabled()
    {
        return (static::$workstation->queue['clusterEnabled'] && static::$cluster);
    }
}
