<?php

/**
 *
 * @package Zmsadmin
 * @copyright BerlinOnline GmbH
 *
 */

namespace BO\Zmsadmin\Helper;

use App;
use BO\Zmsclient\Http as ZmsclientHttp;
use BO\Zmsentities\Config as Entity;
use InvalidArgumentException;
use Psr\Http\Message\RequestInterface;

class Config
{
    protected Entity $entity;

    public static ?ZmsclientHttp $httpClient;

    protected string $gqlFilter;

    public function __construct()
    {
        $this->gqlFilter = GraphDefaults::getConfig();
    }

    public function setGQLFilter(string $gqlFilter): Config
    {
        if (empty($gqlFilter)) {
            throw new InvalidArgumentException("The parameter passed must not be empty");
        }
        $this->gqlFilter = $gqlFilter;
        return $this;
    }

    /**
     *
     * get Entity
     *
     * @return Entity $entity
     **/
    public function getEntity(): Entity
    {
        return $this->entity;
    }

    /**
     * @param Entity $entity
     * @return self
     */
    public function setEntity(Entity $entity): self
    {
        $this->entity = $entity;
        return $this;
    }

    /**
     *
     * fetch instance of config entity
     *
     * @param string $property
     * @param bool   $withDescriptions
     *
     * @return self
     */
    public function fetchEntity(string $property = '', bool $withDescriptions = false): self
    {
        $this->entity = ($property === '') ?
            $this->readConfig($withDescriptions) :
            $this->readConfigByProperty($property, $withDescriptions);

        $this->entity['server']['env'] = getenv('ZMS_ENV');

        return $this;
    }

    /**
     * @param Entity $entity
     * @return self
     */
    public function writeEntity(Entity $entity): self
    {
        $this->entity = App::$http->readPostResult('/config/', $entity)->getEntity();
        return $this;
    }

    /**
     *
     * read complete config
     *
     * @return Entity $entity
     **/
    protected function readConfig(bool $withDescriptions = false): Entity
    {
        return App::$http
            ->readGetResult('/config/', [
                'gql' => $this->gqlFilter,
                'withDescriptions' => $withDescriptions
            ], App::CONFIG_SECURE_TOKEN)
            ->getEntity();
    }

    /**
     *
     * read config filtered by given property
     *
     * @param string $property
     * @param bool $withDescriptions
     * @return Entity $entity
     */
    protected function readConfigByProperty(string $property, bool $withDescriptions = false): Entity
    {
        return App::$http
            ->readGetResult('/config/' . $property . '/', [
                'gql' => $this->gqlFilter,
                'withDescriptions' => $withDescriptions
            ], App::CONFIG_SECURE_TOKEN)
            ->getEntity();
    }

    public function getOidcProviderList(RequestInterface $request): array
    {
        $oidcPreferences = $this->getEntity()->getPreference('oidc', 'provider');
        $providerListJson = array_keys($request->getAttribute('oauthProviderList'));
        $providerListConfig = array_filter(explode(',', $oidcPreferences));
        return array_intersect($providerListJson, $providerListConfig);
    }

    public function isCheckInEnabled(): bool
    {
        $allowedOn = explode(',', $this->entity->getPreference('checkin', 'feature') ?? '');
        $allowedOn = array_map('trim', $allowedOn);
        $allowedOn = array_map('strtolower', $allowedOn);

        return in_array(strtolower(getenv('ZMS_ENV')), $allowedOn);
    }
}
