<?php
/**
 * @copyright BerlinOnline GmbH
 **/

declare(strict_types=1);

namespace BO\Zmsadmin\Helper;

use App;
use BO\Slim\Container;
use BO\Slim\Render;
use BO\Zmsclient\Exception as RequestException;
use BO\Zmsentities\Config;
use BO\Zmsentities\Helper\DateTime;
use BO\Zmsentities\MaintenanceSchedule;
use Fig\Http\Message\StatusCodeInterface;
use Psr\Http\Message\ResponseInterface;
use Slim\Psr7\Request;

class Maintenance
{
    /** cached entity
     * @var false|null|MaintenanceSchedule
     */
    protected $activeEntity = false; //todo: use 'uninitialized' with php8 instead of false

    /** cached entity
     * @var false|null|MaintenanceSchedule
     */
    protected $scheduledEntity = false;

    /** get the start of the running or next maintenance or null when nothing is planned */
    public function getStart(?\DateTimeInterface $nowTime = null): ?\DateTimeInterface
    {
        if ($this->getActiveEntity()) {
            return $this->activeEntity->getStartDateTime();
        }

        if ($this->getScheduledEntity()) {
            $nowTime      = $nowTime ?? new DateTime();
            $leadTime     = $this->getScheduledEntity()->getLeadTime();
            $plannedStart = $this->getScheduledEntity()->getStartDateTime();
            if ($plannedStart->getTimestamp() - (60 * $leadTime) <= $nowTime->getTimestamp()) {
                // return start time only when its relevant
                return $plannedStart;
            }
        }

        return null;
    }
    public function renderMaintenanceResponse(ResponseInterface $response, Request $request): ResponseInterface
    {
        if (!$this->activeEntity) {
            throw new \RuntimeException('Rendering of maintenance request without active maintenance');
        }

        if (strpos($request->getUri()->getPath(), '/calendarPage/')
            || strpos($request->getUri()->getPath(), '/appointmentForm/')
            || strpos($request->getUri()->getPath(), '/queueInfo/')
            || strpos($request->getUri()->getPath(), '/workstation/status/')
        ) {
            return $response->withStatus(StatusCodeInterface::STATUS_SERVICE_UNAVAILABLE, 'Maintenance');
        }
        //used from navigation and else
        App::$maintenance = $this->getMaintenanceData();
        $renderParameter   = array_merge($this->getMaintenanceData(), [
            'workstation' => App::$http->readGetResult('/workstation/')->getEntity(),
        ]);

        return Render::withHtml($response, 'page/maintenance.twig', $renderParameter);
    }

    public static function isBlockedRequest(Request $request): bool
    {
        $unblockedRoutes = [
            'GET /',
            'GET /accessstats/',
            'GET /calldisplay/',
            'GET /profile/',
            'POST /profile/',
            'GET /healthcheck/',
            'GET /logout/',
            'GET /department/*/useraccount/logout/',
            'GET /maintenance/',
            'GET /maintenance/schedule/*/',
            'POST /maintenance/schedule/*/',
            'GET /maintenance/schedule/delete/*/',
            'GET /search/',
            'POST /sign/parameters/',
            'GET /status/',
            'GET /queueTable/',
        ];

        $basePath = App::$slim->getBasePath();
        $basePath = strlen($basePath) > 1 ? $basePath : '';
        $uriPath  = $request->getUri()->getPath();
        $hasBase  = strlen($basePath) && strpos($uriPath, $basePath) === 0;
        $subPath  = $hasBase ? substr($uriPath, strlen($basePath)) : $uriPath;
        $reqParts = explode('/', $request->getMethod() . ' ' . $subPath);
        $nonBlocked = array_map(
            function (string $route) {
                return explode('/', $route);
            },
            $unblockedRoutes
        );

        foreach ($reqParts as $key => $value) {
            foreach ($nonBlocked as $routeNumber => $routeParts) {
                if ($routeParts[$key] === '*'
                    || $routeParts[$key] === $value
                ) {
                    continue;
                }

                unset($nonBlocked[$routeNumber]);
            }
            if (count($nonBlocked) === 0) {
                return true;
            }
        }

        return false;
    }

    /**
     * used for rendering, so it is available only when maintenance is announced or active
     */
    public function getMaintenanceData(): array
    {
        if ($this->getStart()) {
            $maintenanceEntity = $this->getActiveEntity() ?? $this->getScheduledEntity();
            $duration = $maintenanceEntity->getDuration();

            return array_merge((array) $maintenanceEntity, [
                'endDateTime' => $maintenanceEntity->getStartDateTime()->modify('+' . $duration . 'minutes'),
            ]);
        }

        return [];
    }

    public function getActiveEntity(): ?MaintenanceSchedule
    {
        if ($this->activeEntity === false) {
            try {
                $this->activeEntity = App::$http->readGetResult('/maintenanceschedule/active/')->getEntity();
            } catch (RequestException $re) {
                $this->activeEntity = null;
            }
        }

        return $this->activeEntity;
    }

    protected function getScheduledEntity(): ?MaintenanceSchedule
    {
        $config =  \App::getConfigService()->fetchEntity()->getEntity();
        if ($this->scheduledEntity === false) {
            $configEntry = $config->getEntryOrDefault(Config::PROPERTY_MAINTENANCE_NEXT);
            $this->scheduledEntity = null;

            if (!empty($configEntry)) {
                $entryData = json_decode($configEntry, true);
                $entityId  = $entryData['id'];

                try {
                    $this->scheduledEntity = App::$http->readGetResult("/maintenanceschedule/$entityId/")->getEntity();
                    $this->scheduledEntity['startDateTime'] = DateTime::create($entryData['startDateTime']);
                } catch (RequestException $re) {
                    return null;
                }
            }
        }

        return $this->scheduledEntity;
    }
}
