<?php

namespace BO\Zmsadmin\Helper;

class GraphDefaults
{
    public static function defaultFormat($string)
    {
        return preg_replace('#\s+#m', ' ', trim($string));
    }

    /**
     * requests reduced response data
     */
    public static function getConfig()
    {
        $default =<<< EOS
{ 
    appointments 
    availability
    calldisplay
    checkin
    cron
    descriptions
    emergency
    mailings
    maintenance
    notifications
    oidc
    remotecall
    server
    setting
    status
    support
    ticketpriner
    webcalldisplay
    email templates
}
EOS;
        return static::defaultFormat($default);
    }

    /**
     * workstation reduced response data
     */
    public static function getWorkstation()
    {
        $default =<<< EOS
{ 
    id,
    name,
    scope {
        id, 
        shortName
        source,
        contact {
            name
        }
        preferences {
            appointment
            client
            workstation
            ticketprinter
        }
        status 
    },
    queue {
        clusterEnabled
        appointmentsOnly
    },
    useraccount {
        rights
        id
        lastLogin
    },
    linkList
}
EOS;
        return static::defaultFormat($default);
    }

    /**
     * workstation reduced response data
     */
    public static function getWorkstationStatus()
    {
        $default =<<< EOS
{ 
        id,
        name,
        scope { 
            shortName
            contact {
               name
            }
            preferences {
                workstation
            }
            status {
                emergency
            }
        }
        useraccount {
            id
            lastLogin
            rights
        }
EOS;
        return static::defaultFormat($default);
    }

    /**
     * availability reduced response data
     */
    public static function getAvailability()
    {
        $default =<<< EOS
{ 
    id,
    weekday,
    repeat
    startDate,
    endDate,
    startTime,
    endTime,
    type,
    bookable,
    workstationCount,
    lastChange,
    multipleSlotsAllowed,
    slotTimeInMinutes,
    description,
    scope {
        id, 
        source,
        dayoff {
            date
        },
        preferences {
            appointment
        }
    }
}
EOS;
        return static::defaultFormat($default);
    }

    /**
     * availability reduced response data
     */
    public static function getAvailabilityTimes()
    {
        $default =<<< EOS
{ 
    weekday,
    repeat
    startDate,
    endDate,
    startTime,
    endTime,
    type,
    scope {
        dayoff {
            date
        }
    }
}
EOS;
        return static::defaultFormat($default);
    }

    /**
     * scope reduced response data
     */
    public static function getScope()
    {
        $default =<<< EOS
{ 
    id
    hint
    source
    contact
    shortName
    dayoff {
        date
    }
    preferences
    provider {
        id
        contact 
        name 
        data { 
            payment 
        }
    }
    queue
    status
}
EOS;
        return static::defaultFormat($default);
    }

    /**
     * department reduced response data
     */
    public static function getDepartment()
    {
        $default =<<< EOS
{ 
    id 
    name
    preferences 
}
EOS;
        return static::defaultFormat($default);
    }

    /**
     * department reduced response data
     */
    public static function getOwnerForUseraccountList()
    {
        $default =<<< EOS
{ 
    name 
    organisations {
        name
        departments {
            id
            name
        } 
    }
}
EOS;
        return static::defaultFormat($default);
    }

    /**
     * department reduced response data
     */
    public static function getUserForUseraccountList()
    {
        $default =<<< EOS
{ 
    id 
    lastLogin
}
EOS;
        return static::defaultFormat($default);
    }


    /**
     * requests reduced response data
     */
    public static function getRequest()
    {
        $default =<<< EOS
{ 
    id 
    name 
    link 
    timeSlotCount
    data { 
        locations { 
            appointment 
        } 
    } 
}


EOS;
        return static::defaultFormat($default);
    }
    
    /**
     * free process list reduced response data
     */
    public static function getFreeProcessList()
    {
        $default =<<< EOS
{ 
    scope { 
        id
        source 
        contact 
        provider { 
            contact 
            name 
            data { 
                payment 
            }
        } 
        preferences { 
            appointment
        }
    } 
    appointments {
        date
    }
}
EOS;
        return static::defaultFormat($default);
    }

    /**
     * calendar output for day select page
     */
    public static function getCalendar()
    {
        $default =<<< EOS
{ 
    firstDay 
    lastDay 
    days 
    freeProcesses 
    requests {
        id 
        name 
        link
    } 
    scopes { 
        id 
        source 
        provider {
            contact 
            name 
            data { 
                payment
            }
        } 
        preferences { 
            appointment
        }
    } 
}
EOS;
        return static::defaultFormat($default);
    }

    /**
     *  reduced process response data
     */
    public static function getProcess()
    {
        $default =<<< EOS
{
    amendment
    authKey
    apiclient
    id
    status
    createTimestamp
    reminderTimestamp
    appointments{
        date
        slotCount
    }
    clients{
        familyName
        email
        surveyAccepted
        telephone
        emailSendCount
        notificationsSendCount
    }
    queue{
        arrivalTime,
        withAppointment,
        number,
        status,
        waitingTimeEstimate,
        waitingTimeOptimistic,
        waitingTime,
        callCount
    }
    requests{
        id
        link
        name
        source
    }
    scope{
        id
        shortName
        source
        contact
        provider{
            contact 
            name 
            data { 
                payment 
            }
        }
        preferences{
            client
            appointment
            survey
        }
    },
    remotecalls
}
EOS;
        return static::defaultFormat($default);
    }

    /**
     *  reduced process response data
     */
    public static function getPickup()
    {
        $default =<<< EOS
{
    amendment
    id
    appointments{
        date
    }
    clients{
        familyName
        email
        telephone
        emailSendCount
        notificationsSendCount
    }
    queue{
        arrivalTime,
        withAppointment,
        number,
        callCount
    }
    requests{
        name
    }
}
EOS;
        return static::defaultFormat($default);
    }

/**
 *  reduced process response data
 */
    public static function getFreeProcess()
    {
        $default =<<< EOS
{
    appointments{
        date
        slotCount
    }
    scope{
        id
    }
}
EOS;
        return static::defaultFormat($default);
    }
}
