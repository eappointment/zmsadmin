<?php
/**
 *
 * @package Zmsadmin
 * @copyright BerlinOnline GmbH
 *
 */
namespace BO\Zmsadmin\Helper;

use BO\Mellon\Validator;
use BO\Zmsentities\Collection\ScopeList;
use BO\Zmsentities\Process;
use BO\Zmsentities\Schema\Entity as SchemaEntity;
use BO\Zmsentities\Scope;
use BO\Zmsentities\Workstation;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @SuppressWarnings(Complexity)
 */
class AppointmentFormHelper
{
    public static function readFreeProcessList($request, $workstation, $selectedProcess)
    {
        $validator = $request->getAttribute('validator');
        $scope = static::readSelectedScope($request, $workstation, $selectedProcess);
        $scopeList = ($scope) ? (new ScopeList)->addEntity($scope) : (new ClusterHelper($workstation))->getScopeList();
        
        $slotType = static::setSlotType($validator);
        $slotsRequired = static::setSlotsRequired($validator, $scope, $selectedProcess);
        $freeProcessList = static::readProcessListByScopeAndDate(
            $validator,
            $scopeList,
            $slotType,
            $slotsRequired
        );
        $freeProcessList = ($freeProcessList) ? $freeProcessList->withoutExpiredAppointmentDate(\App::$now) : null;
        if (!($scope && $selectedProcess && $scope->getId() != $selectedProcess->getScopeId())) {
            $freeProcessList = static::getFreeProcessListWithSelectedProcess(
                $validator,
                $scopeList,
                $freeProcessList,
                $selectedProcess
            );
        }
        return ($freeProcessList) ? $freeProcessList->toProcessListByTime()->sortByTimeKey() : null;
    }

    public static function readRequestList($request, $workstation, $selectedScope = null)
    {
        $scope = ($selectedScope) ? $selectedScope : static::readSelectedScope($request, $workstation);
        $requestList = null;
        if ($scope) {
            $requestList = \App::$http
                ->readGetResult('/scope/'. $scope->getId().'/request/', [
                    'gql' => GraphDefaults::getRequest()
                ])
                ->getCollection();
        }
        return ($requestList) ? $requestList->sortByName() : new \BO\Zmsentities\Collection\RequestList;
    }

    /**
     * @param RequestInterface|ServerRequestInterface $request
     * @param Workstation $workstation
     * @param $selectedProcess
     * @return Scope|null
     */
    public static function readSelectedScope(
        RequestInterface $request,
        Workstation $workstation,
        $selectedProcess = null
    ): ?Scope {
        /** @var Validator $validator */
        $validator = $request->getAttribute('validator');
        $input = $request->getParsedBody();
        $selectedScope = null;
        $selectedScopeId = (isset($input['scope']))
            ? $input['scope']
            : $validator->getParameter('selectedscope')->isNumber()->isGreaterThan(0)->getValue();

        if (!$workstation->queue['clusterEnabled']
            && !$selectedScopeId
            && $workstation->getScope()
            && $workstation->getScope()->getId()
        ) {
            $selectedScope = new Scope($workstation->getScope());
        }
        if ($selectedScopeId) {
            $selectedScope = \App::$http
              ->readGetResult('/scope/'. $selectedScopeId .'/', [
                  'resolveReferences' => 1,
                  'gql' => GraphDefaults::getScope()
                ])
              ->getEntity();
        }
        if (!$selectedScopeId
            && !$workstation->queue['clusterEnabled']
            && $selectedProcess
            && $selectedProcess->hasId()
        ) {
            $selectedScope = $selectedProcess->getCurrentScope();
        }

        return $selectedScope;
    }

    public static function readSelectedProcess($request)
    {
        $validator = $request->getAttribute('validator');
        $selectedScopeId = $validator->getParameter('selectedscope')->isNumber()->getValue();
        $selectedProcessId = $validator->getParameter('selectedprocess')->isNumber()->getValue();
        return ($selectedProcessId) ?
            \App::$http->readGetResult('/process/'. $selectedProcessId .'/', [
                'gql' => GraphDefaults::getProcess(),
                'selectedscope' => $selectedScopeId,
            ])->getEntity() :
            null;
    }

    /**
     * @param Process|SchemaEntity $process
     *
     */
    public static function updateMailAndNotification(array $formData, Process $process)
    {
        if (isset($formData['sendMailConfirmation'])) {
            $mailConfirmation = $formData['sendMailConfirmation'];
            $mailConfirmation = (isset($mailConfirmation['value'])) ? $mailConfirmation['value'] : $mailConfirmation;
            self::writeMail($mailConfirmation, $process);
        }
        if (isset($formData['sendConfirmation'])) {
            $smsConfirmation = $formData['sendConfirmation'];
            $smsConfirmation = (isset($smsConfirmation['value'])) ? $smsConfirmation['value'] : $smsConfirmation;
            self::writeNotification($smsConfirmation, $process);
        }
    }

    protected static function readProcessListByScopeAndDate($validator, $scopeList, $slotType, $slotsRequired)
    {
        $selectedDate = $validator->getParameter('selecteddate')->isString()->getValue();
        $calendar = new Calendar($selectedDate);
        return $calendar->readAvailableSlotsFromDayAndScopeList($scopeList, $slotType, $slotsRequired);
    }

    protected static function getFreeProcessListWithSelectedProcess(
        $validator,
        $scopeList,
        $freeProcessList,
        $selectedProcess
    ) {
        $selectedDate = $validator->getParameter('selecteddate')->isString()->getValue();
        if ($selectedProcess &&
            $selectedProcess->queue->withAppointment &&
            $selectedDate == $selectedProcess->getFirstAppointment()->toDateTime()->format('Y-m-d')
        ) {
            if ($freeProcessList) {
                $freeProcessList->setTempAppointmentToProcess(
                    $selectedProcess->getFirstAppointment()->toDateTime(),
                    $scopeList->getFirst()->getId()
                );
            } elseif (! $freeProcessList) {
                $freeProcessList = (new \BO\Zmsentities\Collection\ProcessList())->addEntity($selectedProcess);
            }
        }
        
        return ($freeProcessList) ? $freeProcessList : null;
    }

    protected static function setSlotType($validator)
    {
        $slotType = $validator->getParameter('slotType')->isString()->getValue();
        $slotType = ($slotType) ? $slotType : 'intern';
        return $slotType;
    }

    protected static function setSlotsRequired($validator, Scope $scope, $process)
    {
        $slotsRequired = 0;
        if ($scope && $scope->getPreference('appointment', 'multipleSlotsEnabled')) {
            $slotsRequired = $validator->getParameter('slotsRequired')->isNumber()->getValue();
            $slotsRequired = ($slotsRequired) ? $slotsRequired : 0;
        }
        $slotsRequired = (0 == $slotsRequired && $process)
            ? $process->getFirstAppointment()->getSlotCount()
            : $slotsRequired;
        return $slotsRequired;
    }

    protected static function writeNotification($smsConfirmation, Process $process)
    {
        if ($smsConfirmation &&
            $process->scope->hasNotificationEnabled() &&
            $process->getFirstClient()->hasTelephone()
        ) {
            \App::$http->readPostResult(
                '/process/'. $process->id .'/'. $process->authKey .'/confirmation/notification/',
                $process
            );
        }
    }

    protected static function writeMail($mailConfirmation, Process $process)
    {
        if ($mailConfirmation &&
            $process->getFirstClient()->hasEmail() &&
            $process->scope->hasEmailFrom()
        ) {
            \App::$http->readPostResult(
                '/process/'. $process->id .'/'. $process->authKey .'/confirmation/mail/',
                $process
            );
        }
    }
}
