<?php
/**
 *
 * @package Zmsadmin
 * @copyright BerlinOnline GmbH
 *
 */
namespace BO\Zmsadmin\Helper;

use BO\Zmsentities\Cluster;
use BO\Zmsentities\Collection\WorkstationList;
use BO\Zmsentities\Scope;

class WorkstationInfo
{
    public static function getInfoBoxData(\BO\Zmsentities\Workstation $workstation, $selectedDate)
    {
        $scopeId  = $workstation->getScope() ? $workstation->getScope()->getId() : null;
        $infoData = array(
            'waitingTimeEstimate' => 0,
            'waitingTimeOptimistic' => 0,
            'waitingClientsFullList' => 0,
            'waitingClientsBeforeNext' => 0,
            'waitingClientsEffective' => 0
        );
        $scope = $scopeId ? \App::$http->readGetResult('/scope/'. $scopeId .'/workstationcount/')->getEntity() : null;

        $clusterHelper = (new ClusterHelper($workstation));
       
        $infoData['workstationGhostCount'] = $scope ? $scope->status['queue']['ghostWorkstationCount'] : 0;
        $infoData['workstationList'] = ($clusterHelper->isClusterEnabled()) ?
            static::getWorkstationsByCluster($clusterHelper->getEntity()) :
            static::getWorkstationsByScope($scope);

        $queueListHelper = (new QueueListHelper($clusterHelper, $selectedDate));
        if ($queueListHelper->getWaitingCount()) {
            $infoData['waitingClientsFullList'] = $queueListHelper->getWaitingCount();
            if ($selectedDate == \App::$now->format('Y-m-d')) {
                $infoData = static::getAdditionalInfoData($infoData, $queueListHelper);
            }
        }
        return $infoData;
    }


    public static function getWorkstationsByScope(?Scope $scope)
    {
        if ($scope === null || !$scope->getId()) {
            return new WorkstationList();
        }

        return \App::$http
            ->readGetResult('/scope/'. $scope->getId() . '/workstation/', ['resolveReferences' => 1])
            ->getCollection();
    }

    public static function getWorkstationsByCluster(?Cluster $cluster)
    {
        if ($cluster === null || !$cluster->getId()) {
            return new WorkstationList();
        }

        return \App::$http
            ->readGetResult('/cluster/'. $cluster->getId() . '/workstation/', ['resolveReferences' => 1])
            ->getCollection();
    }

    protected static function getAdditionalInfoData($infoData, $queueListHelper)
    {
        $infoData['waitingTimeEstimate'] = $queueListHelper->getEstimatedWaitingTime();
        $infoData['waitingTimeOptimistic'] = $queueListHelper->getOptimisticWaitingTime();
        $infoData['waitingClientsBeforeNext'] = $queueListHelper->getWaitingClientsBeforeNext();
        $infoData['waitingClientsEffective'] = $queueListHelper->getWaitingClientsEffective();
        return $infoData;
    }
}
