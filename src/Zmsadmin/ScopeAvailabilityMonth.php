<?php

/**
 * Return availability list by scope in month view.
 *
 * @copyright 2018 BerlinOnline GmbH
 */

namespace BO\Zmsadmin;

use BO\Mellon\Validator;

use BO\Zmsadmin\Helper\Calendar as CalendarHelper;
use BO\Zmsadmin\Helper\GraphDefaults as GraphDefaultsHelper;

use BO\Zmsentities\Collection\AvailabilityList;
use BO\Zmsentities\Collection\ProcessList;
use BO\Zmsentities\Collection\ScopeList;
use BO\Zmsentities\Helper\DateTime as DateTimeHelper;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

use BO\Slim\Render;

use BO\Zmsclient\Exception as ClientException;

/**
 *
 * @SuppressWarnings(Param)
 * @SuppressWarnings(Cyclomatic)
 * @SuppressWarnings(Complexity)
 * @SuppressWarnings(Coupling)
 */
class ScopeAvailabilityMonth extends BaseController
{
    /**
     *
     * @param RequestInterface  $request  The request instance
     * @param ResponseInterface $response The response instance
     * @param array             $args     The path arguments
     *
     * @return ResponseInterface
     */
    public function readResponse(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        $workstation = \App::$http->readGetResult('/workstation/', [
            'resolveReferences' => 1,
            'gql' => GraphDefaultsHelper::getWorkstation()
        ])->getEntity();
        
        $dateTime = (isset($args['date'])) ? new DateTimeHelper($args['date']) : \App::$now;
        $startDate = $dateTime->modify('first day of this month');
        $endDate = $dateTime->modify('last day of this month');

        $scopeId = Validator::value($args['id'])->isNumber()->getValue();
        $scope = \App::$http->readGetResult('/scope/' . $scopeId . '/', [
            'resolveReferences' => 1
        ])->getEntity();
        $calendar = new CalendarHelper($dateTime->format('Y-m-d'));
        $scopeList = (new ScopeList())->addEntity($scope);
        $month = $calendar->readMonthListByScopeList($scopeList, 'intern', 0)->getFirst();

        $availabilityList = $this->getAvailabilityList($scope, $startDate, $endDate);
        $processConflictList = \App::$http
            ->readGetResult('/scope/' . $scope->getId() . '/conflict/', [
                'startDate' => \App::$now->format('Y-m-d'),
                'endDate' => $endDate->format('Y-m-d'),
            ])
            ->getCollection();
        $processConflictList = $processConflictList ?
            $processConflictList->toConflictListByDay() :
            new ProcessList();
        
        $legendTypes = CalendarPage::$legendtypes;
        unset($legendTypes['square']['selected']);
        return Render::withHtml(
            $response,
            'page/availabilityMonth.twig',
            array(
                'availabilityList' => $availabilityList,
                'conflicts' => $processConflictList,
                'calendar' => $calendar,
                'dayoffList' => $scope->getDayoffList(),
                'dateTime' => $dateTime,
                'timestamp' => $dateTime->getTimeStamp(),
                'month' => $month,
                'scope' => $scope,
                'menuActive' => 'owner',
                'title' => 'Behörden und Standorte - Öffnungszeiten',
                'workstation' => $workstation,
                'baseMonthString' => $startDate->format('m'),
                'baseYearString' => $endDate->format('Y'),
                'baseMonth_timestamp' => $startDate->getTimeStamp(),
                'legendtypes' => $legendTypes,
            )
        );
    }

    protected function getAvailabilityList($scope, $startDate, $endDate)
    {
        try {
            $availabilityList = \App::$http
                ->readGetResult(
                    '/scope/' . $scope->getId() . '/availability/',
                    [
                        'startDate' => $startDate->format('Y-m-d'),
                        'endDate' => $endDate->format('Y-m-d')
                    ]
                )
                ->getCollection();
        } catch (ClientException $exception) {
            if ($exception->template != 'BO\Zmsapi\Exception\Availability\AvailabilityNotFound') {
                throw $exception;
            }
            $availabilityList = new AvailabilityList();
        }
        return $availabilityList;
    }
}
