<?php
/**
 * @package Zmsadmin
 * @copyright BerlinOnline GmbH
 **/

namespace BO\Zmsadmin;

use App;
use BO\Slim\Render;
use BO\Zmsclient\WorkstationRequests;
use BO\Zmsentities\Collection\LogList;
use BO\Zmsentities\Collection\ProcessList;
use BO\Zmsentities\Workstation as WorkstationEntity;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class Search extends BaseController
{
    /**
     * @SuppressWarnings(param)
     * @SuppressWarnings(complexity)
     *
     * @param RequestInterface  $request
     * @param ResponseInterface $response
     * @param array             $args
     *
     * @return ResponseInterface
     */
    public function readResponse(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        $workstation = App::$http->readGetResult('/workstation/', ['resolveReferences' => 1])->getEntity();
        $workstationRequest = new WorkstationRequests(\App::$http, $workstation);
        $validator = $request->getAttribute('validator');
        $queryString = $validator->getParameter('query')
            ->isString()
            ->getValue();
        $scopeId = $validator->getParameter('scopeId')
            ->isNumber()
            ->getValue();
        $processList = new ProcessList();
        if ($queryString) {
            $processList = App::$http->readGetResult('/process/search/', [
                'query' => $queryString,
                'resolveReferences' => 1,
            ])->getCollection();
        }
        $logList = new LogList();
        if (preg_match('#^\d{4,}$#', $queryString) && $workstation->hasSuperUseraccount()) {
            $processLogList = App::$http->readGetResult("/log/process/$queryString/")->getCollection();
            $logList = ($processLogList) ? $logList->addList($processLogList) : $logList;
        }
        if ($scopeId && $workstation->hasSuperUseraccount()) {
            $scopeLogList = App::$http->readGetResult("/log/scope/$scopeId/")->getCollection();
            $logList = ($scopeLogList) ? $logList->addList($scopeLogList) : $logList;
        }

        $processList = ($processList) ? $processList : new ProcessList();
        $processListOther = new ProcessList();
        $isClustered = $this->isWorkstationScopeInCluster($workstationRequest);
        if (!$workstation->hasSuperUseraccount() && (!$isClustered || !$workstation->isClusterEnabled())) {
            $processListOther = $processList->withOutScopeId($workstation->scope['id']);
            $processList = $processList->withScopeId($workstation->scope['id']);
        }
        return Render::withHtml(
            $response,
            'page/search.twig',
            [
                'title' => 'Suche',
                'workstation' => $workstation,
                'processList' => $processList,
                'processListOther' => $processListOther,
                'logList' => $logList,
                'searchQuery' => $queryString,
                'isClustered' => $isClustered,
                'menuActive' => 'search',
            ]
        );
    }

    protected function isWorkstationScopeInCluster($workstationRequest)
    {
        $cluster = $workstationRequest->readCluster();
        $scopeList = $cluster->toProperty()->scopes->get();
        return ($scopeList->count() > 1);
    }
}
