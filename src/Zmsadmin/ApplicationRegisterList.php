<?php
/**
 * @copyright BerlinOnline GmbH
 **/

declare(strict_types=1);

namespace BO\Zmsadmin;

use BO\Mellon\Validator;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use BO\Zmsentities\Collection\ApplicationRegisterList as ApplicationList;

class ApplicationRegisterList extends BaseController
{
    /**
     * @SuppressWarnings(UnusedFormalParameter)
     * @return ResponseInterface
     */
    public function readResponse(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        $workstation = \App::$http->readGetResult('/workstation/', ['resolveReferences' => 1])->getEntity();
        $workstation->getUseraccount()->testRights(['organisation']);

        /** @var Validator $validator */
        $validator = $request->getAttribute('validator');
        $appList   = \App::$http->readGetResult('/applicationregister/')->getCollection();
        $appList   = $appList ?? new ApplicationList();
        $scopeList = \App::$http->readGetResult('/scope/', ['resolveReferences' => 0])->getCollection();

        $map = [];
        /** @var \BO\Zmsentities\Scope $scope */
        foreach ($scopeList as $scope) {
            $map[$scope->getId()] = $scope->getName() . ' ' . $scope->shortName;
        }

        return \BO\Slim\Render::withHtml(
            $response,
            'page/applicationRegisterList.twig',
            [
                'title' => 'Client Register',
                'menuActive' => 'applicationregister',
                'workstation' => $workstation,
                'applicationlist' => $appList,
                'scopeNames' => $map,
                'origin' => urlencode(\App::$slim->urlFor('application_register_list')),
                'success' => $validator->getParameter('success')->isString()->getValue(),
            ]
        );
    }
}
