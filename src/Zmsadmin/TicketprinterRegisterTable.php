<?php
/**
 * @copyright BerlinOnline GmbH
 **/

declare(strict_types=1);

namespace BO\Zmsadmin;

use BO\Zmsentities\ApplicationRegister;

class TicketprinterRegisterTable extends CalldisplayRegisterTable
{
    protected $template = 'block/list/appclients/ticketprinter.twig';
    protected $appType  = ApplicationRegister::TYPE_ZMS2_TICKETPRINTER;
}
