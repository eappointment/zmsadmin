<?php
/**
 * @package Zmsadmin
 * @copyright BerlinOnline GmbH
 **/

namespace BO\Zmsadmin;

use BO\Slim\Render;
use BO\Zmsadmin\Helper\ImageUploader;
use BO\Zmsclient\Exception;
use BO\Zmsentities\Cluster as Entity;
use BO\Mellon\Validator;
use BO\Zmsentities\Mimepart;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class Cluster extends BaseController
{
    /**
     * @SuppressWarnings(Param)
     * @SuppressWarnings(Cyclomatic)
     * @SuppressWarnings(NPath)
     * @SuppressWarnings(Length)
     */
    public function readResponse(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        $workstation = \App::$http->readGetResult('/workstation/', ['resolveReferences' => 1])->getEntity();
        $entityId = Validator::value($args['clusterId'])->isNumber()->getValue();
        $departmentId = Validator::value($args['departmentId'])->isNumber()->getValue();

        $entity = \App::$http
                ->readGetResult('/cluster/' . $entityId . '/', ['resolveReferences' => 2])
                ->getEntity();
        $organisation = $this->testOrganisation($entityId);

        $department = \App::$http->readGetResult(
            '/department/' . $departmentId . '/',
            ['resolveReferences' => 2]
        )->getEntity();

        $parameters = [
            'title' => 'Cluster',
            'menuActive' => 'owner',
            'workstation' => $workstation,
            'organisation' => $organisation,
            'cluster' => $entity->getArrayCopy(),
            'department' => $department,
            'scopeList' => $department->getScopeList()->sortByContactName(),
            'success' => $request->getAttribute('validator')->getParameter('success')->isString()->getValue(),
        ];

        $input = $request->getParsedBody();
        $input['id'] = $entityId;

        $imageData   = \App::$http->readGetResult('/cluster/'. $entityId .'/imagedata/calldisplay/')->getEntity();
        $uploadFiles = $request->getUploadedFiles();
        $uploadedImg = $uploadFiles['uploadCallDisplayImage'] ?? null;
        $uploadedImg = ($uploadedImg === null || $uploadedImg->getFilePath() == '') ? null : new Mimepart([
            'mime'    => $uploadedImg->getClientMediaType(),
            'content' => base64_encode(file_get_contents($uploadedImg->getFilePath())),
            'base64'  => true,
        ]);
        $selectedImg = !isset($input['selectedImage']) ? null : new Mimepart([
            'content' => $input['selectedImage'],
            'mime'    => $input['selectedImageMime'],
            'base64'  => true,
        ]);

        unset($input['selectedImage']);
        unset($input['selectedImageMime']);

        $parameters['callDisplayImage'] = $uploadedImg ?? $selectedImg ?? $imageData;
        if ($uploadedImg || $selectedImg) {
            $parameters['selectedImage'] = $uploadedImg ?? $selectedImg;
        }

        /** intended not to loop (ignore/disable IDE warnings), but to reduce complexity */
        while (is_array($input) && array_key_exists('save', $input)) {
            unset($input['save']); //prevent loop

            $parameters['cluster'] = new Entity($input);
            $entity = (new Entity($input))->withCleanedUpFormData();
            try {
                $entity = \App::$http->readPostResult('/cluster/' . $entity->id . '/', $entity)->getEntity();
            } catch (Exception $clientException) {
                $parameters['exception'] = [
                    'template' => Helper\TwigExceptionHandler::getExceptionTemplate($clientException),
                    'data'     => $clientException->data
                ];
                break;
            }

            if (isset($input['removeImage']) && $input['removeImage']) {
                \App::$http->readDeleteResult('/cluster/'. $entityId .'/imagedata/calldisplay/');
            } elseif (isset($parameters['selectedImage'])) {
                try {
                    $imageUploader  = new ImageUploader($request, 'uploadCallDisplayImage');
                    $uploadFunction = $uploadedImg ? 'writeUploadToCluster' : 'writeMimepartToCluster';
                    $imageUploader->$uploadFunction($entity->id, $parameters['selectedImage']);
                } catch (Exception $clientException) {
                    $parameters['exception'] = [
                        'template' => Helper\TwigExceptionHandler::getExceptionTemplate($clientException),
                        'data'     => $clientException->data,
                    ];
                    break;
                } catch (\InvalidArgumentException $exception) {
                    $parameters['exception'] = [
                        'template' => 'exception/bo/zmsentities/exception/schemavalidation.twig',
                        'data'     => ['content' => ['messages' => [$exception->getMessage()]]],
                    ];
                    break;
                }
            }

            return Render::redirect('cluster', [
                'clusterId'    => $entityId,
                'departmentId' => $departmentId,
            ], [
                'success' => 'cluster_saved'
            ]);
        }

        return Render::withHtml(
            $response,
            'page/cluster.twig',
            $parameters
        );
    }

    protected function testOrganisation($entityId)
    {
        try {
            $organisation = \App::$http->readGetResult('/cluster/' . $entityId . '/organisation/')->getEntity();
        } catch (\BO\Zmsclient\Exception $exception) {
            if ($exception->template == 'BO\Zmsdb\Exception\ClusterWithoutScopes') {
                $organisation = new \BO\Zmsentities\Organisation();
            } else {
                throw $exception;
            }
        }
        return $organisation;
    }
}
