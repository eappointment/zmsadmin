<?php

declare(strict_types=1);

namespace BO\Zmsadmin;

use App;
use BO\Slim\Render;
use BO\Zmsentities\ApplicationRegister;
use BO\Zmsentities\CheckInConfig;
use BO\Zmsentities\Collection\ApplicationRegisterList as ApplicationList;
use BO\Zmsentities\Collection\CheckInConfigList as Collection;
use BO\Zmsentities\Helper\DateTime;
use BO\Zmsentities\Schema\Entity;
use BO\Zmsentities\Workstation;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Slim\Exception\HttpNotFoundException;

class CheckIn extends BaseController
{
    /**
     * @SuppressWarnings(Param)
     */
    public function readResponse(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        if (!App::getConfigService()->isCheckInEnabled()) {
            throw new HttpNotFoundException($request);
        }

        /** @var Workstation $workstation */
        $workstation = App::$http->readGetResult('/workstation/', ['resolveReferences' => 1])->getEntity();
        $workstation->getUseraccount()->testRights(['scope']);

        $department = App::$http->readGetResult(
            '/scope/' . $workstation->scope['id'] . '/department/',
            ['resolveReferences' => 2]
        )->getEntity();
        $scopes = $department->getScopeList();

        $checkInConfigList = App::$http
            ->readGetResult('/department/' . $department->getId() . '/checkinconfig/')
            ->getCollection() ?? new Collection();
        $applicationList   = App::$http
            ->readGetResult(
                '/applicationregister/',
                [
                    'type'     => 'checkin',
                    'scopeIds' => implode(',', $scopes->getIds()),
                ]
            )->getCollection() ?? new ApplicationList();

        $appDetails = [];
        /** @var CheckInConfig $config */
        foreach ($checkInConfigList as $config) {
            $appDetails[$config->getId()] = [
                'lastDate' => null,
                'dateDelta' => Entity::getCurrentTimestamp() - DateTime::create($config->lastChange)->getTimestamp(),
            ];
        }
        /** @var ApplicationRegister $application */
        foreach ($applicationList as $application) {
            if ($checkInConfigList->count() && in_array($application->getId(), $checkInConfigList->getIds())) {
                $delta = max(
                    0,
                    Entity::getCurrentTimestamp() - $application->lastDate->setTime(20, 0)->getTimestamp()
                );
                $appDetails[$application->getId()] = [
                    'lastDate' => $application->lastDate->format('Y-m-d'),
                    'dateDelta' => $delta,
                ];
            }
        }

        return Render::withHtml(
            $response,
            'page/checkInConfig.twig',
            [
                'title'      => 'Check-In-Schalter Übersicht',
                'workstation' => $workstation,
                'scopes'     => $scopes,
                'collection' => $checkInConfigList,
                'appDetails' => $appDetails,
                'menuActive' => 'checkin',
                'success' => $request->getAttribute('validator')->getParameter('success')->isString()->getValue(),
            ]
        );
    }
}
