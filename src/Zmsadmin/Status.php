<?php
/**
 *
 * @copyright BerlinOnline GmbH
 *
 */
namespace BO\Zmsadmin;

use BO\Slim\Render;
use Exception;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class Status extends BaseController
{

    /**
     * @SuppressWarnings(UnusedFormalParameter)
     * @return String
     */
    public function readResponse(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        try {
            $workstation = \App::$http->readGetResult('/workstation/')->getEntity();
        } catch (Exception $workstationexception) {
            $workstation = null;
        }
        $result = \App::$http->readGetResult('/status/');
        $response = Render::withLastModified($response, time());
        return Render::withHtml(
            $response,
            'page/status.twig',
            array(
                'title' => 'Status der Terminvereinbarung',
                'status' => $result->getEntity(),
                'workstation' => $workstation
            )
        );
    }
}
