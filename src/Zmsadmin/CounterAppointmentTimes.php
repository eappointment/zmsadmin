<?php
/**
 * @package Zmsadmin
 * @copyright BerlinOnline GmbH
 **/

namespace BO\Zmsadmin;

use BO\Zmsentities\Scope as ScopeEntity;

class CounterAppointmentTimes extends BaseController
{
    /**
     * @SuppressWarnings(Param)
     * @return String
     */
    public function readResponse(
        \Psr\Http\Message\RequestInterface $request,
        \Psr\Http\Message\ResponseInterface $response,
        array $args
    ) {
        $validator = $request->getAttribute('validator');
        $selectedDate = $validator->getParameter('selecteddate')->isString()->getValue();
        $selectedScope = $validator->getParameter('selectedscope')->isNumber()->getValue();
        $dateTime = new \BO\Zmsentities\Helper\DateTime($selectedDate);
        $workstation = \App::$http->readGetResult('/workstation/', ['resolveReferences' => 0])->getEntity();
        if ($selectedScope && $workstation->getScope()->getId() != $selectedScope) {
            $workstation->offsetSet('scope', new ScopeEntity(['id' => $selectedScope]));
        }
        
        $availabilityList = \App::$http
            ->readGetResult('/scope/'. $workstation->scope['id'] . '/availability/', [
                'startDate' => $selectedDate,
                'endDate' => $selectedDate
            ], \App::CONFIG_SECURE_TOKEN)
            ->getCollection()
            ->withDateTime($dateTime);

        return \BO\Slim\Render::withHtml(
            $response,
            'block/appointment/times.twig',
            array(
                'workstation' => $workstation,
                'availabilityList' => $availabilityList
            )
        );
    }
}
