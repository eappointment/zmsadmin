<?php

namespace BO\Zmsadmin\Exception\OAuth;

/**
 * example class to generate an exception
 */
class OAuthPreconditionFailed extends \Exception
{
    protected $code = 412;

    protected $message = 'A verfied email address is mandatory for login over open id connect';
}
