<?php
/**
 * @package Zmsadmin
 * @copyright BerlinOnline GmbH
 **/

namespace BO\Zmsadmin;

use BO\Slim\Render;
use BO\Zmsclient\Auth;
use BO\Zmsentities\AccessStats;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class WorkstationStatus extends BaseController
{
    /**
     * @SuppressWarnings(Param)
     * @SuppressWarnings(EmptyCatchBlock)
     */
    public function readResponse(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        $workstation = \App::$http->readGetResult('/workstation/', ['resolveReferences' => 1])->getEntity();
        $workstation = $this->withFixedLastLogin($workstation);

        $trackId = Auth::getKey();
        if ($trackId !== false) {
            $statsEntity = new AccessStats([
                'id'       => substr(preg_replace("/[^0-9]/", "", $trackId), 0, 18),
                'role'     => 'user',
                'location' => 'zmsadmin'
            ]);

            try {
                \App::$http->readPostResult('/accessstats/', $statsEntity)->getCollection();
            } catch (\Exception $ignored) {
            }
        }

        $response = Render::withLastModified($response, time(), '0');
        return Render::withJson($response, ['workstation' => $workstation]);
    }

    protected function withFixedLastLogin($workstation)
    {
        $dateTime = (new \DateTime())->setTimestamp($workstation->getUseraccount()->lastLogin);
        $hour = \App::$now->format('H');
        $minute = $dateTime->format('i');
        $second = $dateTime->format('s');
        $dateTime->setTime($hour, $minute, $second);
        $workstation->getUseraccount()->lastLogin = $dateTime->getTimestamp();
        return $workstation;
    }
}
