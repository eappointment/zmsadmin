import React, { Component } from 'react'
import PropTypes from 'prop-types'
import * as Inputs from '../../../lib/inputs'
const { Description, FormGroup, Label, Controls, Select } = Inputs
import applicationRegisterTable from "../../../element/table/applicationRegister";
import $ from "jquery";

const readPropsCluster = cluster => {
    const { name, id } = cluster

    return {
        type: 'c',
        id,
        name
    }
}

const readPropsScope = scope => {
    const { shortName, contact, id } = scope

    return {
        type: 's',
        id,
        shortName,
        contact
    }
}

class TicketPrinterConfigView extends Component {
    constructor(props) {
        super(props)
        //console.log('TicketPrinterConfigView::constructor', props)

        this.includeUrl = props.includeurl || "";
        this.state = {
            edit: Boolean(props.edit),
            selectedItems: [],
            departments: props.departments.map(department => {
                const { name, id, scopes = [], clusters = [] } = department
                return {
                    id,
                    name,
                    clusters: clusters.map(readPropsCluster),
                    scopes: scopes.map(readPropsScope)
                }
            }),
            generatedUrl: "",
            homeUrl: "",
            template: "default",
            ticketPrinterName: "",
            sid: props.sid,
            scopeIds: props.scopeids,
            appList: {__html: ''},
            dataTable: null,
            origin: props.origin
        }

        if (props.parameters && props.parameters.ticketprinter) {
            if (typeof props.parameters.ticketprinter.buttonlist !== "undefined") {
                let pos = 1;
                for (let itemId of props.parameters.ticketprinter.buttonlist) {
                    let id = itemId.toString().replace(/\D/g,'');
                    let item = {id: id, type: 's', position: pos};
                    this.state.selectedItems.push(item);
                    pos += 1;
                }
            }
            if (typeof props.parameters.ticketprinter.home !== "undefined") {
                this.state.homeUrl = props.parameters.ticketprinter.home
            }
            if (typeof props.parameters.ticketprinter.name !== "undefined") {
                this.state.ticketPrinterName = props.parameters.ticketprinter.name
            }
            if (typeof props.parameters.template !== "undefined") {
                this.state.template = props.parameters.template
            }
        }

        if (!this.state.edit) {
            this.fetchRegister();
            setInterval(applicationRegisterTable.decorateApplicationTable(this), 1000);
        }
    }

    buildHost() {
        return document.location.origin;
    }

    buildParameters() {
        const itemList = this.state.selectedItems.map(item => `${item.type}${item.id}`).join(',')
        let parameters = []

        if (itemList) {
            parameters.push(`ticketprinter[buttonlist]=${itemList}`)
        }

        if (this.state.ticketPrinterName) {
            parameters.push(`ticketprinter[name]=${this.state.ticketPrinterName}`)
        }

        if (this.state.homeUrl) {
            parameters.push(`ticketprinter[home]=${this.state.homeUrl}`)
        }

        if (this.state.template !== 'default') {
            parameters.push(`template=${this.state.template}`)
        }

        if (this.state.sid) {
            parameters.push(`sid=${this.state.sid}`)
        }

        return parameters
    }

    buildUrl() {
        const baseUrl = `${this.buildHost()}${this.props.config.ticketprinter.baseUrl}`
        let parameters = this.buildParameters()

        return `${baseUrl}?${parameters.join('&')}`
    }

    renderNumberSelect(value, onNumberChange) {
        const onChange = ev => onNumberChange(ev.target.value)

        const usedSlots = this.state.selectedItems.map(item => item.position)
        const availableSlots = [null, 1, 2, 3, 4, 5, 6].filter(slot => usedSlots.indexOf(slot) < 0 || slot === value)

        return (
            <select {... { onChange, value }} className="form-control">
                {availableSlots.map(n => <option key={n} value={n}>{n ? `Position ${n}` : 'nicht anzeigen'}</option>)}
            </select>
        )
    }

    showItem(item, position) {
        const items = this.state.selectedItems.filter(i => i.id !== item.id)
        const newItem = Object.assign({}, item, { position })
        items.push(newItem)
        items.sort((a, b) => {
            const aPos = a.position
            const bPos = b.position

            if (aPos < bPos) { return -1 }
            if (aPos > bPos) { return 1 }
            return 0
        })

        this.setState({ selectedItems: items })
    }

    hideItem(item) {
        const items = this.state.selectedItems.filter(i => i.id !== item.id)
        this.setState({ selectedItems: items })
    }

    renderItem(item) {
        const onChange = n => {
            const position = parseInt(n, 10)

            if (position) {
                this.showItem(item, position)
            } else {
                this.hideItem(item)
            }
        }

        const text = `${item.contact ? item.contact.name : item.name} ${item.shortName ? item.shortName : ""}`
        const prefix = item.type === 'c' ? 'Cluster: ' : ''
        const position = (this.state.selectedItems.filter(i => i.id === item.id)[0] || {}).position

        return (
            <div key={item.type + "-" + item.id} className="form-group--inline ticketprinter-config__item" >
                <label className="light">
                    <span>{prefix}{text}</span>
                    <Controls>
                        {this.renderNumberSelect(position, onChange)}
                    </Controls>
                </label>
            </div >
        )
    }

    renderScopes(scopes) {
        if (scopes.length > 0) {
            return (
                <fieldset key="scopeList">
                    <legend className="label">Standorte</legend>
                    {scopes.map(this.renderItem.bind(this))}
                </fieldset>
            )
        }
    }

    renderClusters(clusters) {
        if (clusters.length > 0) {
            return (
                <fieldset key="clusterList">
                    <legend className="label">Standort­gruppe</legend>
                    {clusters.map(this.renderItem.bind(this))}
                </fieldset>
            )
        }
    }

    renderDepartment(department) {
        return (
            <div key={department.id}>
                <h2 className="block__heading">{department.name}</h2>
                {this.renderScopes(department.scopes)}
                {/* this.renderClusters(department.clusters) */}
            </div>
        )
    }

    fetchRegister() {
        if (this.state.scopeIds.length === 0) return

        const url = `${this.includeUrl}/applicationregister/ticketprinter/table/?scopeIds=${this.state.scopeIds}`

        $.ajax(url, {method: 'GET'}).done(responseData => {
            this.setState({ appList: {__html: responseData }})
        })
    }

    renderRegister() {
        if (this.state.appList.__html.length === 0) return
        return (
            <div>
                <h2>Aktive Wartenummernausgaben:</h2>
                <div dangerouslySetInnerHTML={this.state.appList}></div>
            </div>
        )
    }

    renderBackButton() {
        if (this.state.origin) {
            return (
                <button type="button" className="button" onClick={() => {this.navigateTo(this.state.origin)}}>
                    <i className="fas fa-times-circle">&nbsp;</i>
                    Zurück
                </button>
            )
        }
    }

    navigateTo(url) {
        document.location.href = url
    }

    render() {
        const editMode = this.state.edit;
        const formAttributes = {};

        if (editMode) {
            formAttributes.action = './?origin=' + this.state.origin;
            formAttributes.method = 'POST';
        }

        const onNameChange = (name, value) => {
            this.setState({ ticketPrinterName: value })
        }

        const onHomeChange = (name, value) => {
            this.setState({ homeUrl: value })
        }

        const onTemplateStatusChange = (_, value) => {
            this.setState({
                template: value
            })
        }

        const generatedUrl = this.buildUrl()

        return (
            <form className="form--base ticketprinter-config" { ...formAttributes }>
                {this.state.departments.map(this.renderDepartment.bind(this))}
                <fieldset key="ticketprinter-fieldset">
                    <FormGroup key="ticketprinter-name">
                        <Label attributes={{ "htmlFor": "ticketprinterName" }} value="Name zur internen Identifikation (optional)"></Label>
                        <Controls>
                            <Inputs.Text
                                attributes={{ "id": "ticketprinterName" }}
                                onChange={onNameChange}
                                value={this.state.ticketPrinterName}
                            />
                        </Controls>
                    </FormGroup>
                    <FormGroup key="ticketprinter-Starturl">
                        <Label attributes={{ "htmlFor": "ticketprinterStarturl" }} value="StartUrl (optional)"></Label>
                        <Controls>
                            <Inputs.Text
                                attributes={{ "id": "ticketprinterStarturl", "aria-describedby": "help_ticketprinterStarturl" }}
                                onChange={onHomeChange}
                                value={this.state.homeUrl}
                            />
                            <Description attributes={{ "id": "help_ticketprinterStarturl" }}>Tragen Sie eine alternative URL ein, wenn nach der Ausgabe einer Wartenummer eine alternative Startseite aufgerufen werden soll</Description>
                        </Controls>
                    </FormGroup>
                    <FormGroup key="ticketprinter-layout">
                        <Label attributes={{ "htmlFor": "ticketprinterLayout" }} value="Layout"></Label>
                        <Controls>
                            <Select
                                attributes={{ "id": "ticketprinterLayout" }}
                                options={[
                                    { name: 'Standard', value: 'default' },
                                    { name: 'Mit wartenden Kunden', value: 'wait' },
                                    { name: 'Mit voraussichtlicher Wartezeit', value: 'time' },
                                    { name: 'Mit wartenden Kunden und voraussichtlicher Wartezeit', value: 'timewait' }
                                ]}
                                value={this.state.template}
                                onChange={onTemplateStatusChange} />
                        </Controls>
                    </FormGroup>
                    <FormGroup key="ticketprinter-url">
                        <Label attributes={{ "htmlFor": "ticketprinterUrl" }} value="URL"></Label>
                        <Controls>
                            <Inputs.Text
                                value={generatedUrl} attributes={{ readOnly: true, "id": "ticketprinterUrl" }} />
                        </Controls>
                    </FormGroup>
                    <div className="form-actions">
                        { editMode ? (
                            <React.Fragment>
                                <input type="hidden" name="parameters" value={this.buildParameters().join('&')} />
                                { this.renderBackButton() }
                                <button type="submit" className="button"><i className="fas fa-save">&nbsp;</i>Aktuelle Konfiguration der gewählten Wartenummernausgabe zuordnen</button>
                            </React.Fragment>
                        ) : (
                            <a href={generatedUrl} target="_blank" rel="noopener noreferrer" className="button button-submit"><i className="fas fa-external-link-alt"></i> Aktuelle Kiosk-Konfiguration in einem neuen Fenster öffnen</a>
                        )}
                    </div>
                </fieldset>
                { this.renderRegister() }
            </form >
        )
    }
}

TicketPrinterConfigView.propTypes = {
    departments: PropTypes.array,
    organisation: PropTypes.object,
    config: PropTypes.shape({
        ticketprinter: PropTypes.shape({
            baseUrl: PropTypes.object
        })
    })
}

export default TicketPrinterConfigView
