import React, { Component } from 'react'
import PropTypes from 'prop-types'

import * as Inputs from '../../../lib/inputs'
import QrCodeView from "../qrCode";
import applicationRegisterTable from "../../../element/table/applicationRegister";
import $ from "jquery";

const { FormGroup, Label, Controls, Select } = Inputs

const readPropsCluster = cluster => {
    const { name, id } = cluster

    return {
        type: 'cluster',
        id,
        name
    }
}

const readPropsScope = scope => {
    const { shortName, contact, id } = scope
    return {
        type: 'scope',
        id,
        shortName,
        contact
    }
}

class CallDisplayConfigView extends Component {
    constructor(props) {
        super(props)
        this.includeUrl = props.includeurl || "";
        //console.log('CallDisplayConfigView::constructor', props)

        this.state = {
            edit: Boolean(props.edit),
            selectedItems: [], //props.parameters ? props.parameters.collections : [],
            departments: props.departments.map(department => {
                const { name, id, scopes = [], clusters = [] } = department
                return {
                    name,
                    id,
                    scopes: scopes.map(readPropsScope),
                    clusters: clusters.map(readPropsCluster)
                }
            }),
            queueStatus: 'all',
            template: 'defaultplatz',
            webtemplate: 'defaultplatz',
            hmac: '',
            enableQrCode: false,
            showQrCode: false,
            displayId: props.displayid,
            scopeIds: props.scopeids,
            appList: {__html: ''},
            dataTable: null,
            origin: props.origin
        }

        if (props.parameters && props.parameters.collections) {
            if (typeof props.parameters.collections.scopelist !== "undefined") {
                for (let itemId of props.parameters.collections.scopelist) {
                    let item = {id: itemId, type: 'scope'};
                    this.state.selectedItems.push(item);
                }
            }
            if (typeof props.parameters.collections.clusterlist !== "undefined") {
                for (let itemId of props.parameters.collections.clusterlist) {
                    let item = {id: itemId, type: 'cluster'};
                    this.state.selectedItems.push(item);
                }
            }
        }

        if (props.parameters) {
            if (typeof props.parameters.queue !== "undefined") {
                this.state.queueStatus = props.parameters.queue.status;
            }
            if (typeof props.parameters.qrcode !== "undefined") {
                this.state.enableQrCode = Boolean(Number(props.parameters.qrcode));
            }
            if (typeof props.parameters.template !== "undefined") {
                this.state.template = props.parameters.template;
            }
        }

        this.signParameters(this.state)

        if (!this.state.edit) {
            this.fetchRegister()
            setInterval(applicationRegisterTable.decorateApplicationTable(this), 1000);
        }
    }

    getSelectedItemsCollection(state) {
        return state.selectedItems.reduce((carry, current) => {
            if (current.type === "cluster") {
                carry.clusterlist.push(current.id)
            } else if (current.type === "scope") {
                carry.scopelist.push(current.id)
            }

            return carry
        }, {
            scopelist: [],
            clusterlist: []
        })
    }

    buildHost() {
        return document.location.origin;
    }

    buildCalldisplayUrl() {
        const baseUrl  = this.props.config.calldisplay.baseUrl
        let parameters = this.buildParameters(false);
        parameters.push(`sid=${this.state.displayId}`)

        return `${this.buildHost()}${baseUrl}?${parameters.join('&')}`
    }

    buildWebcalldisplayUrl() {
        const baseUrl  = this.props.config.webcalldisplay.baseUrl
        let parameters = this.buildParameters(true, 'webcalldisplay');

        return `${this.props.config.appointments.urlAppointments}${baseUrl}?${parameters.join('&')}`
    }

    buildParameters(hashParameters, target = 'calldisplay') {
        const collections = this.getSelectedItemsCollection(this.state)
        let queryParts = []

        if (collections.scopelist.length > 0) {
            queryParts.push(`collections[scopelist]=${collections.scopelist.join(",")}`)
        }

        if (collections.clusterlist.length > 0) {
            queryParts.push(`collections[clusterlist]=${collections.clusterlist.join(",")}`)
        }

        if (this.state.queueStatus !== 'all') {
            queryParts.push(`queue[status]=${this.state.queueStatus}`)
        }

        if (target == 'calldisplay' && this.state.template !== 'default') {
            queryParts.push(`template=${this.state.template}`)
        }

        if (target == 'webcalldisplay' && this.state.template !== 'default') {
            queryParts.push(`template=${this.state.webtemplate}`)
        }

        if (! hashParameters && this.state.enableQrCode) {
            queryParts.push(`qrcode=1`)
        }

        if (hashParameters) {
            queryParts.push(`hmac=${this.state.hmac}`)
        }

        return queryParts
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevState.selectedItems !== this.state.selectedItems
            || prevState.queueStatus !== this.state.queueStatus
        ) {
            this.signParameters(this.state)
        }
    }

    signParameters(state) {
        const collections = this.getSelectedItemsCollection(state)

        let signingData = {
            'section': 'webcalldisplay',
            'parameters': {}
        }

        if (collections.scopelist.length > 0) {
            signingData.parameters.collections = signingData.parameters.collections || {}
            signingData.parameters.collections.scopelist = collections.scopelist.join(",")
        }
        if (collections.clusterlist.length > 0) {
            signingData.parameters.collections = signingData.parameters.collections || {}
            signingData.parameters.collections.clusterlist = collections.clusterlist.join(",")
        }
        if (this.state.queueStatus !== 'all') {
            signingData.parameters.queue = {}
            signingData.parameters.queue.status = this.state.queueStatus
        }

        const signParametersUrl = this.includeUrl + '/sign/parameters/'
        fetch(
            signParametersUrl,
            {
                method: 'POST',
                cache: 'no-cache',
                headers: {'Accept': 'application/json', 'Content-Type': 'application/json'},
                body: JSON.stringify(signingData)
            }
        ).then(response => response.json()).then(data => this.setState({ hmac: data.hmac }))
    }

    toggleQrCodeView() {
        this.setState({
            showQrCode: !this.state.showQrCode
        });
    }

    renderCheckbox(enabled, onShowChange, label) {
        const onChange = () => onShowChange(!enabled)

        return (
            <Inputs.Checkbox checked={enabled} {...{ onChange, label }} />
        )
    }

    showItem(item) {
        const items = this.state.selectedItems.filter(i => i.id !== item.id || (i.id == item.id && i.type !== item.type))
        const newItem = Object.assign({}, item)
        items.push(newItem)
        this.setState({ selectedItems: items })
    }

    hideItem(item) {
        const items = this.state.selectedItems.filter(i => i.id !== item.id || (i.id == item.id && i.type !== item.type))
        this.setState({ selectedItems: items })
    }

    renderItem(item) {
        const onChange = show => {
            if (show) {
                this.showItem(item)
            } else {
                this.hideItem(item)
            }
        }

        const text = `${item.contact ? item.contact.name : item.name} ${item.shortName ? item.shortName : ""}`
        const prefix = item.type === 'cluster' ? 'Cluster: ' : ''

        const itemEnabled = this.state.selectedItems.reduce((carry, current) => {
            return carry || (current.id === item.id && current.type === item.type)
        }, false)
        return (
            <li>
                <div key={item.id} className="form-check ticketprinter-config__item">
                    {this.renderCheckbox(itemEnabled, onChange, prefix + text)}
                </div>
            </li>
        )
    }

    renderQrCodeEnabled() {
        const onChange = () => {
            this.setState({
                enableQrCode: !this.state.enableQrCode
            });
        }
        return (
            <fieldset>
                <legend className="label">QR-Code für Aufrufanzeige</legend>
                <div key="qrcodeEnabled" className="form-check ticketprinter-config__item">
                    {this.renderCheckbox(this.state.enableQrCode, onChange, "QR-Code anzeigen")}
                </div>
            </fieldset>
        )
    }

    renderScopes(scopes) {
        if (scopes.length > 0) {
            return (
                <fieldset>
                    <legend className="label">Standorte</legend>
                    <ul aria-label="Standortliste" className="checkbox-list">
                    {scopes.map(this.renderItem.bind(this))}
                    </ul>
                </fieldset>
            )
        }
    }

    renderClusters(clusters) {
        if (clusters.length > 0) {
            return (
                <fieldset>
                    <legend className="label">Standort­gruppe</legend>
                    <ul aria-label="Standortclusterliste" className="checkbox-list">
                    {clusters.map(this.renderItem.bind(this))}
                    </ul>
                </fieldset>
            )
        }
    }

    renderDepartment(department) {
        return (
            <div key={department.id}>
                <h2 className="block__heading">{department.name}</h2>
                {this.renderScopes(department.scopes)}
                {this.renderClusters(department.clusters)}
            </div>
        )
    }

    fetchRegister() {
        if (this.state.scopeIds.length === 0) return

        const url = `${this.includeUrl}/applicationregister/calldisplay/table/?scopeIds=${this.state.scopeIds}`

        $.ajax(url, {method: 'GET'}).done(responseData => {
            this.setState({ appList: {__html: responseData }})
        })
    }

    renderRegister() {
        if (this.state.appList.__html.length === 0) return
        return (
            <div>
                <h2>Aktive Aufrufanzeigen:</h2>
                <div dangerouslySetInnerHTML={this.state.appList}></div>
            </div>
        )
    }

    renderBackButton() {
        if (this.state.origin) {
            return (
                <button type="button" className="button" onClick={() => {this.navigateTo(this.state.origin)}}>
                    <i className="fas fa-times-circle">&nbsp;</i>
                    Zurück
                </button>
            )
        }
    }

    navigateTo(url) {
        document.location.href = url
    }

    render() {
        const calldisplayUrl = this.buildCalldisplayUrl();
        const webcalldisplayUrl = this.buildWebcalldisplayUrl();
        const editMode = this.state.edit;
        const formAttributes = {};

        if (editMode) {
            formAttributes.action = './?origin=' + this.state.origin;
            formAttributes.method = 'POST';
        }

        const onQueueStatusChange = (_, value) => {
            this.setState({
                queueStatus: value
            })
        }

        const onTemplateStatusChange = (_, value) => {
            this.setState({
                template: value
            })
        }

        const onWebTemplateStatusChange = (_, value) => {
            this.setState({
                webtemplate: value
            })
        }

        return (
            <form className="form--base form-group calldisplay-config" { ...formAttributes }>
                {this.state.departments.map(this.renderDepartment.bind(this))}
                {this.renderQrCodeEnabled()}

                <FormGroup>
                    <Label 
                        attributes={{ "htmlFor": "visibleCalls" }} 
                        value="Angezeigte Aufrufe">
                    </Label>
                    <Controls>
                        <Select
                            options={[{ name: 'Alle', value: 'all' }, { name: "Nur Abholer", value: 'pickup' }, { name: "Spontan- und Terminkunden", value: 'called' }]}
                            value={this.state.queueStatus}
                            attributes={{ "id": "visibleCalls" }}
                            onChange={onQueueStatusChange} />
                    </Controls>
                </FormGroup>
                <FormGroup>
                    <Label attributes={{ "htmlFor": "calldisplayLayout" }} value="Layout Aufrufanzeige"></Label>
                    <Controls>
                        <Select
                            attributes={{ "id": "calldisplayLayout" }}
                            options={[
                                { name: 'Uhrzeit, 6-12 Aufrufe | Platz', value: 'defaultplatz' },
                                { name: 'Uhrzeit, 6-12 Aufrufe | Raum', value: 'defaultraum' },
                                { name: 'Uhrzeit, 6 Aufrufe | Platz', value: 'clock5platz' },
                                { name: 'Uhrzeit, Anzahl Wartende, 6-12 Aufrufe | Platz', value: 'clocknrplatz' },
                                { name: 'Uhrzeit, Anzahl Wartende, 6-12 Aufrufe | Raum', value: 'clocknrraum' },
                                { name: 'Uhrzeit, Anzahl Wartende, Wartezeit, 6-12 Aufrufe | Platz', value: 'clocknrwaitplatz' },
                                { name: 'Uhrzeit, Anzahl Wartende, Wartezeit, 6-12 Aufrufe | Raum', value: 'clocknrwaitraum' },
                                { name: '6-18 Aufrufe | Platz', value: 'raw18platz' }
                            ]}
                            value={this.state.template}
                            onChange={onTemplateStatusChange} />
                    </Controls>
                </FormGroup>
                <FormGroup>
                    <Label attributes={{ "htmlFor": "calldisplayUrl" }} value="URL"></Label>
                    <Controls>
                        <Inputs.Text
                            value={calldisplayUrl}
                            attributes={{ readOnly: true, id: "calldisplayUrl" }} />
                    </Controls>
                </FormGroup>
                <div className="form-actions">
                    { editMode ? (
                        <React.Fragment>
                            <input type="hidden" name="parameters" value={this.buildParameters(false).join('&')} />
                            { this.renderBackButton() }
                            <button type="submit" className="button"><i className="fas fa-save">&nbsp;</i>Aktuelle Konfiguration der gewählten Aufrufanzeige zuordnen</button>
                        </React.Fragment>
                    ) : (
                        <a href={calldisplayUrl} target="_blank" rel="noopener noreferrer" className="button button-submit"><i className="fas fa-external-link-alt"></i> Aktuelle Konfiguration in einem neuen Fenster öffnen</a>
                    )}
                </div>

                <FormGroup>
                    <Label attributes={{ "htmlFor": "webcalldisplayLayout" }} value="Layout mobile Aufrufanzeige"></Label>
                    <Controls>
                        <Select
                            attributes={{ "id": "webcalldisplayLayout" }}
                            options={[
                                { name: '6-12 Aufrufe | Platz', value: 'defaultplatz' },
                                { name: '6-12 Aufrufe | Raum', value: 'defaultraum' },
                                { name: 'Anzahl Wartende, 6-12 Aufrufe | Platz', value: 'nrwaitplatz' },
                                { name: 'Anzahl Wartende, 6-12 Aufrufe | Raum', value: 'nrwaitraum' },
                                { name: 'Wartezeit, 6-12 Aufrufe | Platz', value: 'nrtimeplatz' },
                                { name: 'Wartezeit, 6-12 Aufrufe | Raum', value: 'nrtimeraum' },
                                { name: 'Anzahl Wartende, Wartezeit, 6-12 Aufrufe | Platz', value: 'nrwaittimeplatz' },
                                { name: 'Anzahl Wartende, Wartezeit, 6-12 Aufrufe | Raum', value: 'nrwaittimeraum' }
                            ]}
                            value={this.state.webtemplate}
                            onChange={onWebTemplateStatusChange} />
                    </Controls>
                </FormGroup>
                <FormGroup>
                    <Label attributes={{ "htmlFor": "webcalldisplayUrl" }} value="Webcall Display URL"></Label>
                    <Controls>
                        <Inputs.Text
                            value={webcalldisplayUrl}
                            attributes={{ readOnly: true, id: "webcalldisplayUrl" }} />
                    </Controls>
                </FormGroup>
                <div className="form-actions">
                    <button className="button" onClick={(event) => {event.preventDefault(); this.toggleQrCodeView();}}>QR-Code anzeigen / drucken</button>
                    <a href={webcalldisplayUrl} target="_blank" rel="noopener noreferrer" className="button button-submit"><i className="fas fa-external-link-alt"></i> in der mobilen Anzeige öffnen</a>
                </div>
                { this.renderRegister() }
                { this.state.showQrCode ? <QrCodeView text='QrCode für die mobile Ansicht des Aufrufsystems' targetUrl={webcalldisplayUrl} togglePopup={this.toggleQrCodeView.bind(this)} /> : null }
            </form>
        )
    }
}

CallDisplayConfigView.propTypes = {
    includeUrl: PropTypes.string,
    departments: PropTypes.array,
    organisation: PropTypes.object,
    workstation: PropTypes.object,
    config: PropTypes.shape({
        calldisplay: PropTypes.shape({
            baseUrl: PropTypes.object
        }),
        webcalldisplay: PropTypes.shape({
            baseUrl: PropTypes.object
        }),
        appointments: PropTypes.shape({
            urlAppointments: PropTypes.object
        })
    })
}

export default CallDisplayConfigView
