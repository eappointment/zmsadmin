import React, { Component } from 'react'
import PropTypes from 'prop-types'
import $ from "jquery"
import * as Inputs from '../../lib/inputs'
import { getEntity } from '../../lib/schema'
import { isValidJson } from '../../lib/utils'

const renderProvider = (provider, index, onChange, onEventDelete, labels, descriptions, source, errors) => {
    const formName = `providers[${index}]`

    const onDataChange = (ev) => {
        let clearedClassName = document.getElementById('help_providersData' + index).className.replace('message--error', '').trim()
        if (!isValidJson(ev.target.value)) {
            document.getElementById('help_providersData' + index).className = clearedClassName  + ' message--error'
        } else {
            document.getElementById('help_providersData' + index).className = clearedClassName
        }
    }

    let dataValue = (provider.data) ? JSON.stringify(provider.data) : ''
        dataValue = (dataValue === '[]') ? '{}' : dataValue

    return (
        <tr key={index} className="provider-item">
            <td className="provider-item__id" width="auto">
                <Inputs.Text
                    name={`${formName}[id]`}
                    placeholder={labels.id}
                    value={provider.id}
                    attributes={{ "readOnly": "1", "aria-label": "Laufende Nummer" }}
                />
            </td>
            <td className="provider-item__name" width="auto">
                <Inputs.Text
                    name={`${formName}[name]`}
                    placeholder={labels.name}
                    value={provider.name}
                    onChange={onChange}
                    attributes={{ "aria-label": "Bezeichnung" }}
                />
                {(errors && errors.providers && provider.id in errors.providers && 'name' in errors.providers[provider.id]) ? (
                    <Inputs.ErrorDescription
                        messages={errors.providers[provider.id].name.messages}
                        attributes={{ "id": "help_mandantLabel" }}
                    />
                ) : ''}
            </td>
            <td className="provider-item__link">
                <Inputs.FormGroup>
                    <Inputs.Label
                        value={`${labels.url}`}
                        attributes={{ "htmlFor": `providersUrl${index}` }}
                    />
                    <Inputs.Controls>
                        <Inputs.Text
                            name={`${formName}[link]`}
                            placeholder={labels.url}
                            value={provider.link}
                            onChange={onChange}
                            attributes={{ "id": `providersUrl${index}` }}
                        />
                    </Inputs.Controls>
                </Inputs.FormGroup>
                <Inputs.FormGroup>
                    <Inputs.Label
                        value={labels.street}
                        attributes={{ "htmlFor": `providersStreet${index}` }}
                    />
                    <Inputs.Controls>
                        <Inputs.Text
                            name={`${formName}[contact][street]`}
                            placeholder={labels.street}
                            value={provider.contact.street}
                            onChange={onChange}
                            attributes={{ "id": `providersStreet${index}` }}
                        />
                    </Inputs.Controls>
                </Inputs.FormGroup>
                <Inputs.FormGroup>
                    <Inputs.Label
                        value={labels.streetNumber}
                        attributes={{ "htmlFor": `providersStreetnumber${index}` }}
                    />
                    <Inputs.Controls>
                        <Inputs.Text
                            name={`${formName}[contact][streetNumber]`}
                            placeholder={labels.streetNumber}
                            value={provider.contact.streetNumber}
                            onChange={onChange}
                            attributes={{ "id": `providersStreetnumber${index}` }}
                        />
                    </Inputs.Controls>
                </Inputs.FormGroup>
                <Inputs.FormGroup>
                    <Inputs.Label
                        value={labels.postalCode}
                        attributes={{ "htmlFor": `providersPostalcode${index}` }}
                    />
                    <Inputs.Controls>
                        <Inputs.Text
                            name={`${formName}[contact][postalCode]`}
                            placeholder={labels.postalCode}
                            value={provider.contact.postalCode}
                            onChange={onChange}
                            attributes={{ "id": `providersPostalcode${index}` }}
                        />
                    </Inputs.Controls>
                </Inputs.FormGroup>
                <Inputs.FormGroup>
                    <Inputs.Label
                        value={labels.city}
                        attributes={{ "htmlFor": `providersCity${index}` }}
                    />
                    <Inputs.Controls>
                        <Inputs.Text
                            name={`${formName}[contact][city]`}
                            placeholder={labels.city}
                            value={provider.contact.city}
                            onChange={onChange}
                            attributes={{ "id": `providersCity${index}` }}
                        />
                    </Inputs.Controls>
                </Inputs.FormGroup>
                <Inputs.FormGroup>
                    <Inputs.Label
                        value={labels.data}
                        attributes={{ "htmlFor": `providersData${index}` }}
                    />
                    <Inputs.Controls>
                        <Inputs.Textarea
                            name={`${formName}[data]`}
                            value={dataValue}
                            placeholder='{}'
                            onChange={onChange}
                            attributes={{
                                "id": `providersData${index}`,
                                "aria-describedby": `help_providersData${index}`,
                                "onInput": onDataChange
                            }}
                        />
                        {(errors && errors.providers && provider.id in errors.providers && 'data' in errors.providers[provider.id]) ? (
                            <Inputs.ErrorDescription
                                messages={errors.data.messages}
                                attributes={{ "id": `help_providersData${index}` }}
                            />
                        ) : (
                            <Inputs.Description
                                value={descriptions.data}
                                attributes={{ "id": `help_providersData${index}` }}
                            />
                        )}
                    </Inputs.Controls>
                </Inputs.FormGroup>
                <Inputs.Hidden
                    name={`${formName}[source]`}
                    value={source}
                />
            </td>
            <td className="provider-item__delete">
                <div className="form-check">
                    <label className="checkboxdeselect provider__delete-button form-check-label color-destructive"
                           onClick={(event) => onEventDelete(event, index)}
                           onKeyDown={(event) => onEventDelete(event, index)}
                           role="button" tabIndex="0" aria-label="Diesen Datensatz löschen">
                        <i className="fas fa-trash" aria-hidden="true"/><span>&nbsp;Löschen</span>
                    </label>
                </div>
            </td>
        </tr>
    )
}

class ProvidersView extends Component {
    constructor(props) {
        super(props)
    }

    getNextId() {
        let nextId = Number(this.props.source.providers.length ? this.props.source.providers[this.props.source.providers.length - 1].id : 0) + 1
        return nextId;
    }

    getProvidersWithLabels(onChange, onEventDelete) {
        return this.props.source.providers.map((provider, index) => renderProvider(provider, index, onChange, onEventDelete, this.props.labelsproviders, this.props.descriptions, this.props.source.source, this.props.errors))
    }

    hideDeleteButton() {
        $('.provider-item').each((index, item) => {
            if ($(item).find('.provider-item__name input').val()) {
                $(item).find('.provider__delete-button').css("visibility", "hidden");
            }
            if ($(item).find('.provider-item__name input').val() === ''
                && $(item).find('.message--error').length
            ) {
                $(item).find('.provider__delete-button').css("visibility", "hidden");
            }
        })
    }

    componentDidMount() {
        console.log("mounted provider component")
        this.hideDeleteButton()
    }

    componentDidUpdate() {
        //console.log("updated provider component")
    }

    render() {
        const onNewClick = ev => {
            ev.preventDefault()
            getEntity('provider').then((entity) => {
                entity.id = this.getNextId()
                entity.source = this.props.source.source
                this.props.addNewHandler('providers', [entity])
            })
        }

        const onEventDelete = (event, index) => {
            if ((event.type === 'keyup' || event.type === 'keydown') && !(event.key === " " || event.key === "Enter")) {
                return
            }
            event.stopPropagation()
            event.preventDefault()
            this.props.deleteHandler('providers', index)
        }

        const onChange = (field, value) => {
            this.props.changeHandler(field, value)
        }

        return (
            <div className="table-responsive-wrapper department-providers__list" aria-live="polite" id="liveregionProvidersList">
                <table className="table--base">
                    <thead>
                        <tr>
                            <th>LfdNr.</th>
                            <th>Name</th>
                            <th>Link und weitere Daten</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.getProvidersWithLabels(onChange, onEventDelete)}
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colSpan="4">
                                <p>
                                    <Inputs.Description
                                        value={this.props.descriptions.delete}
                                    />
                                </p>
                            </td>
                        </tr>
                    </tfoot>
                </table>
                <div className="table-action-link">
                    <button className="link button-default" onClick={onNewClick}><i className="fas fa-plus-square color-positive"></i> Neuer Dienstleister</button>
                </div>
            </div>
        )
    }
}

ProvidersView.propTypes = {
    labelsproviders: PropTypes.object.isRequired,
    descriptions: PropTypes.object.isRequired,
    source: PropTypes.object.isRequired,
    changeHandler: PropTypes.func,
    addNewHandler: PropTypes.func,
    deleteHandler: PropTypes.func
}

export default ProvidersView
