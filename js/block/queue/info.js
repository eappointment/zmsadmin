import BaseView from "../../lib/baseview"
import $ from "jquery"

class View extends BaseView {

    constructor (element, options) {
        super(element, options);
        this.$main = $(element);
        this.selectedDate = options.selectedDate;
        this.selectedScope = options.selectedScope;
        this.includeUrl = options.includeUrl || "";
        this.showLoader = options.showLoader || false;
        this.onGhostWorkstationChange = options.onGhostWorkstationChange || (() => {});
        this.bindPublicMethods('load');
        $.ajaxSetup({ cache: false });
        this.bindEvents();
        //console.log('Component: Queue Info', this, options);
        this.load();
    }

    load() {
        let url = `${this.includeUrl}/counter/queueInfo/`
            url += `?selecteddate=${this.selectedDate}&selectedscope=${this.selectedScope}`
        return this.loadContent(url, 'GET', null, null, this.showLoader);
    }

    bindEvents() {
        this.$main.off('click').on('change', 'select[name=count]', (event)=> {
            this.onGhostWorkstationChange(this.$main, event)
        })
    }
}

export default View;
