import BaseView from "../../../lib/baseview"
import $ from "jquery"
import {stopEvent} from "../../../lib/utils";

class ActionDialogView extends BaseView {

    constructor(element, options) {
        super(element)
        this.$main = $(element)
        this.options = options
        this.bindPublicMethods('getSimpleConfirmHtml')
        this.bindPublicMethods('getYesNoHtml')
        this.bindPublicMethods('initDeleteDialogs')
    }

    getSimpleConfirmHtml(message, confirmButtonHtml = null) {
        if (!confirmButtonHtml) {
            confirmButtonHtml = '<button type="button" class="button button-ok">' +
                this.translate('Ok') +
                '</button>'
        }

        return '<div class="dialog" xmlns="http://www.w3.org/1999/html">' +
            '<div class="board__heading board__actions"><h2>' +
                this.translate('Bestätigung notwendig') +
                '</h2></div>' +
                '<div class="panel--light">' +
                '<div class="panel--heavy"><span class="board__message">' +
                this.translate(message) +
                '</span></div>' +
                '<div class="form-actions buttons-row"><div class="controls">' +
                    confirmButtonHtml +
                    '<button type="button" class="button button-abort">' +
                    this.translate('Abbrechen') +
                    '</button>' +
            '</div></div></div></div>'
    }

    getYesNoHtml(message, yesButton = null) {
        if (!yesButton) {
            yesButton = '<button type="button" class="button button-ok">' +
                this.translate('Ja') +
                '</button>'
        }
        return '<div class="dialog" xmlns="http://www.w3.org/1999/html">' +
            '<div class="board__heading board__actions"><h2>' +
            this.translate('Bestätigung notwendig') +
            '</h2></div>' +
            '<div class="panel--light">' +
            '<div class="panel--heavy"><span class="board__message">' +
            this.translate(message) +
            '</span></div>' +
            '<div class="form-actions buttons-row"><div class="controls">' +
            yesButton +
            '<button type="button" class="button button-abort">' +
            this.translate('Nein') +
            '</button>' +
            '</div></div></div></div>'
    }

    initDeleteDialogs() {
        this.$main.find('.button.type-delete').each((index, button) => {
            let topic = $(document).find('h1').first().text();
            topic = '' + topic;
            let words = topic.split(' ');
            let message = 'Soll diese(r) ' + words[0] + ' wirklich gelöscht werden?';
            let tmp = $(button).data('dialog')
            if (typeof tmp != 'undefined') {
                message = tmp
            }

            button.addEventListener('click', (event) => {
                stopEvent(event)
                BaseView.loadDialogStatic(this.buildDeleteDialogHtml(message, button), null, null, null, false, button)
            })
        })

        return this;
    }

    initYesNoDialogs() {
        this.$main.find('.button.type-yes-no').each((index, button) => {
            let message = $(button).data('dialog')
            if (!message) {
                message = button.innerText + ' ?';
            }
            let attributes = 'class="button button-ok"';
            $.each(button.attributes, function(index, attribute) {
                if (attribute.specified && attribute.name != 'class' && attribute.name != 'data-dialog') {
                    attributes += attribute.name + '="' + attribute.value + '" ';
                    if (attribute.name == 'href') { // todo: make link working within lightbox (affecting eventlisteners)
                        attributes += 'onclick="document.location=\'' + attribute.value + '\'" ';
                    }
                }
            })

            let yesButton = '<a ' + attributes + '>' + this.translate('Ja') + '</a> ';

            button.addEventListener('click', (event) => {
                stopEvent(event)
                BaseView.loadDialogStatic(this.getYesNoHtml(message, yesButton), () => {}, null, null, false, button)
            })
        })

        return this;
    }

    buildDeleteDialogHtml(message, button) {
        let attributes = ''
        $.each(button.attributes, function(index, attribute) {
            if (attribute.specified) {
                attributes += attribute.name + '="' + attribute.value + '" '
            }
        })

        return this.getSimpleConfirmHtml(message, '<a ' + attributes + '>' + button.innerText + '</a> ')
    }
}

export default ActionDialogView