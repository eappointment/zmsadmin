import BaseView from '../../lib/baseview'
import $ from 'jquery'
import { deepGet, tryJson } from '../../lib/utils'

class View extends BaseView {

    constructor (element, options) {
        super(element, options);
        this.$main = $(element);
        this.includeUrl = options.includeUrl;
        this.data = this.$main.data('header-scope');
        this.bindPublicMethods('refresh');
        this.render();
        this.refresh();
    }

    loadData() {
        const url = `${this.includeUrl}/workstation/status/`
        return new Promise((resolve, reject) => {
            $.ajax(url, {
                method: 'GET'
            }).done(data => {
                resolve(data)
            }).fail(err => {
                if (typeof err.statusText !== "undefined" && err.statusText.toLowerCase().includes('maintenance')) {
                    if (window.location.href.pathname == '/admin/workstation/') {
                        window.location.href = '/admin/workstation/select/';
                        reject(err)
                    }
                    return;
                }
                console.log('XHR error', url, err)
                reject(err)
            })
        })
    }

    refresh() {
        this.loadData()
            .then(data => {
                this.data = Object.assign({}, deepGet(tryJson(data), ['workstation', 'scope']));
                this.render()
            })
    }
    
    render() {
        const data = this.data;
        let length = 38;  // set to the number of characters you want to keep
        let scopeName = data.contact.name || '';
        if (typeof data.shortName === 'string') {
            scopeName += ' ' + data.shortName;
        }
        let trimmedName = (scopeName.length > length) ? scopeName.substr(0, length-4) + '...' : scopeName;
        this.$main.find('.header-scope-name').text(trimmedName)
        this.$main.find('.header-scope-title').attr('title', scopeName)
    }
}

export default View
