import $ from "jquery";
import dataTable from "../../lib/dataTables";
import language from "../../static/dt-de-DE.js";

const applicationRegisterTable = {
    getOptions: function(basePath, selector) {
        let wideColumns = ['ID', 'Standort', 'User-Agent', 'Parameter', 'Änderungen'];
        let truncateLength = 20;
        let searchableColumns = ['ID', 'Standort'];
        let truncateColumn = {"ID": true, "Standort": true, "User-Agent": true, "Parameter": true, "Änderungen": true};
        let columnLabels = $(selector + ' .label');
        let getCellContent = function(col, data) {
            if (col in truncateColumn && truncateColumn[col] && data.length > truncateLength) {
                return '<span title="' + data + '">' + data.substring(0, truncateLength - 3) + '...</span>';
            }
            return data;
        }

        let columnAdjustment = function (tableApi) {
            tableApi.columns.adjust();
        };

        let buildColumnOptions = function () {
            let columnOptions = [];

            for (let label of columnLabels) {
                let title = label.textContent.trim();
                let column = {data: title};
                if (title.length <= 1) {
                    column.orderable = false;
                }
                if (wideColumns.includes(title)) {
                    column['data'] = function (row, type, val, meta) {
                        if (type === 'set' && typeof row[title] === "undefined") {
                            row[title] = val;
                        } else if (type === 'set') {
                            return false;
                        } else if (type === 'display') {
                            return getCellContent(title, row[title])
                        }
                        return row[title];
                    }
                }
                columnOptions.push(column)
            }

            return columnOptions;
        };

        return {
            dom: 'it',
            scrollX: true,
            paging: false,
            columns: buildColumnOptions(),
            language: language,

            setWideColumns: function (columnTitles) {
                wideColumns = columnTitles;
            },

            setAdjustmentFunction: function (replacement) {
                columnAdjustment = replacement;
            },

            buildColumnOptions: buildColumnOptions,
            columnAdjustment:   function (tableApi) { columnAdjustment(tableApi); },

            initComplete: function () {
                let api  = this.api();
                api.columns().every(function () {
                    let column = this;
                    let title = column.header().textContent.trim();

                    let columnModifier = (event) => {
                        event.stopPropagation();
                        truncateColumn[title] = !truncateColumn[title];
                        api.rows().invalidate('data').draw(false);
                        columnAdjustment(api);
                        event.target.innerHTML = truncateColumn[title] ? '&#8676;&#8677;' : '&#8677;&#8676;';
                        event.target.title = truncateColumn[title] ? 'Aktivieren um die Spalte ' + title + ' zu expandieren' : 'Aktivieren um die Spalte ' + title + ' zu komprimieren';
                        event.target.ariaLabel = event.target.title;
                    }

                    if (wideColumns.includes(title)) {
                        let button = document.createElement('button');
                        button.type = 'button';
                        button.title = 'Aktivieren um die Spalte ' + title + ' zu expandieren';
                        button.ariaLabel = button.title;
                        button.innerHTML = '&#8676;&#8677;';
                        button.addEventListener('click', columnModifier);
                        button.addEventListener('keypress', (event) => {
                            event.preventDefault();
                            event.stopPropagation();
                        });
                        button.addEventListener('keyup', (event) => {
                            event.preventDefault();
                            if (!(event.code === 'Space' || event.code === 'Enter')) {
                                event.stopPropagation();
                                return;
                            }
                            columnModifier(event);
                        });
                        button.style.lineHeight = '0.8em';
                        button.style.marginBottom = '5px';

                        column.header().firstChild.prepend(' ');
                        column.header().firstChild.prepend(button);
                    }

                    if (searchableColumns.includes(title)) {
                        // Create input element
                        let input = document.createElement('input');
                        input.placeholder = 'Suche ' + title;
                        input.title       = 'Geben Sie einen Suchbegriff für die Spalte ' + title + ' per Tastatur ein.';
                        input.ariaLabel   = input.title;
                        input.style.width = '11em';
                        column.header().appendChild(input);

                        // Event listener for user input
                        input.addEventListener('keyup', () => {
                            if (column.search() !== this.value) {
                                column.search(input.value).draw();
                            }
                        });
                        input.addEventListener('keypress', (event) => {event.stopPropagation();});
                        input.addEventListener('click', (event) => {
                            event.stopPropagation();
                        });
                    }
                });

                columnAdjustment(api);
            }
        }
    },
    decorateApplicationTable: function (view) {
        return function () {
            if (view.state.dataTable === null && $('.application-register-table').length > 0) {
                let tableOptions = applicationRegisterTable.getOptions(view.includeUrl, '.application-register-table');
                tableOptions.setWideColumns(['ID', 'User-Agent', 'Parameter', 'Änderungen']);
                tableOptions.columns = tableOptions.buildColumnOptions();
                tableOptions.bAutoWidth = false;

                view.state.dataTable = dataTable('.application-register-table', tableOptions);
                view.state.dataTable.on('draw', function () {
                    tableOptions.columnAdjustment(view.state.dataTable);
                });
                setTimeout(function () {
                    $('table.dataTable').css('margin', '0 0 0 0'); // fixing a margin problem
                    view.state.dataTable.order([1, 'asc']).draw();
                    applicationRegisterTable.completeInitialization(view.state.dataTable);
                }, 1000);
            }
        };
    },

    completeInitialization: function(table) {
        table.on('stateSaveParams', function () {
            $('div.dataTables_scrollBody button').attr('tabIndex', '-1'); // avoid invisible elements to be navigable
            $('div.dataTables_scrollBody input').attr('tabIndex', '-1');
        });
        table.on('column-sizing', function () {
            $('div.dataTables_scrollBody button').attr('tabIndex', '-1'); // avoid invisible elements to be navigable
            $('div.dataTables_scrollBody input').attr('tabIndex', '-1');
        });
        $('div.dataTables_scrollHead').on('scroll', (event) => {
            let bodyScrollValue = $('div.dataTables_scrollBody')[0].scrollLeft;
            if (event.currentTarget.scrollLeft != bodyScrollValue) {
                $('div.dataTables_scrollBody')[0].scrollLeft = event.currentTarget.scrollLeft;
            }
        });
        $('div.dataTables_scrollHead table th').each((index, element) => {
            let handle = $(element);
            if (handle.hasClass('sorting')) {
                element.addEventListener('keypress', (event) => {
                    event.preventDefault();
                    event.stopPropagation();
                });
                element.addEventListener('keyup', (event) => {
                    if (event.code === 'Space') {
                        event.preventDefault();
                        event.stopPropagation();
                        let direction = 'asc';
                        if (handle.hasClass('sorting_asc')) {
                            direction = 'desc';
                        }
                        table.order(index, direction).draw();
                    }
                });
            }
        });
    }
}

export default applicationRegisterTable;
