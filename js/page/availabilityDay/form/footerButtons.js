import React from 'react'
import PropTypes from 'prop-types'

const FooterButtons = (props) => {
    const { hasConflicts, stateChanged, data, calculateslotsondemand, onNew, onPublish, onAbort, onCalculateSlots } = props

    return (
        <div className="form-actions" style={{"marginTop":"0", "padding":"0.75em"}}>    
                 <button title="Neue Öffnungszeit anlegen und bearbeiten" className="button button--diamond button-new" onClick={onNew} disabled={(stateChanged || hasConflicts || data)}>neue Öffnungszeit</button> 
                <button title="Alle Änderungen werden zurückgesetzt" className="button btn" type="abort" onClick={onAbort} disabled={(!stateChanged && !hasConflicts && !data)}>Abbrechen</button>
                { calculateslotsondemand ? 
                <button title="Terminslots neu berechnen" className="button button--diamond" type="save" value="calculate" onClick={onCalculateSlots} disabled={(stateChanged || hasConflicts)}>Terminslots neu kalkulieren
                </button> : null
                }
                <button title="Alle Änderungen werden gespeichert" className="button button--positive button-save" type="save" value="publish" onClick={onPublish} disabled={(! stateChanged || hasConflicts)}>Alle Änderungen aktivieren
                </button>
        </div>
       
    )
}

FooterButtons.propTypes = {
    data: PropTypes.object,
    hasConflicts: PropTypes.bool,
    calculateslotsondemand: PropTypes.bool,
    stateChanged: PropTypes.bool,
    onNew: PropTypes.func,
    onPublish: PropTypes.func,
    onAbort: PropTypes.func,
    onCalculateSlots: PropTypes.func
}

export default FooterButtons
