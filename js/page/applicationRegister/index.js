import dataTable from '../../lib/dataTables'
import BaseView from '../../lib/baseview'
import applicationRegisterTable from "../../element/table/applicationRegister";
import $ from 'jquery'

class View extends BaseView {

    constructor(element, options) {
        super(element);
        this.$main = $(element);
        this.includeUrl = options.includeurl;
        this.bindPublicMethods('load');

        this.load();
        this.table = null;
    }

    load() {
        $(window).on(
            'load', () => {
                this.table = dataTable('.application-register-table', applicationRegisterTable.getOptions(this.includeUrl, '.application-register-table'));
                let that = this;
                setTimeout(function () {
                    $('table.dataTable').css('margin', '0 0 0 0'); // fixing a margin problem
                    that.table.order([[1, 'asc']]).draw();
                    that.table.draw();
                    applicationRegisterTable.completeInitialization(that.table);
                }, 1000)
            }
        );
    }
}

export default View;
