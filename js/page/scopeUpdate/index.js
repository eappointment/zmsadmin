import BaseView from '../../lib/baseview'
import $ from 'jquery'
import {numericKeyEventsOnly, forceInputRange} from '../../lib/utils'

class ScopeUpdateView extends BaseView {

    constructor (element, options) {
        super(element);
        this.element = $(element);
        this.bindPublicMethods('bindEvents');
        $(this.bindEvents);
        console.log('Component: Scope Update');
    }

    bindEvents() {
        this.$main.on('keydown', '#startInDaysDefault', numericKeyEventsOnly);
        this.$main.on('input', '#startInDaysDefault', (ev) => {forceInputRange(ev.target, 0, 99)});

        this.$main.on('keydown', '#endInDaysDefault', numericKeyEventsOnly);
        this.$main.on('input', '#endInDaysDefault', (ev) => {forceInputRange(ev.target, 1, 366)});

        this.$main.on('keydown', '#reservationDuration', numericKeyEventsOnly);
        this.$main.on('input', '#reservationDuration', (ev) => {forceInputRange(ev.target, 1, 99)});

        this.$main.on('keydown', '#deallocationDuration', numericKeyEventsOnly);
        this.$main.on('input', '#deallocationDuration', (ev) => {forceInputRange(ev.target, 1, 99999)});

        this.$main.on('keydown', '#headsUpTime', numericKeyEventsOnly);
        this.$main.on('input', '#headsUpTime', (ev) => {forceInputRange(ev.target, 0, 60)});

        this.$main.on('keydown', '#notificationsDelay', numericKeyEventsOnly);
        this.$main.on('input', '#notificationsDelay', (ev) => {forceInputRange(ev.target, 0, 99)});

        this.$main.on('keydown', '#callCountMax', numericKeyEventsOnly);
        this.$main.on('input', '#callCountMax', (ev) => {forceInputRange(ev.target, 0, 9999)});

        this.$main.on('keydown', '#firstNumber', numericKeyEventsOnly);
        this.$main.on('input', '#firstNumber', (ev) => {forceInputRange(ev.target, 0, 99999999)});

        this.$main.on('keydown', '#lastNumber', numericKeyEventsOnly);
        this.$main.on('input', '#lastNumber', (ev) => {forceInputRange(ev.target, 0, 99999999)});

        this.$main.on('keydown', '#maxNumberContingent', numericKeyEventsOnly);
        this.$main.on('input', '#maxNumberContingent', (ev) => {forceInputRange(ev.target, 0, 99999999)});

        this.$main.on('keydown', '#processingTimeAverage', numericKeyEventsOnly);
        this.$main.on('input', '#processingTimeAverage', (ev) => {forceInputRange(ev.target, 1, 59)});
    }
}

export default ScopeUpdateView;
