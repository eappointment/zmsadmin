import BaseView from '../../lib/baseview'
import Datepicker from '../../lib/inputs/date'
import moment from 'moment/min/moment-with-locales';
import { addDays, addMinutes } from 'date-fns';
import $ from 'jquery'
import {createRoot} from "react-dom/client";
import setHours from "date-fns/setHours";
import setMinutes from "date-fns/setMinutes";


class View extends BaseView {

    constructor(element, options) {
        super(element);
        this.$main = $(element);
        this.reloadTimer;
        this.reloadInterval = options.reloadinterval | 5000;
        this.includeUrl = options.includeurl;
        this.scheduleData = options.schedule;
        this.currentDate;
        this.startDate;
        this.endDate;
        this.minTime;
        this.maxTime;
        this.bindPublicMethods(
            'bindEvents'
        );
        $(() => {
            if (this.$main.hasClass("edit")) {
                this.initRepetiveTimeView();
            } else {
                this.fetchStatsList();
                this.setReloadTimer();
            }
            this.bindEvents();
        });
    }

    bindEvents() {
        this.$main.off('click').on('change', '#scheduleRepetition', (event) => {
            this.onChangeRepitition(event);
        }).on('click', '#btn_announcement_edit', () => {
            $('#announcement_preview').hide();
            $('#announcement_edit').show();
            return false;
        }).on('click', '#btn_documentBody_edit', () => {
            $('#documentBody_preview').hide();
            $('#documentBody_edit').show();
            return false;
        });
    }

    setDates() {
        this.currentDate = new Date();
        if (this.scheduleData && this.scheduleData.timeString && this.scheduleData.timeString.length > 0 && this.scheduleData.isRepetitive === false) {
            this.startDate = moment(this.scheduleData.timeString, 'YYYY-MM-DD HH:mm').toDate();
            this.endDate = addMinutes(this.startDate, this.scheduleData.duration);
        } else {
            this.startDate = addMinutes(this.roundToNearestMinute(this.currentDate), 20)
            this.endDate = addMinutes(this.roundToNearestMinute(this.currentDate), 40)
        }
        if (this.startDate.toLocaleDateString('de-DE') == this.currentDate.toLocaleDateString('de-DE')) {
            this.minTime = setHours(setMinutes(this.currentDate, this.currentDate.getMinutes()), this.currentDate.getHours())
            this.maxTime = setHours(setMinutes(this.currentDate, 1), 23)
        }
    }

    roundToNearestMinute(date, closestMinute = 10) {
        let coeff = 1000 * 60 * closestMinute;
        return new Date(Math.round(date.getTime() / coeff) * coeff);
    }

    initRepetiveTimeView() {
        if (this.scheduleData && this.scheduleData.isRepetitive) {
            $('#group_repetitiveTime').show();
            $('#group_singleTime').hide();
            $('#group_repetitiveTime #repetitiveTime').attr("name", "timeString");
            $('#group_singleTime #singleTimeStart').attr("name", "");
            $('#group_repetitiveTime #selectedDuration').attr("name", "duration");
            $('#group_singleTime #calculatedDuration').attr("name", "");
        } else {
            $('#group_repetitiveTime').hide();
            $('#group_singleTime').show();
            $('#group_repetitiveTime #repetitiveTime').attr("name", "");
            $('#group_singleTime #singleTimeStart').attr("name", "timeString");
            $('#group_singleTime #calculatedDuration').attr("name", "duration");
            $('#group_repetitiveTime #selectedDuration').attr("name", "");
            this.loadDatePicker();
        }
    }

    onChangeRepitition(event) {
        let timeString = '';
        if (this.scheduleData && this.scheduleData.isRepetitive) {
            timeString = this.scheduleData.timeString;
        }
        if (event.target.checked) {
            $('#group_repetitiveTime').show();
            $('#group_singleTime').hide();
            $('#group_repetitiveTime #repetitiveTime').val(timeString);
            $('#group_repetitiveTime #repetitiveTime').attr("name", "timeString");
            $('#group_singleTime #singleTimeStart').attr("name", "");
            $('#group_repetitiveTime #selectedDuration').attr("name", "duration");
            $('#group_singleTime #calculatedDuration').attr("name", "");
        } else {
            $('#group_repetitiveTime').hide();
            $('#group_singleTime').show();
            $('#group_repetitiveTime #repetitiveTime').attr("name", "");
            $('#group_singleTime #singleTimeStart').attr("name", "timeString");
            $('#group_singleTime #calculatedDuration').attr("name", "duration");
            $('#group_repetitiveTime #selectedDuration').attr("name", "");
            this.loadDatePicker();
        }
    }

    fetchStatsList()
    {
        const url = `${this.includeUrl}/accessstats/`;
        fetch(url)
            .then((response) => response.json())
            .then((data) => {
                this.processStatsList(data);
            });
    }

    processStatsList(data)
    {
        const occurrences = data.reduce(function (acc, item) {
            return acc[item.role] ? ++acc[item.role] : acc[item.role] = 1, acc
        }, {});


        $('#activeCitizen').text(occurrences['citizen']|0);
        $('#activeUsers').text(occurrences['user']|0);
    }

    setReloadTimer() {
        clearTimeout(this.reloadTimer);
        this.reloadTimer = setTimeout(() => {
            this.fetchStatsList();
            this.setReloadTimer();
        }, this.reloadInterval);
    }

    loadDatePicker() {
        this.setDates();
        const calendarElementStart = createRoot(document.getElementById('wrapper_singleTimeStart'));
        const calendarElementEnd = createRoot(document.getElementById('wrapper_singleTimeEnd'));

        const onChangeDateStart = (value) => {
            let startDate = moment(value, 'YYYY-MM-DD HH:mm');
            let endDate = moment($('#singleTimeEnd').val(), 'YYYY-MM-DD HH:mm');
            $('#calculatedDuration').val(endDate.diff(startDate, 'minutes'))
        }
        const onChangeDateEnd = (value) => {
            let startDate = moment($('#singleTimeStart').val(), 'YYYY-MM-DD HH:mm');
            let endDate = moment(value, 'YYYY-MM-DD HH:mm');
            $('#calculatedDuration').val(endDate.diff(startDate, 'minutes'))
        }

        return (
            calendarElementStart.render(
                <Datepicker
                    id="singleTimeStart"
                    name="timeString"
                    value={this.startDate/1000}
                    attributes={{ "aria-label": "Datum" }}
                    onChange={onChangeDateStart}
                    minDate={this.currentDate}
                    //maxDate={addDays(currentDateTime, 1)}
                    minTime={this.minTime}
                    maxTime={this.maxTime}
                    showTimeSelect
                    timeFormat="HH:mm"
                    dateFormat="yyyy-MM-dd HH:mm"
                    onChangeDateFormat="YYYY-MM-DD HH:mm"
                    timeCaption="Uhrzeit"
                    timeIntervals={10}
                />
            ),
            calendarElementEnd.render(
                <Datepicker
                    id="singleTimeEnd"
                    value={this.endDate/1000}
                    attributes={{ "aria-label": "Datum" }}
                    onChange={onChangeDateEnd}
                    minDate={this.currentDate}
                    //maxDate={addDays(currentDateTime, 1)}
                    minTime={this.minTime}
                    maxTime={this.maxTime}
                    showTimeSelect
                    timeFormat="HH:mm"
                    dateFormat="yyyy-MM-dd HH:mm"
                    onChangeDateFormat="YYYY-MM-DD HH:mm"
                    timeCaption="Uhrzeit"
                    timeIntervals={10}
                />
            )
        );
    }
}

export default View;
