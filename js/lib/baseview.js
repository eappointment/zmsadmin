import $ from "jquery";
import ErrorHandler from './errorHandler';
import ExceptionHandler from './exceptionHandler';
import MessageHandler from './messageHandler';
import DialogHandler from './dialogHandler';
import { lightbox, showSpinner, hideSpinner, noOp } from './utils';

class BaseView extends ErrorHandler {

    constructor(element, options = {}) {
        super();
        this.$main = $(element);
        this.$main.off();
        this.options = options;
        this.loadPromise = Promise.reject(null).catch(noOp);
    }

    // needs development
    translate(contentStr) {
        return contentStr;
    }

    loadContent(url, method = 'GET', data = null, container = null, spinner = true) {
        if (container !== null) {
            this.$main = container;
        }

        if (spinner) {
            showSpinner(this.$main);
        }

        const ajaxSettings = {
            method
        };

        if (method === 'POST' || method === 'PUT') {
            ajaxSettings.data = data;
        }

        return new Promise((resolve, reject) => {
            $.ajax(url, ajaxSettings).done(responseData => {
                this.$main.html(responseData);
                DialogHandler.hideMessages();
                resolve(responseData);
            }).fail((err) => {
                let isException = false;
                if (err && err.responseText) {
                    isException = err.responseText.toLowerCase().includes('exception');
                }
                let isMaintenance = false;
                if (err && err.statusText) {
                    isMaintenance = err.statusText.toLowerCase().includes('maintenance');
                }
                if (err.status == 503 && isMaintenance) {
                    window.location.href = `${this.includeUrl}/workstation/select/`;
                }
                if (err.status >= 400 && isException) {
                    new ExceptionHandler(this.$main, {
                        code: err.status,
                        message: err.responseText,
                        parent: this
                    });
                    if (this['errorCallback']) {
                        let callback = this['errorCallback'];
                        callback(err);
                    }
                } else {
                    console.log('XHR load error', url, err);
                    reject(err);
                }
            }).always(() => {
                if (spinner) {
                    hideSpinner(this.$main);
                }
            })
        });
    }

    loadCall(url, method = 'GET', data = null, spinner = false, $container = this.$main) {
        return BaseView.loadCallStatic(url, method, data, spinner, $container);
    }

    static loadCallStatic(url, method = 'GET', data = null, spinner = false, parent) {
        if (parent !== null) {
            this.$main = parent;
        }
        if (spinner) {
            showSpinner(this.$main);
        }
        const ajaxSettings = {
            method
        };
        if (method === 'POST' || method === 'PUT') {
            ajaxSettings.data = data;
        }
        return new Promise((resolve, reject) => {
            $.ajax(url, ajaxSettings).done(responseData => {
                resolve(responseData);
            }).fail(err => {
                let isException = err.responseText.toLowerCase().includes('exception');
                if (err.status >= 400 && isException) {
                    hideSpinner(this.$main);
                    new ExceptionHandler(this.$main, {
                        code: err.status,
                        message: err.responseText,
                        parent: this.$main,
                    });
                } else {
                    console.log('XHR load error', url, err);
                    reject(err);
                }
            }).always(() => {
                hideSpinner(this.$main);
            })
        });
    }

    destroy() {
        this.$main.off().empty();
        this.$main = null;
    }

    get $() {
        return this.$main;
    }

    cleanReload() {
        window.setTimeout(() => {
            console.log("Clean reload %o", window.location.href);
            window.location.assign(window.location.href)
        }, 400);
    }

    locationLoad(url) {
        window.location.href = url;
    }

    loadMessage(response, callback, $container = null, returnTarget = null) {
        return new Promise((resolve, reject) => {
            if (!$container) {
                $container = this.$main;
            }

            const { lightboxContentElement, destroyLightbox } = lightbox($container, () => {
                destroyLightbox();
                if (returnTarget) {
                    returnTarget.focus();
                }
                callback();
                resolve(); // Promise auflösen, nachdem der Callback ausgeführt wurde
            });

            MessageHandler.init(lightboxContentElement, {
                message: response,
                callback: () => {
                    callback();
                    destroyLightbox();
                    if (returnTarget) {
                        returnTarget.focus();
                    }
                    resolve(); // Promise hier ebenfalls auflösen, nachdem der Callback ausgeführt wurde
                },
                parent: this,
                handleLightbox: destroyLightbox
            }).then(messageHandler => {
                resolve(messageHandler);
            }).catch(error => {
                console.error("MessageHandler konnte nicht initialisiert werden:", error);
                reject(error);
            });

        });
    }

    loadDialog(response, callback, abortCallback, returnTarget) {
        return BaseView.loadDialogStatic(response, callback, abortCallback, this, false, returnTarget);
    }

    static loadDialogStatic(response, callback, abortCallback, parent, callbackAsBackgroundAction = false, returnTarget = false) {
        return new Promise((resolve, reject) => {
            let $container = null;
            let $loader = null;
            if (parent) {
                $container = parent.$main;
                $loader = parent.loadCall;
            }

            const { lightboxContentElement, destroyLightbox } = lightbox($container, () => {
                destroyLightbox();
                if (callbackAsBackgroundAction) {
                    callback();
                } else if (abortCallback) {
                    abortCallback();
                }
            });

            DialogHandler.init(lightboxContentElement, {
                response: response,
                callback: () => {
                    callback();
                    destroyLightbox();
                    if (returnTarget) {
                        returnTarget.focus();
                    }
                },
                abortCallback: () => {
                    if (abortCallback) {
                        abortCallback();
                    }
                    destroyLightbox();
                    if (returnTarget) {
                        returnTarget.focus();
                    }
                },
                parent: parent,
                returnTarget: returnTarget,
                loader: $loader
            })
                .then(dialogHandler => {
                    resolve(dialogHandler);
                })
                .catch(error => {
                    console.error("DialogHandler konnte nicht initialisiert werden:", error);
                    reject(error);
                });
        });
    }

    loadErrorCallback(err) {
        if (typeof err.statusText !== "undefined" && err.statusText.toLowerCase().includes('maintenance')) {
            window.location.href = `${this.includeUrl}/workstation/select/`;
        }
        if (typeof err.message !== "undefined" && err.message.toLowerCase().includes('exception')) {
            let exceptionType = $(err.message).filter('.exception').data('exception');
            this.load();
            console.log('EXCEPTION thrown: ' + exceptionType);
        }
        else
            console.log('Ajax error', err);
    }
}

export default BaseView;
