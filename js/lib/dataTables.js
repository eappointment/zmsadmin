import DataTable from 'datatables.net-dt';

const dataTables = (selector, options) => {
    return new DataTable(selector, options);
}
export default dataTables