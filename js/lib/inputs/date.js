/* eslint-disable react/prop-types */
import React from 'react'
import moment from 'moment'
import DatePicker, { registerLocale } from 'react-datepicker'
import de from 'date-fns/locale/de'
import setHours from "date-fns/setHours";
import setMinutes from "date-fns/setMinutes";
import * as Inputs from "./index";
registerLocale('de', de)

const isToday = (dateToCheck) => {
    const today = new Date();
    return (
        dateToCheck.getDate() === today.getDate() &&
        dateToCheck.getMonth() === today.getMonth() &&
        dateToCheck.getFullYear() === today.getFullYear()
    );
};

const isPast = (dateToCheck) => {
    const today = new Date();
    return (dateToCheck < today);
};
class Datepicker extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            startDate: moment.unix(props.value).toDate(),
            datePickerIsOpen: false,
            minTime: props.minTime,
            maxTime: props.maxTime
        };
        if (props.minTime && props.maxTime) {
            this.setState({
                minTime: setHours(setMinutes(this.state.startDate, this.state.startDate.getMinutes()), this.state.startDate.getHours()),
                maxTime: setHours(setMinutes(this.state.startDate, 59), 23)
            });
        }
        //format of date to use in urls, not for displaying
        this.onChangeDateFormat = props.onChangeDateFormat || "YYYY-MM-DD"
        this.handleChange = this.handleChange.bind(this);
        this.openDatePicker = this.openDatePicker.bind(this);
        this.closeDatePicker = this.closeDatePicker.bind(this);
        this.escHandler = this.escHandler.bind(this);
        this.keyDownHandler = this.keyDownHandler.bind(this);
        this.handleIcon = this.handleIcon.bind(this);
    }

    componentDidMount(){
        if (this.props.accessKey)
            this.datepicker.input.accessKey = this.props.accessKey;
        if (this.props.ariaLive)
            this.datepicker.input.ariaLive = "polite";

        document.addEventListener("keydown", this.escHandler, false);
      }

    escHandler(event) {
        if (event.key === "Escape") {
            this.closeDatePicker();
        }
    }

    keyDownHandler(event) {
        if (event.key === 'Enter') {
            event.preventDefault()
            if (this.state.datePickerIsOpen) {
                this.closeDatePicker() 
            } else {
                this.openDatePicker()
            }
        }
    }

    handleDateChangeRaw(e) {
        e.preventDefault();
    }

    handleIcon(event) {
        event.preventDefault();
        this.openDatePicker()
    }

    handleChange(date) {
        this.setState({
            startDate: date,
        });
        if (this.state.minTime && this.state.maxTime) {
            this.setState({
                minTime: setHours(setMinutes(date, new Date().getMinutes()), new Date().getHours()),
                maxTime: setHours(setMinutes(date, 59), 23)
            });

            if (! isToday(date)) {
                this.setState({
                    minTime: setHours(date, 0),
                    maxTime: setHours(date, 23)
                });
            }
        }

        this.props.onChange(moment(date, 'X').format(this.onChangeDateFormat))
        if (! this.props.showTimeSelect)
            this.closeDatePicker();
    }
    
    openDatePicker() {
        this.setState({
            datePickerIsOpen: true,
        });
        this.datepicker.input.focus();
    }

    closeDatePicker() {
        this.setState({
            datePickerIsOpen: false,
        });
    }

    render () {
        return (
            <div className="add-date-picker" {...this.props.attributes}>
                <DatePicker
                    todayButton="Heute"
                    locale="de"
                    id={this.props.id}
                    //disabled={this.state.disabled}
                    name={this.props.name}
                    minDate={this.props.minDate || null}
                    //maxDate={this.props.maxDate || null}
                    minTime={this.state.minTime || null}
                    maxTime={this.state.maxTime || null}
                    startDate={this.state.startDate}
                    endDate={this.state.endDate || null}
                    showTimeSelect={this.props.showTimeSelect || false}
                    timeFormat={this.props.timeFormat || null}
                    timeIntervals={this.props.timeIntervals || null}
                    timeCaption={this.props.timeCaption || "Uhrzeit"}
                    selected={this.state.startDate}
                    //selectsRange={this.props.selectsRange || false}
                    //withPortal={this.props.withPortal || false}
                    className="form-control form-input"
                    dateFormat={this.props.dateFormat || "dd.MM.yyyy"}
                    onChange={this.handleChange} {...this.props.name }
                    onChangeRaw={this.handleDateChangeRaw}
                    onInputClick={this.openDatePicker}
                    onKeyDown={this.keyDownHandler}
                    onClickOutside={this.closeDatePicker}
                    strictParsing={true}
                    ref={(datepicker) => { this.datepicker = datepicker }}
                    chooseDayAriaLabelPrefix="Datumsauswahl"
                    disabledDayAriaLabelPrefix="Nicht auswählbar"
                    previousMonthAriaLabel="vorheriger Monat"
                    nextMonthAriaLabel="nächster Monat"
                    monthAriaLabelPrefix="Monat"
                    ariaDescribedBy={"help_" + this.props.id}
                />
                <a href="#" className="calendar-placement icon" title="Kalender öffnen" aria-label="Kalender öffnen" onClick={this.handleIcon} onKeyDown={this.keyDownHandler}>
                    <i className="far fa-calendar-alt" aria-hidden="true" />
                </a>
            </div>
        )
    }
}

export default Datepicker;