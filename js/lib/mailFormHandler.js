import {showSpinner} from "./utils";

class MailFormHandler {
    constructor(options) {
        this.actionurl = options.actionurl;
        this.container = options.container;
    }

    handleForm() {
        return new Promise((resolve, reject) => {
            this.bindCheckboxListeners();
            let $form = this.container.find('.dialog form');
            $form.off('submit').on('submit', (event) => {
                event.preventDefault();

                showSpinner(this.container);
                const formData = $form.serializeArray();

                $.ajax({
                    url: this.actionurl,
                    method: 'POST',
                    data: $.param(formData),
                    success: (response) => {
                        resolve(response); // Bei Erfolg
                    },
                    error: (error) => {
                        reject(error); // Bei Fehler
                    }
                });
            });
        });
    }

    showFormWithErrors(responseText) {
        var content = $(responseText).find('.dialog');
        this.container.html(content);
        this.bindCheckboxListeners();
    }

    bindCheckboxListeners() {
        let $form = this.container.find('.dialog form');
        const $submitButton = $form.find('#button-submit');

        if (this.actionurl.includes('/mail/cancel/all/')) {
            const $availabilityAcceptedCheckbox = $form.find('#availabilityAcceptedCheckbox');
            const $cancelAcceptedCheckbox = $form.find('#cancelAcceptedCheckbox');
            const toggleSubmitButton = () => {
                const bothChecked = $availabilityAcceptedCheckbox.is(':checked') && $cancelAcceptedCheckbox.is(':checked');
                $submitButton.prop('disabled', !bothChecked);
            };
            $availabilityAcceptedCheckbox.on('change', toggleSubmitButton);
            $cancelAcceptedCheckbox.on('change', toggleSubmitButton);
            toggleSubmitButton();
        } else if (this.actionurl.includes('/mail/')) {
            const $adminAcceptedCheckbox = $form.find('#adminAcceptedCheckbox');
            const toggleSubmitButton = () => {
                const isChecked = $adminAcceptedCheckbox.is(':checked');
                $submitButton.prop('disabled', !isChecked);
            };
            $adminAcceptedCheckbox.on('change', toggleSubmitButton);
            toggleSubmitButton();
        }
    }
}

export default MailFormHandler