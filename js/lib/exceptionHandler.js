import $ from 'jquery'
import Baseview from './baseview';
import { hideSpinner, noOp } from './utils';

class ExceptionHandler {

    constructor(element, options) {
        this.$main = $(element)
        this.message = options.message;
        this.parent = options.parent || this.$main;
        this.code = options.code;
        this.callback = options.callback || this.closeException;
        this.render();
    }

    render() {
        var $message = $(this.message).filter('.exception');
        if ($message.length == 0) {
            $message = $(this.message).find('.exception');
        }
        if ($message.length == 0) {
            return;
        }
        this.$messageElement = $($message.get(0).outerHTML);

        if ($(this.$main.get(0)).hasClass('lightbox__content')) {
            this.$main.html($message.get(0).outerHTML);
        } else {
            this.$main.find('.body').first().prepend(this.$messageElement);
        }


        this.handleMissingButton()
        hideSpinner();
    }

    handleMissingButton()
    {
        var $buttons = this.$messageElement.find('.btn');
        if ($buttons.length == 0) {
            $buttons = $('<div class="form-actions"><button class="button button-cancel close right" title="Schließen"><span class="aural">Schließen</span>Ok</button></div>');
            this.$messageElement.append($buttons);
        }

        $buttons.on('click', (ev) => {
            ev.preventDefault();
            ev.stopPropagation();
            let callback = $(ev.target).data('callback');
            let location = $(ev.target).attr('href');
            if (callback && this.parent[callback]) {
                this.callback = this.parent[callback];
            } else if (!callback && location && location !== '#') {
                document.location.href = location;
            }
            this.callback(ev);
        })
    }

    closeException() {
        if (this.$messageElement) {
            this.$messageElement.remove();
        }
    }

}

export default ExceptionHandler
