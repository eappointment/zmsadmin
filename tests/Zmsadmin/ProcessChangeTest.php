<?php

namespace BO\Zmsadmin\Tests;

class ProcessChangeTest extends Base
{
    protected $arguments = [];

    protected $parameters = [
        'amendment' => '',
        'selectedprocess' => '82252',
        'selectedtime' => '17-00',
        'selecteddate' => '2016-05-27',
        'slotCount' => 1,
        'familyName' => 'Test BO',
        'email' => 'test@example.com',
        'telephone' => '1234567890',
        'scope' => 141,
        'requests' => [120703]
    ];

    protected $classname = "ProcessChange";

    public function testRendering()
    {
        $this->setApiCalls(
            [
                [
                    'function' => 'readGetResult',
                    'url' => '/workstation/',
                    'parameters' => ['resolveReferences' => 2],
                    'response' => $this->readFixture("GET_Workstation_Resolved2.json")
                ],
                [
                    'function' => 'readGetResult',
                    'url' => '/process/82252/',
                    'response' => $this->readFixture("GET_process_82252_12a2.json")
                ],
                [
                    'function' => 'readGetResult',
                    'url' => '/scope/141/',
                    'parameters' => [
                        'resolveReferences' => 1,
                        'gql' => \BO\Zmsadmin\Helper\GraphDefaults::getScope()
                    ],
                    'response' => $this->readFixture("GET_scope_141.json")
                ],
                [
                    'function' => 'readPostResult',
                    'url' => '/process/82252/12a2/',
                    'response' => $this->readFixture("GET_process_82252_12a2.json")
                ],
                [
                    'function' => 'readPostResult',
                    'url' => '/process/82252/12a2/appointment/',
                    'parameters' => [
                        'resolveReferences' => 1,
                        'slotType' => 'intern',
                        'clientkey' => '',
                        'slotsRequired' => 0
                    ],
                    'response' => $this->readFixture("GET_process_82252_12a2.json")
                ],
                [
                    'function' => 'readPostResult',
                    'url' => '/process/82252/12a2/delete/mail/',
                    'response' => $this->readFixture("POST_mail.json")
                ],
                [
                    'function' => 'readPostResult',
                    'url' => '/process/82252/12a2/delete/notification/',
                    'response' => $this->readFixture("POST_notification.json")
                ]
            ]
        );
        $response = $this->render($this->arguments, $this->parameters, [], 'POST');
        $this->assertStringContainsString(
            'Der Vorgang mit der Nummer 82252 wurde erfolgreich geändert.',
            (string)$response->getBody()
        );
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testValidationFailed()
    {
        $this->setApiCalls(
            [
                [
                    'function' => 'readGetResult',
                    'url' => '/workstation/',
                    'parameters' => ['resolveReferences' => 2],
                    'response' => $this->readFixture("GET_Workstation_Resolved2.json")
                ],
                [
                    'function' => 'readGetResult',
                    'url' => '/process/82252/',
                    'response' => $this->readFixture("GET_process_82252_12a2.json")
                ],
                [
                    'function' => 'readGetResult',
                    'url' => '/scope/143/',
                    'parameters' => [
                        'resolveReferences' => 1,
                        'gql' => \BO\Zmsadmin\Helper\GraphDefaults::getScope()
                    ],
                    'response' => $this->readFixture("GET_scope_141.json")
                ]
            ]
        );
        $response = $this->render($this->arguments, [
            'selectedprocess' => '82252',
            'selectedtime' => '17-00',
            'selecteddate' => '2016-05-27',
            'slotCount' => 1,
            'familyName' => '',
            'telephone' => '1234567890',
            'scope' => 143,
            'requests' => [120703]
        ], [], 'POST');
        $this->assertStringContainsString(
            'Name eingegeben werden',
            (string)$response->getBody()
        );
        $this->assertEquals(200, $response->getStatusCode());
    }
}
