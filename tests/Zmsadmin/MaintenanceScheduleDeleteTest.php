<?php

declare(strict_types=1);

namespace BO\Zmsadmin\Tests;

use Fig\Http\Message\StatusCodeInterface;

class MaintenanceScheduleDeleteTest extends Base
{
    protected $classname = "MaintenanceScheduleDelete";

    public function testRendering()
    {
        $this->setApiCalls(
            [
                [
                    'function' => 'readDeleteResult',
                    'url' => '/maintenanceschedule/999999999/',
                    'response' => $this->readFixture("GET_data_empty.json")
                ],
            ]
        );

        $response = $this->render(['id' => 999999999], $this->parameters, [], 'POST');

        $this->assertRedirect($response, '/maintenance/?success=maintenance_schedule_delete');
    }
}
