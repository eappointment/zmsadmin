<?php

/**
 *
 * @copyright BerlinOnline GmbH
 *
 */
namespace BO\Zmsadmin\Tests;

use BO\Slim\Response;
use BO\Zmsadmin\Helper\Config;
use BO\Zmsadmin\Helper\GraphDefaults;

abstract class Base extends \BO\Zmsclient\PhpUnit\Base
{
    protected $namespace = '\\BO\\Zmsadmin\\';

    public function readFixture($filename)
    {
        $path = dirname(__FILE__) . '/fixtures/' . $filename;
        if (! is_readable($path) || ! is_file($path)) {
            throw new \Exception("Fixture $path is not readable");
        }
        return file_get_contents($path);
    }

    protected function getApiCalls()
    {
        return array_merge($this->getOptionalApiCalls(), parent::getApiCalls());
    }

    protected function getOptionalApiCalls(): array
    {
        $exception = new \BO\Zmsclient\Exception('API-Call failed', new Response(404));
        $exception->template = 'bo/zmsclient/exception/apifailed';

        return [
            'GET_maintenanceschedule_active' => [
                'function'   => 'readGetResult',
                'url'        => '/maintenanceschedule/active/',
                'exception'  => $exception,
                'optional'   => true,
            ],
            'GET_config' => [
                'function' => 'readGetResult',
                'url' => '/config/',
                'parameters' => [
                    'gql' => GraphDefaults::getConfig(),
                    'withDescriptions' => false
                ],
                'xtoken' => \App::CONFIG_SECURE_TOKEN,
                'response' => $this->readFixture("GET_config.json"),
                'optional'   => true,
            ],
        ];
    }

    public function setUp(): void
    {
        parent::setUp();
        \App::$slim->getContainer()->set('zmsadmin.services.config', new Config());
        \App::getConfigService()->fetchEntity();
        \App::$now = new \DateTimeImmutable('2016-04-01 11:55:10', new \DateTimeZone('Europe/Berlin'));
    }

    public function tearDown(): void
    {
        if (\App::$slim->getContainer()->has('zmsadmin.services.config')) {
            \App::$slim->getContainer()->offsetUnset('zmsadmin.services.config');
        }
        \App::$now = new \DateTimeImmutable('2016-04-01 11:55:00', new \DateTimeZone('Europe/Berlin'));
    }
}
