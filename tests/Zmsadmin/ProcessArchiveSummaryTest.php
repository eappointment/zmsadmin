<?php

namespace BO\Zmsadmin\Tests;

class ProcessArchiveSummaryTest extends Base
{
    protected $arguments = [
        'processId' => 82252,
        'id' => 'b83ae8059b0644db905d3cbf420a68e2',
    ];

    protected $parameters = [];

    protected $classname = "ProcessArchiveSummary";

    public function testRendering()
    {
        $this->setApiCalls(
            [
                [
                    'function' => 'readGetResult',
                    'url' => '/workstation/',
                    'parameters' => ['resolveReferences' => 1],
                    'response' => $this->readFixture("GET_Workstation_Resolved1.json")
                ],
                [
                    'function' => 'readGetResult',
                    'url' => '/process/archive/'. $this->arguments['id'] . '/',
                    'parameters' => ['resolveReferences' => 1],
                    'response' => $this->readFixture("GET_process_82252_12a2.json")
                ]
            ]
        );
        $response = $this->render($this->arguments, $this->parameters, []);
        $this->assertStringContainsString('Vorgangsdaten', (string)$response->getBody());
        $this->assertStringContainsString('Bürgerdaten', (string)$response->getBody());
        $this->assertStringContainsString('b83ae8059b0644db905d3cbf420a68e2', (string)$response->getBody());
        $this->assertStringContainsString('Bürgeramt Heerstraße', (string)$response->getBody());
        $this->assertStringContainsString('Diesen Bürger bearbeiten', (string)$response->getBody());
        $this->assertStringContainsString('Diesen Bürger aufrufen', (string)$response->getBody());
        $this->assertStringContainsString('/workstation/?selectedprocess=82252', (string)$response->getBody());
        $this->assertStringContainsString('/workstation/?calledprocess=82252', (string)$response->getBody());

        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testButtonsInFuture()
    {
        \App::$now = new \DateTimeImmutable('2016-04-02 11:55:00', new \DateTimeZone('Europe/Berlin'));
        $this->setApiCalls(
            [
                [
                    'function' => 'readGetResult',
                    'url' => '/workstation/',
                    'parameters' => ['resolveReferences' => 1],
                    'response' => $this->readFixture("GET_Workstation_Resolved1.json")
                ],
                [
                    'function' => 'readGetResult',
                    'url' => '/process/archive/'. $this->arguments['id'] . '/',
                    'parameters' => ['resolveReferences' => 1],
                    'response' => $this->readFixture("GET_process_82252_12a2.json")
                ]
            ]
        );
        $response = $this->render($this->arguments, $this->parameters, []);
        $this->assertStringContainsString('Vorgangsdaten', (string)$response->getBody());
        $this->assertStringContainsString('Bürgerdaten', (string)$response->getBody());
        $this->assertStringContainsString('b83ae8059b0644db905d3cbf420a68e2', (string)$response->getBody());
        $this->assertStringContainsString('Bürgeramt Heerstraße', (string)$response->getBody());
        $this->assertStringNotContainsString('Diesen Bürger bearbeiten', (string)$response->getBody());
        $this->assertStringNotContainsString('Diesen Bürger aufrufen', (string)$response->getBody());
        $this->assertStringNotContainsString('/workstation/?selectedprocess=82252', (string)$response->getBody());
        $this->assertStringNotContainsString('/workstation/?calledprocess=82252', (string)$response->getBody());

        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testEditButton()
    {
        \App::$now = new \DateTimeImmutable('2016-03-31 11:55:00', new \DateTimeZone('Europe/Berlin'));
        $this->setApiCalls(
            [
                [
                    'function' => 'readGetResult',
                    'url' => '/workstation/',
                    'parameters' => ['resolveReferences' => 1],
                    'response' => $this->readFixture("GET_Workstation_Resolved1.json")
                ],
                [
                    'function' => 'readGetResult',
                    'url' => '/process/archive/'. $this->arguments['id'] . '/',
                    'parameters' => ['resolveReferences' => 1],
                    'response' => $this->readFixture("GET_process_82252_12a2.json")
                ]
            ]
        );
        $response = $this->render($this->arguments, $this->parameters, []);
        $this->assertStringContainsString('Vorgangsdaten', (string)$response->getBody());
        $this->assertStringContainsString('Bürgerdaten', (string)$response->getBody());
        $this->assertStringContainsString('b83ae8059b0644db905d3cbf420a68e2', (string)$response->getBody());
        $this->assertStringContainsString('Bürgeramt Heerstraße', (string)$response->getBody());
        $this->assertStringContainsString('Diesen Bürger bearbeiten', (string)$response->getBody());
        $this->assertStringNotContainsString('Diesen Bürger aufrufen', (string)$response->getBody());
        $this->assertStringContainsString('/workstation/?selectedprocess=82252', (string)$response->getBody());
        $this->assertStringNotContainsString('/workstation/?calledprocess=82252', (string)$response->getBody());

        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testNoAccess()
    {
        \App::$now = new \DateTimeImmutable('2016-04-01 07:00:00', new \DateTimeZone('Europe/Berlin'));
        $this->setApiCalls(
            [
                [
                    'function' => 'readGetResult',
                    'url' => '/workstation/',
                    'parameters' => ['resolveReferences' => 1],
                    'response' => $this->readFixture("GET_Workstation_101_Resolved1.json")
                ],
                [
                    'function' => 'readGetResult',
                    'url' => '/process/archive/'. $this->arguments['id'] . '/',
                    'parameters' => ['resolveReferences' => 1],
                    'response' => $this->readFixture("GET_process_82252_12a2.json")
                ]
            ]
        );
        $response = $this->render($this->arguments, $this->parameters, []);
        $this->assertStringContainsString('Vorgangsdaten', (string)$response->getBody());
        $this->assertStringNotContainsString('Bürgerdaten', (string)$response->getBody());
        $this->assertStringContainsString('b83ae8059b0644db905d3cbf420a68e2', (string)$response->getBody());
        $this->assertStringContainsString('Bürgeramt Heerstraße', (string)$response->getBody());
        $this->assertStringNotContainsString('Diesen Bürger bearbeiten', (string)$response->getBody());
        $this->assertStringNotContainsString('/workstation/?selectedprocess=82252', (string)$response->getBody());

        $this->assertEquals(200, $response->getStatusCode());
    }
}
