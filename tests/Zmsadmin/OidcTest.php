<?php

namespace BO\Zmsadmin\Tests;

use BO\Zmsclient\Auth;
use BO\Zmsclient\Psr7\Request;

use BO\Zmsadmin\Middleware\OAuth\OAuthService;

use Mockery as MockeryService;

class OidcTest extends Base
{
    protected $provider;

    protected $providerService;

    protected $providerName = 'keycloak';

    protected $arguments = [];

    protected $parameters = ['state' => 'c273715018c869f40eb747aa33bad4ed'];

    protected $classname = "Oidc";

    public function setUp(): void
    {
        $this->providerService = $this->getProviderService($this->providerName);
        $this->provider = $this->getProviderService($this->providerName)->getProvider();
    }

    public function tearDown(): void
    {
        MockeryService::close();
        parent::tearDown();
    }

    /*
        test successful login for existing useraccount and redirect to selection of scopes
    */
    public function testRendering()
    {
        Auth::setKey($this->parameters['state']);
        $response = $this->render($this->arguments, $this->parameters, []);
        $this->assertRedirect($response, '/workstation/select/');
    }

    public function testWithAuthorization()
    {
        Auth::removeKey();
        $response = $this->render($this->arguments, [
            'oauthProvider' => 'keycloak',
            '__middleware' => [
                'class' => 'BO\Zmsadmin\Middleware\OAuth\OAuthLoginService'
            ]
        ], []);
        $this->assertStringContainsString('/workstation/select/', $response->getHeaderLine('location'));
    }

    

    public function testGetAuthorizationUrl()
    {
        $url = $this->provider->getAuthorizationUrl();
        $uri = parse_url($url);
        $this->assertStringContainsString('/realms/zms/protocol/openid-connect/auth', $uri['path']);

        $this->providerName = 'gitlab';
        $this->setUp();
        $url = $this->provider->getAuthorizationUrl();
        $uri = parse_url($url);
        $this->assertStringContainsString('/oauth/authorize', $uri['path']);
    }

    public function testGetLogoutUrl()
    {
        $url = $this->provider->getLogoutUrl();
        $uri = parse_url($url);
        $this->assertStringContainsString('/realms/zms/protocol/openid-connect/logout', $uri['path']);

        $this->providerName = 'gitlab';
        $this->setUp();
        $url = $this->provider->getBaseLogoutUrl();
        $uri = parse_url($url);
        $this->assertStringContainsString('/oauth/revocation', $uri['path']);
    }

    public function testGetBaseAccessTokenUrl()
    {
        $params = [];

        $url = $this->provider->getBaseAccessTokenUrl($params);
        $uri = parse_url($url);
        $this->assertStringContainsString('/realms/zms/protocol/openid-connect/token', $uri['path']);

        $this->providerName = 'gitlab';
        $this->setUp();
        $url = $this->provider->getBaseAccessTokenUrl($params);
        $uri = parse_url($url);
        $this->assertStringContainsString('/oauth/token', $uri['path']);
    }

    public function testGetAccessToken()
    {
        $response = MockeryService::mock('Psr\Http\Message\ResponseInterface');
        $response->shouldReceive('getBody')
            ->andReturn('{"access_token":"mock_access_token", "scope":"email", "token_type":"bearer"}');
        $response->shouldReceive('getHeader')
            ->andReturn(['content-type' => 'json']);

        $client = MockeryService::mock('BO\Zmsclient\Psr7\ClientInterface');
        $client->shouldReceive('readResponse')
            ->times(1)
            ->andReturn($response);
        $this->provider->setHttpClient($client);
        $this->providerService->setProvider($this->provider);

        $this->providerService->setAuthcode(new Request('GET', ''), 'mock_authorization_code');
        $token = $this->providerService->getAccessToken();

        $this->assertEquals('mock_access_token', $token->getToken());
        $this->assertNull($token->getExpires());
        $this->assertNull($token->getRefreshToken());
        $this->assertNull($token->getResourceOwnerId());
    }

    public function testGetAccessTokenGitlab()
    {
        $response = MockeryService::mock('Psr\Http\Message\ResponseInterface');
        $response->shouldReceive('getBody')
            ->andReturn('{"access_token":"mock_access_token", "scope":"email", "token_type":"bearer"}');
        $response->shouldReceive('getHeader')
            ->andReturn(['content-type' => 'json']);

        $client = MockeryService::mock('BO\Zmsclient\Psr7\ClientInterface');
        $client->shouldReceive('readResponse')
            ->times(1)
            ->andReturn($response);
        
        $this->providerName = 'gitlab';
        $this->setUp();
        $this->provider->setHttpClient($client);
        $this->providerService->setProvider($this->provider);

        $this->providerService->setAuthcode(new Request('GET', ''), 'mock_authorization_code');
        $token = $this->providerService->getAccessToken();

        $this->assertEquals('mock_access_token', $token->getToken());
        $this->assertNull($token->getExpires());
        $this->assertNull($token->getRefreshToken());
        $this->assertNull($token->getResourceOwnerId());
    }

    public function testGetAccessTokenFailed()
    {
        $this->expectException('InvalidArgumentException');
        $this->expectExceptionMessage('Required option not passed: "access_token"');

        $response = MockeryService::mock('Psr\Http\Message\ResponseInterface');
        $response->shouldReceive('getBody')
            ->andReturn('{"scope":"email", "token_type":"bearer"}');
        $response->shouldReceive('getHeader')
            ->andReturn(['content-type' => 'json']);

        $client = MockeryService::mock('BO\Zmsclient\Psr7\ClientInterface');
        $client->shouldReceive('readResponse')
            ->times(1)
            ->andReturn($response);
        $this->provider->setHttpClient($client);
        $this->providerService->setProvider($this->provider);

        $this->providerService->setAuthcode(new Request('GET', ''), 'mock_authorization_code');
        $this->providerService->getAccessToken();
    }

    public function testGetRefreshToken()
    {
        $options = [
            "expires"=>1459490400,
            "access_token"=>"mock_access_token",
            "scope"=>"email,profile,openid",
            "token_type"=>"bearer",
            "refresh_token"=>"mock_refresh_access_token"
        ];
        $response = MockeryService::mock('Psr\Http\Message\ResponseInterface');
        $response->shouldReceive('getBody')
            ->andReturn(json_encode($options));
        $response->shouldReceive('getHeader')
            ->andReturn(['content-type' => 'json']);
        $client = MockeryService::mock('BO\Zmsclient\Psr7\ClientInterface');
        $client->shouldReceive('readResponse')
            ->times(1)
            ->andReturn($response);
        $this->provider->setHttpClient($client);

        $this->providerService->setProvider($this->provider);
        $refreshedToken = $this->providerService->getRefreshedAccessToken($options);

        $this->assertEquals('mock_access_token', $refreshedToken->getToken());
        $this->assertEquals('mock_refresh_access_token', $refreshedToken->getRefreshToken());
        $this->assertEquals('1459490400', $refreshedToken->getExpires());
    }

    public function testGetRefreshTokenGitlab()
    {
        $options = [
            "expires"=>1459490400,
            "access_token"=>"mock_access_token",
            "scope"=>"email,profile,openid",
            "token_type"=>"bearer",
            "refresh_token"=>"mock_refresh_access_token"
        ];
        $response = MockeryService::mock('Psr\Http\Message\ResponseInterface');
        $response->shouldReceive('getBody')
            ->andReturn(json_encode($options));
        $response->shouldReceive('getHeader')
            ->andReturn(['content-type' => 'json']);
        $client = MockeryService::mock('BO\Zmsclient\Psr7\ClientInterface');
        $client->shouldReceive('readResponse')
            ->times(1)
            ->andReturn($response);
        
        $this->providerName = 'gitlab';
        $this->setUp();
        $this->provider->setHttpClient($client);

        $this->providerService->setProvider($this->provider);
        $refreshedToken = $this->providerService->getRefreshedAccessToken($options);

        $this->assertEquals('mock_access_token', $refreshedToken->getToken());
        $this->assertEquals('mock_refresh_access_token', $refreshedToken->getRefreshToken());
        $this->assertEquals('1459490400', $refreshedToken->getExpires());
    }

    public function testGetRefreshTokenFailed()
    {
        $this->expectException('InvalidArgumentException');
        $this->expectExceptionMessage('Required option not passed: "access_token"');

        $options = [
            "expires"=>1459490400,
            "scope"=>"email,profile,openid",
            "token_type"=>"bearer",
            "refresh_token"=>"mock_refresh_access_token"
        ];
        $client = MockeryService::mock('BO\Zmsclient\Psr7\ClientInterface');
        $this->provider->setHttpClient($client);

        $this->providerService->setProvider($this->provider);
        $refreshedToken = $this->providerService->getRefreshedAccessToken($options);

        $this->assertEquals('mock_access_token', $refreshedToken->getToken());
        $this->assertEquals('mock_refresh_access_token', $refreshedToken->getRefreshToken());
        $this->assertEquals('1459490400', $refreshedToken->getExpires());
    }

    public function testGetAccessTokenBodyFailed()
    {
        $this->expectException('UnexpectedValueException');

        $response = MockeryService::mock('Psr\Http\Message\ResponseInterface');
        $response->shouldReceive('getBody')
            ->andReturn('["access_token" => "mock_access_token", "scope" => "email", "token_type" => "bearer"]');
        $response->shouldReceive('getHeader')
            ->andReturn(['content-type' => 'json']);

        $client = MockeryService::mock('BO\Zmsclient\Psr7\ClientInterface');
        $client->shouldReceive('readResponse')
            ->times(1)
            ->andReturn($response);
        $this->provider->setHttpClient($client);
        $this->provider->getAccessToken('authorization_code', ['code' => 'mock_authorization_code']);
    }

    public function testUserData()
    {
        $preferred_username = 'unittest.user';
        $email = 'user@unittest.de';

        $postResponse = MockeryService::mock('Psr\Http\Message\ResponseInterface');
        $postResponse->shouldReceive('getBody')
            ->andReturn(
                'access_token=mock_access_token&expires=3600&refresh_token=mock_refresh_token&otherKey={1234}'
            );
        $postResponse->shouldReceive('getHeader')
            ->andReturn(['content-type' => 'application/x-www-form-urlencoded']);

        $userResponse = MockeryService::mock('Psr\Http\Message\ResponseInterface');
        $userResponse->shouldReceive('getBody')
            ->andReturn('{
                "email": "'.$email.'", 
                "email_verified": true, 
                "preferred_username": "'.$preferred_username.'"
            }');
        $userResponse->shouldReceive('getHeader')
            ->andReturn(['content-type' => 'json']);

        $client = MockeryService::mock('BO\Zmsclient\Psr7\ClientInterface');
        $client->shouldReceive('readResponse')
            ->times(2)
            ->andReturn($postResponse, $userResponse);
        $this->provider->setHttpClient($client);
        $this->providerService->setProvider($this->provider);

        $token = $this->providerService->getAccessToken();
        $user = $this->providerService->getResourceOwnerData($token, ['onlyVerifiedMail' => true]);

        $this->assertEquals($preferred_username.'@'.$this->providerName, $user['username']);
        $this->assertEquals($email, $user['email']);
    }

    public function testUserDataGitlab()
    {
        $preferred_username = 'unittest.user';
        $email = 'user@unittest.de';

        $postResponse = MockeryService::mock('Psr\Http\Message\ResponseInterface');
        $postResponse->shouldReceive('getBody')
            ->andReturn(
                'access_token=mock_access_token&expires=3600&refresh_token=mock_refresh_token&otherKey={1234}'
            );
        $postResponse->shouldReceive('getHeader')
            ->andReturn(['content-type' => 'application/x-www-form-urlencoded']);

        $userResponse = MockeryService::mock('Psr\Http\Message\ResponseInterface');
        $userResponse->shouldReceive('getBody')
            ->andReturn('{
                "email": "'.$email.'", 
                "email_verified": true, 
                "preferred_username": "'.$preferred_username.'"
            }');
        $userResponse->shouldReceive('getHeader')
            ->andReturn(['content-type' => 'json']);

        $client = MockeryService::mock('BO\Zmsclient\Psr7\ClientInterface');
        $client->shouldReceive('readResponse')
            ->times(2)
            ->andReturn($postResponse, $userResponse);
        
        $this->providerName = 'gitlab';
        $this->setUp();
        $this->provider->setHttpClient($client);
        $this->providerService->setProvider($this->provider);

        $token = $this->providerService->getAccessToken();
        $user = $this->providerService->getResourceOwnerData($token, ['onlyVerifiedMail' => true]);

        $this->assertEquals($preferred_username.'@'.$this->providerName, $user['username']);
        $this->assertEquals($email, $user['email']);
    }

    public function testUserDataWithoutVerfifiedMail()
    {
        $this->expectException('BO\Zmsadmin\Exception\OAuth\OAuthPreconditionFailed');
        $this->expectExceptionMessage('A verfied email address is mandatory for login over open id connect');
        $preferred_username = 'unittest.user';
        $email = 'user@unittest.de';

        $postResponse = MockeryService::mock('Psr\Http\Message\ResponseInterface');
        $postResponse->shouldReceive('getBody')
            ->andReturn(
                'access_token=mock_access_token&expires=3600&refresh_token=mock_refresh_token&otherKey={1234}'
            );
        $postResponse->shouldReceive('getHeader')
            ->andReturn(['content-type' => 'application/x-www-form-urlencoded']);

        $userResponse = MockeryService::mock('Psr\Http\Message\ResponseInterface');
        $userResponse->shouldReceive('getBody')
            ->andReturn('{
                "email": "'.$email.'", 
                "email_verified": false, 
                "preferred_username": "'.$preferred_username.'"
            }');
        $userResponse->shouldReceive('getHeader')
            ->andReturn(['content-type' => 'json']);

        $client = MockeryService::mock('BO\Zmsclient\Psr7\ClientInterface');
        $client->shouldReceive('readResponse')
            ->times(2)
            ->andReturn($postResponse, $userResponse);
        $this->provider->setHttpClient($client);
        $this->providerService->setProvider($this->provider);

        $token = $this->providerService->getAccessToken();
        $this->providerService->getResourceOwnerData($token, ['onlyVerifiedMail' => true]);
    }

    public function testUserDataWithVerifiedMail()
    {
        $preferred_username = 'unittest.user';
        $email = null;

        $postResponse = MockeryService::mock('Psr\Http\Message\ResponseInterface');
        $postResponse->shouldReceive('getBody')
            ->andReturn(
                'access_token=mock_access_token&expires=3600&refresh_token=mock_refresh_token&otherKey={1234}'
            );
        $postResponse->shouldReceive('getHeader')
            ->andReturn(['content-type' => 'application/x-www-form-urlencoded']);

        $userResponse = MockeryService::mock('Psr\Http\Message\ResponseInterface');
        $userResponse->shouldReceive('getBody')
            ->andReturn('{
                "email": "'.$email.'", 
                "email_verified": false, 
                "preferred_username": "'.$preferred_username.'"
            }');
        $userResponse->shouldReceive('getHeader')
            ->andReturn(['content-type' => 'json']);

        $client = MockeryService::mock('BO\Zmsclient\Psr7\ClientInterface');
        $client->shouldReceive('readResponse')
            ->times(2)
            ->andReturn($postResponse, $userResponse);
        $this->provider->setHttpClient($client);
        $this->providerService->setProvider($this->provider);

        $token = $this->providerService->getAccessToken();
        $user = $this->providerService->getResourceOwnerData($token, []);

        $this->assertEquals($preferred_username.'@keycloak', $user['username']);
        $this->assertEquals($email, $user['email']);
    }

    protected function getProviderService($providerName = 'keycloak')
    {
        $oauthService = new OAuthService();
        $providerOptions = $oauthService->readProviderListFromJson();
        return $providerOptions[$providerName];
    }
}
