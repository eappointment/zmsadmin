<?php

namespace BO\Zmsadmin\Tests;

use BO\Zmsadmin\Helper\Config;
use BO\Zmsadmin\Helper\GraphDefaults;
use BO\Zmsclient\Auth;

use BO\Zmsadmin\Middleware\OAuth\OAuthService;

use Mockery as MockeryService;

class OidcRefreshTest extends Base
{
    protected $provider;

    protected $providerService;

    protected $providerName = 'keycloak';

    protected $arguments = [];

    protected $parameters = ['state' => '61ef5df7ec8a839d06f1d388bb6f48df'];

    protected $classname = "Pickup";

    protected $tokenOptions = [
        "expires"=>1459490400,
        "access_token"=>"mock_access_token",
        "scope"=>"email,profile,openid",
        "token_type"=>"bearer",
        "refresh_token"=>"mock_refresh_access_token"
    ];

    public function setUp(): void
    {
        $this->providerService = $this->getProviderService($this->providerName);
        $this->provider = $this->providerService->getProvider();
    }

    public function tearDown(): void
    {
        MockeryService::close();
        parent::tearDown();
    }

    public function testRendering()
    {
        Auth::setKey($this->parameters['state']);
        $this->setApiCalls(
            [
                [
                    'function' => 'readGetResult',
                    'url' => '/workstation/',
                    'parameters' => ['resolveReferences' => 1],
                    'response' => $this->readFixture("GET_Workstation_Resolved2.json")
                ],
                'GET_config' => [
                    'function' => 'readGetResult',
                    'url' => '/config/',
                    'parameters' => [
                        'gql' => GraphDefaults::getConfig(),
                        'withDescriptions' => false
                    ],
                    'xtoken' => \App::CONFIG_SECURE_TOKEN,
                    'response' => $this->readFixture("GET_config.json"),
                    'optional'   => true,
                ],
            ]
        );

        $response = $this->render($this->arguments, [
            '__middleware' => [
                'class' => 'BO\Zmsadmin\Middleware\OAuth\OAuthRefreshService'
            ]
        ], []);
        $this->assertStringContainsString('Abholer verwalten', (string)$response->getBody());
    }

    public function testGetRefreshToken()
    {
        $refreshedToken = $this->getMockRefreshToken();
        $this->assertEquals('mock_access_token', $refreshedToken->getToken());
        $this->assertEquals('mock_refresh_access_token', $refreshedToken->getRefreshToken());
        $this->assertEquals('1459490400', $refreshedToken->getExpires());
    }

    protected function getMockRefreshToken()
    {
        $response = MockeryService::mock('Psr\Http\Message\ResponseInterface');
        $response->shouldReceive('getBody')
            ->andReturn(json_encode($this->tokenOptions));
        $response->shouldReceive('getHeader')
            ->andReturn(['content-type' => 'json']);
        $client = MockeryService::mock('BO\Zmsclient\Psr7\ClientInterface');
        $client->shouldReceive('readResponse')
            ->times(1)
            ->andReturn($response);
        $this->provider->setHttpClient($client);

        $this->providerService->setProvider($this->provider);
        return $this->providerService->getRefreshedAccessToken($this->tokenOptions);
    }

    protected function getProviderService($providerName = 'keycloak')
    {
        $oauthService = new OAuthService();
        $providerOptions = $oauthService->readProviderListFromJson();
        return $providerOptions[$providerName];
    }
}
