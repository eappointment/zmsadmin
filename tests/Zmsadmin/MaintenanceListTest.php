<?php
/**
 * @copyright BerlinOnline GmbH
 **/

declare(strict_types=1);

namespace BO\Zmsadmin\Tests;

class MaintenanceListTest extends Base
{
    protected $classname = "MaintenanceList";

    public function testRendering()
    {
        $this->setApiCalls(
            [
                [
                    'function' => 'readGetResult',
                    'url' => '/workstation/',
                    'parameters' => ['resolveReferences' => 1],
                    'response' => $this->readFixture("GET_Workstation_Resolved1.json")
                ],
                [
                    'function' => 'readGetResult',
                    'url' => '/maintenanceschedule/',
                    'response' => $this->readFixture("GET_maintenanceschedule_list.json")
                ],
                'GET_maintenanceschedule_scheduled' => [
                    'function'   => 'readGetResult',
                    'url' => '/maintenanceschedule/1000000000/',
                    'response' => $this->readFixture("GET_maintenanceschedule_1000000000.json"),
                    'optional'   => true,
                ],
            ]
        );

        $response = $this->render([], []);

        self::assertStringContainsString('23 20 * * *', (string) $response->getBody());
        self::assertStringContainsString('2016-04-01, 15:15', (string) $response->getBody());
    }
}
