<?php

declare(strict_types=1);

namespace BO\Zmsadmin\Tests;

class MaintenanceScheduleEditTest extends Base
{
    protected $classname = "MaintenanceScheduleEdit";

    public function testRendering()
    {
        $data = [
            'id'               => 1000000000,
            'isActive'         => false,
            'isRepetitive'     => true,
            'timeString'       => '0 23 * * *',
            'duration'         => 90,
            'leadTime'         => 30,
            'area'             => 'zms',
            'announcement'     => 'Wait for it',
            'documentBody'     => 'There it is',
            'save'             => 'save',
        ];

        $this->setApiCalls([
            [
                'function' => 'readGetResult',
                'url' => '/workstation/',
                'parameters' => ['resolveReferences' => 1],
                'response' => $this->readFixture("GET_Workstation_Resolved1.json")
            ],
            [
                'function' => 'readGetResult',
                'url' => '/maintenanceschedule/1000000000/',
                'response' => $this->readFixture("GET_maintenanceschedule_1000000000.json")
            ],
            [
                'function' => 'readPostResult',
                'url' => '/maintenanceschedule/1000000000/',
                'response' => $this->readFixture("GET_maintenanceschedule_1000000000.json")
            ],
        ]);

        $response = $this->render(['id' => 1000000000], $data, [], 'POST');

        $this->assertRedirect($response, '/maintenance/?success=maintenance_schedule_edit');
        $this->assertEquals(302, $response->getStatusCode());
    }
}
