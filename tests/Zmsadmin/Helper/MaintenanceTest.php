<?php
/**
 * @copyright BerlinOnline GmbH
 **/

declare(strict_types=1);

namespace BO\Zmsadmin\Tests\Helper;

use BO\Slim\Request;
use BO\Slim\Response;
use BO\Zmsadmin\BaseController;
use BO\Zmsadmin\Helper\Config;
use BO\Zmsadmin\Helper\Maintenance;
use BO\Zmsadmin\Workstation;
use BO\Zmsclient\Exception as RequestException;
use Fig\Http\Message\StatusCodeInterface;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use Prophecy\PhpUnit\ProphecyTrait;
use Slim\Psr7\Factory\StreamFactory;
use Slim\Psr7\Headers;
use Slim\Psr7\Stream;
use Slim\Psr7\Uri;

class MaintenanceTest extends TestCase
{
    use ProphecyTrait;

    public function setUp(): void
    {
        // static values (container) keep their values between tests when phpunit runs the tests non-isolated
        \App::$slim->getContainer()->set('maintenance', new \BO\Zmsadmin\Helper\Maintenance());
        \App::$slim->getContainer()->set('zmsadmin.services.config', new Config());
        \App::$now = new \DateTimeImmutable('2016-04-01 11:55:00', new \DateTimeZone('Europe/Berlin'));
    }

    public function tearDown(): void
    {
        if (\App::$slim->getContainer()->has('zmsadmin.services.config')) {
            \App::$slim->getContainer()->offsetUnset('zmsadmin.services.config');
        }
    }

    public function testGetStartIsNull(): void
    {
        $clientProphecy = $this->prophesize('\BO\Zmsclient\Http');
        $clientProphecy
            ->readGetResult('/maintenanceschedule/active/', Argument::any(), Argument::any())
            ->willThrow(new RequestException('API-Call failed', new Response(404)));

        $responseBody = (new StreamFactory())->createStream();
        $responseBody->write(file_get_contents(dirname(__FILE__) . '/../fixtures/GET_config.json'));

        $clientProphecy->readGetResult('/config/', Argument::any(), Argument::any())
            ->willReturn(new \BO\Zmsclient\Result(
                new Response(
                    StatusCodeInterface::STATUS_OK,
                    null,
                    $responseBody
                )
            ));

        \App::$http  = $clientProphecy->reveal();
        $maintenance = new Maintenance();

        self::assertNull($maintenance->getStart());
    }

    public function testGetStartWhenScheduled(): void
    {
        $fixturesFolder = dirname(__FILE__) . '/../fixtures/';
        $clientProphecy = $this->prophesize('\BO\Zmsclient\Http');
        $clientProphecy
            ->readGetResult('/maintenanceschedule/active/', Argument::any(), Argument::any())
            ->willThrow(new RequestException('API-Call failed', new Response(404)));

        $configBody = (new StreamFactory())->createStream();
        $configBody->write(file_get_contents($fixturesFolder . 'GET_config_with_maintenance.json'));

        $clientProphecy->readGetResult('/config/', Argument::any(), Argument::any())
            ->willReturn(new \BO\Zmsclient\Result(
                new Response(
                    StatusCodeInterface::STATUS_OK,
                    null,
                    $configBody
                )
            ));

        $scheduleBody = (new StreamFactory())->createStream();
        $scheduleBody->write(file_get_contents($fixturesFolder . 'GET_maintenanceschedule_999999999.json'));

        $clientProphecy->readGetResult('/maintenanceschedule/999999999/', Argument::any(), Argument::any())
            ->willReturn(new \BO\Zmsclient\Result(
                new Response(
                    StatusCodeInterface::STATUS_OK,
                    null,
                    $scheduleBody
                )
            ));

        \App::$http  = $clientProphecy->reveal();
        $maintenance = new Maintenance();

        self::assertSame('12:34:56', $maintenance->getStart(\App::$now)->format('H:i:s'));
    }

    public function testGetStartWhenActive(): void
    {
        $fixturesFolder = dirname(__FILE__) . '/../fixtures/';
        $scheduleBody   = (new StreamFactory())->createStream();
        $scheduleBody->write(file_get_contents($fixturesFolder . 'GET_maintenanceschedule_999999999.json'));

        $clientProphecy = $this->prophesize('\BO\Zmsclient\Http');
        $clientProphecy->readGetResult('/maintenanceschedule/active/')
            ->willReturn(new \BO\Zmsclient\Result(
                new Response(
                    StatusCodeInterface::STATUS_OK,
                    null,
                    $scheduleBody
                )
            ));

        \App::$http  = $clientProphecy->reveal();
        $maintenance = new Maintenance();

        self::assertSame('15:15:00', $maintenance->getStart(\App::$now)->format('H:i:s'));
    }

    public function testGetMaintenanceData(): void
    {
        $fixturesFolder = dirname(__FILE__) . '/../fixtures/';
        $scheduleBody   = (new StreamFactory())->createStream();
        $scheduleBody->write(file_get_contents($fixturesFolder . 'GET_maintenanceschedule_999999999.json'));

        $clientProphecy = $this->prophesize('\BO\Zmsclient\Http');
        $clientProphecy->readGetResult('/maintenanceschedule/active/')
            ->willReturn(new \BO\Zmsclient\Result(
                new Response(
                    StatusCodeInterface::STATUS_OK,
                    null,
                    $scheduleBody
                )
            ));

        \App::$http  = $clientProphecy->reveal();
        $maintenance = new Maintenance();

        $data = $maintenance->getMaintenanceData();

        self::assertSame('17:15', $data['endDateTime']->format('H:i'));
    }

    public function testIsBlockedRequest(): void
    {
        $uri = new Uri('http', 'localhost', 80, '/maintenance/schedule/999999999/');
        $request = new Request('GET', $uri, new Headers([]), [], [], new Stream(fopen('php://temp', 'wb+')));

        self::assertFalse(Maintenance::isBlockedRequest($request));

        $uri = new Uri('http', 'localhost', 80, '/workstation/');
        $request = new Request('GET', $uri, new Headers([]), [], [], new Stream(fopen('php://temp', 'wb+')));

        self::assertTrue(Maintenance::isBlockedRequest($request));
    }

    public function testRenderMaintenanceResponse(): void
    {
        $fixturesFolder = dirname(__FILE__) . '/../fixtures/';

        $workstationBody   = (new StreamFactory())->createStream();
        $workstationBody->write(file_get_contents($fixturesFolder . 'GET_workstation_basic.json'));

        $clientProphecy = $this->prophesize('\BO\Zmsclient\Http');
        $clientProphecy->readGetResult('/workstation/')
            ->willReturn(new \BO\Zmsclient\Result(
                new Response(
                    StatusCodeInterface::STATUS_OK,
                    null,
                    $workstationBody
                )
            ));

        $scheduleBody   = (new StreamFactory())->createStream();
        $scheduleBody->write(file_get_contents($fixturesFolder . 'GET_maintenanceschedule_999999999.json'));

        $clientProphecy->readGetResult('/maintenanceschedule/active/')
            ->willReturn(new \BO\Zmsclient\Result(
                new Response(
                    StatusCodeInterface::STATUS_OK,
                    null,
                    $scheduleBody
                )
            ));

        \App::$http  = $clientProphecy->reveal();
        $maintenance = new Maintenance();
        $maintenance->getStart();

        $uri = new Uri('http', 'localhost', 80, '/workstation/');
        $request = new Request('GET', $uri, new Headers([]), [], [], new Stream(fopen('php://temp', 'wb+')));
        BaseController::prepareRequest($request);

        $response = $maintenance->renderMaintenanceResponse(new Response(), $request);

        self::assertStringContainsString('Die Terminverwaltung wird momentan gewartet.', (string) $response->getBody());

        $controller = new Workstation(\App::$slim->getContainer());
        $response   = $controller($request, new Response(), []);

        self::assertStringContainsString('Die Terminverwaltung wird momentan gewartet.', (string) $response->getBody());
    }
}
