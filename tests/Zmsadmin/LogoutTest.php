<?php

namespace BO\Zmsadmin\Tests;

use BO\Zmsclient\Auth;

class LogoutTest extends Base
{
    protected $arguments = [];

    protected $parameters = ['state' => 'c273715018c869f40eb747aa33bad4ed'];

    protected $classname = "Logout";

    public function testRendering()
    {
        $this->setApiCalls(
            [
                [
                    'function' => 'readGetResult',
                    'url' => '/workstation/',
                    'parameters' => ['resolveReferences' => 0],
                    'response' => $this->readFixture("GET_Workstation_Resolved2.json")
                ],
                [
                    'function' => 'readDeleteResult',
                    'url' => '/workstation/login/testadmin/',
                    'response' => $this->readFixture("GET_Workstation_Resolved2.json")
                ]
            ]
        );

        $response = $this->render($this->arguments, [], []);
        $this->assertRedirect($response, '/?logout=1');
    }

    public function testLogoutWithProvider()
    {
        Auth::setKey('5d8570559755256652b00231ef7faaa7');
        $this->setApiCalls(
            [
                [
                    'function' => 'readGetResult',
                    'url' => '/workstation/',
                    'parameters' => ['resolveReferences' => 0],
                    'response' => $this->readFixture("GET_Workstation_Resolved2.json")
                ],
                [
                    'function' => 'readDeleteResult',
                    'url' => '/workstation/login/testadmin/',
                    'response' => $this->readFixture("GET_Workstation_Resolved2.json")
                ],
                [
                    'function' => 'readGetResult',
                    'url' => '/session/zmsadmin/5d8570559755256652b00231ef7faaa7/',
                    'parameters' => ['sync' => 0, 'oidc' => 1],
                    'response' => $this->readFixture("GET_oidc_session.json")
                ],
                [
                    'function' => 'readDeleteResult',
                    'url' => '/session/zmsadmin/5d8570559755256652b00231ef7faaa7/',
                    'response' => $this->readFixture("GET_oidc_session.json")
                ]
            ]
        );
        $response = $this->render($this->arguments, [
            'oauthProvider' => 'keycloak',
            '__middleware' => [
                'class' => 'BO\Zmsadmin\Middleware\OAuth\OAuthLogoutService'
            ]
        ], []);
        $this->assertStringContainsString(
            '/?logout=1',
            $response->getHeaderLine('location')
        );
    }

    public function testLogoutWithoutProvider()
    {
        $this->setApiCalls(
            [
                [
                    'function' => 'readGetResult',
                    'url' => '/workstation/',
                    'parameters' => ['resolveReferences' => 0],
                    'response' => $this->readFixture("GET_Workstation_Resolved2.json")
                ],
                [
                    'function' => 'readDeleteResult',
                    'url' => '/workstation/login/testadmin/',
                    'response' => $this->readFixture("GET_Workstation_Resolved2.json")
                ]
            ]
        );
        Auth::setKey('zmsclient');
        $response = $this->render($this->arguments, [
            '__middleware' => [
                'class' => 'BO\Zmsadmin\Middleware\OAuth\OAuthLogoutService'
            ]
        ], []);

        $this->assertStringContainsString(
            '/?logout=1',
            $response->getHeaderLine('location')
        );
    }

    public function testLogoutFailed()
    {
        $this->expectException('\BO\Zmsclient\Exception');
        $exception = new \BO\Zmsclient\Exception();
        $exception->template = '';

        $this->setApiCalls(
            [
                [
                    'function' => 'readGetResult',
                    'url' => '/workstation/',
                    'parameters' => ['resolveReferences' => 0],
                    'exception' => $exception
                ],
            ]
        );

        $this->render($this->arguments, [], []);
    }
}
