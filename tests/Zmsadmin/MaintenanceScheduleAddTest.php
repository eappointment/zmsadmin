<?php
/**
 * @copyright BerlinOnline GmbH
 **/

declare(strict_types=1);

namespace BO\Zmsadmin\Tests;

use BO\Slim\Response;
use BO\Zmsclient\Exception as ClientException;
use Fig\Http\Message\StatusCodeInterface;

class MaintenanceScheduleAddTest extends Base
{
    protected $classname = "MaintenanceScheduleAdd";

    public function testRendering()
    {
        $data = [
            'isActive'         => false,
            'isRepetitive'     => true,
            'timeString'       => '0 23 * * *',
            'duration'         => 90,
            'leadTime'         => 30,
            'area'             => 'zms',
            'announcement'     => 'Wait for it',
            'documentBody'     => 'There it is',
            'save'             => 'save',
        ];

        $this->setApiCalls([
            [
                'function' => 'readGetResult',
                'url' => '/workstation/',
                'parameters' => ['resolveReferences' => 1],
                'response' => $this->readFixture("GET_Workstation_Resolved1.json")
            ],
            [
                'function' => 'readPostResult',
                'url' => '/maintenanceschedule/',
                'response' => $this->readFixture("GET_maintenanceschedule_1000000000.json")
            ],
        ]);

        $response = $this->render([], $data, [], 'POST');

        $this->assertRedirect($response, '/maintenance/?success=maintenance_schedule_add');
        $this->assertEquals(302, $response->getStatusCode());
    }

    public function testPreview(): void
    {
        $data = [
            'isActive'         => false,
            'isRepetitive'     => true,
            'timeString'       => '0 23 * * *',
            'duration'         => 90,
            'leadTime'         => 30,
            'area'             => 'zms',
            'announcement'     => 'This is the announcement',
            'documentBody'     => 'This is the maintenance advice',
            'save'             => 'save',
            'preview'          => 1
        ];

        $this->setApiCalls([
            [
                'function' => 'readGetResult',
                'url' => '/workstation/',
                'parameters' => ['resolveReferences' => 1],
                'response' => $this->readFixture("GET_Workstation_Resolved1.json")
            ],
        ]);

        $response = $this->render([], $data, [], 'POST');

        self::assertSame(200, $response->getStatusCode());
        self::assertStringContainsString('the announcement', (string)$response->getBody());
        self::assertStringContainsString('the maintenance advice', (string)$response->getBody());
    }

    public function testInputFailure(): void
    {
        $data = [
            'isActive'         => false,
            'isRepetitive'     => true,
            'timeString'       => '2001-01-01 12:34',
            'duration'         => 90,
            'leadTime'         => 30,
            'area'             => 'zms',
            'announcement'     => 'Wait for it',
            'documentBody'     => 'There it is',
            'save'             => 'save',
        ];

        $response  = new Response(StatusCodeInterface::STATUS_NOT_ACCEPTABLE);
        $exception = new ClientException('', $response);
        $exception->template = 'BO\Zmsentities\Exception\SchemaValidation';
        $exception->data['timeString']['messages'] = [
            'Der angegebene Wert entspricht nicht der Crontab Syntax'
        ];

        $this->setApiCalls([
            [
                'function' => 'readGetResult',
                'url' => '/workstation/',
                'parameters' => ['resolveReferences' => 1],
                'response' => $this->readFixture("GET_Workstation_Resolved1.json")
            ],
            [
                'function' => 'readPostResult',
                'url' => '/maintenanceschedule/',
                'exception' => $exception,
            ],
        ]);

        $response = $this->render([], $data, [], 'POST');

        $this->assertEquals(200, $response->getStatusCode());
        self::assertStringContainsString('Wartungstermin hinzufügen', (string)$response->getBody());
        self::assertStringContainsString('entspricht nicht der Crontab Syntax', (string)$response->getBody());
    }
}
