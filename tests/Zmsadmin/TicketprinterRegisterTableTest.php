<?php

declare(strict_types=1);

namespace BO\Zmsadmin\Tests;

class TicketprinterRegisterTableTest extends Base
{
    protected $classname = "TicketprinterRegisterTable";

    public function testRendering()
    {
        $this->setApiCalls(
            [
                [
                    'function'   => 'readGetResult',
                    'url'        => '/workstation/',
                    'parameters' => ['resolveReferences' => 1],
                    'response'   => $this->readFixture("GET_Workstation_Resolved1.json")
                ],
                [
                    'function'   => 'readGetResult',
                    'url'        => '/applicationregister/',
                    'parameters' => ['type' => 'ticketprinter', 'scopeIds' => '141,142'],
                    'response'   => $this->readFixture("GET_data_empty.json")
                ],
            ]
        );

        $response = $this->render([], ['type' => 'ticketprinter', 'scopeIds' => '141,142']);

        self::assertEmpty((string)$response->getBody());
    }
}
