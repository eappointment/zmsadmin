<?php

namespace BO\Zmsadmin\Tests;

class UrlParameterSigningTest extends Base
{
    protected $arguments = [];

    protected $parameters = [
        "section" => "webcalldisplay",
        "parameters" => [
            "collections" => [
                "scopelist" => "141",
                "clusterlist" => "109"
            ]
        ]
    ];

    protected $classname = "UrlParameterSigning";

    public function testRendering()
    {
        $this->setApiCalls(
            [
                [
                    'function' => 'readGetResult',
                    'url' => '/workstation/',
                    'parameters' => ['resolveReferences' => 0],
                    'response' => $this->readFixture("GET_workstation_basic.json")
                ],
                [
                    'function' => 'readGetResult',
                    'url' => '/scope/141/organisation/',
                    'parameters' => ['resolveReferences' => 3],
                    'response' => $this->readFixture("GET_organisation_71_resolved3.json")
                ]
            ]
        );
        $data = ['__body' => json_encode($this->parameters)];
        $hash = \BO\Slim\Helper::hashQueryParameters($this->parameters['section'], $this->parameters['parameters'], ['collections', 'queue']);
        $response = $this->render($this->arguments, $data, [], 'POST');
        $this->assertStringContainsString('BNb9bGm1x1xLsMJn', (string)$response->getBody());
        $this->assertTrue($hash == 'BNb9bGm1x1xLsMJn');
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testBadRequestMissingSection()
    {
        $this->expectException('BO\Zmsadmin\Exception\BadRequest');
        $this->expectExceptionMessage('The request body was empty or not having the right format.');
        $this->setApiCalls(
            [
            ]
        );
        unset($this->parameters['section']);
        $data = ['__body' => json_encode($this->parameters)];
        $this->render($this->arguments, $data, [], 'POST');
    }

    public function testBadRequestMissingParameters()
    {
        $this->expectException('BO\Zmsadmin\Exception\BadRequest');
        $this->expectExceptionMessage('The request body was empty or not having the right format.');
        $this->setApiCalls(
            [
            ]
        );
        unset($this->parameters['parameters']);
        $data = ['__body' => json_encode($this->parameters)];
        $this->render($this->arguments, $data, [], 'POST');
    }

    public function testScopeUserAccessFailed()
    {
        $this->expectException('BO\Zmsentities\Exception\UserAccountAccessRightsFailed');
        $this->expectExceptionMessage('Level of user rights are low, access rejected');
        $this->setApiCalls(
            [
                [
                    'function' => 'readGetResult',
                    'url' => '/workstation/',
                    'parameters' => ['resolveReferences' => 0],
                    'response' => $this->readFixture("GET_workstation_basic.json")
                ],
                [
                    'function' => 'readGetResult',
                    'url' => '/scope/141/organisation/',
                    'parameters' => ['resolveReferences' => 3],
                    'response' => $this->readFixture("GET_organisation_71_resolved3.json")
                ]
            ]
        );
        $this->parameters['parameters']['collections']['scopelist'] = "141,133";
        $data = ['__body' => json_encode($this->parameters)];
        $this->render($this->arguments, $data, [], 'POST');
    }

    public function testClusterUserAccessFailed()
    {
        $this->expectException('BO\Zmsentities\Exception\UserAccountAccessRightsFailed');
        $this->expectExceptionMessage('Level of user rights are low, access rejected');
        $this->setApiCalls(
            [
                [
                    'function' => 'readGetResult',
                    'url' => '/workstation/',
                    'parameters' => ['resolveReferences' => 0],
                    'response' => $this->readFixture("GET_workstation_basic.json")
                ],
                [
                    'function' => 'readGetResult',
                    'url' => '/scope/141/organisation/',
                    'parameters' => ['resolveReferences' => 3],
                    'response' => $this->readFixture("GET_organisation_71_resolved3.json")
                ]
            ]
        );
        $this->parameters['parameters']['collections']['clusterlist'] = "109,193";
        $data = ['__body' => json_encode($this->parameters)];
        $this->render($this->arguments, $data, [], 'POST');
    }
}
