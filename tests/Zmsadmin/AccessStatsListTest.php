<?php

declare(strict_types=1);

namespace BO\Zmsadmin\Tests;

class AccessStatsListTest extends Base
{
    protected $classname = "AccessStatsList";

    public function testRendering()
    {
        $this->setApiCalls(
            [
                [
                    'function' => 'readGetResult',
                    'url' => '/workstation/',
                    'parameters' => ['resolveReferences' => 1],
                    'response' => $this->readFixture("GET_Workstation_Resolved1.json")
                ],
                [
                    'function' => 'readGetResult',
                    'url' => '/accessstats/',
                    'response' => $this->readFixture("GET_accessStats_list.json")
                ],
            ]
        );

        $response = $this->render([], []);

        self::assertStringContainsString('accessStats.json', (string) $response->getBody());
        self::assertStringContainsString('user', (string) $response->getBody());
        self::assertStringContainsString('citizen', (string) $response->getBody());
    }
}
