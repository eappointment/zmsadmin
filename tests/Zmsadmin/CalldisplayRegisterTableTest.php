<?php

declare(strict_types=1);

namespace BO\Zmsadmin\Tests;

use Fig\Http\Message\StatusCodeInterface;

class CalldisplayRegisterTableTest extends Base
{
    protected $classname = "CalldisplayRegisterTable";

    public function testRendering()
    {
        $this->setApiCalls(
            [
                [
                    'function'   => 'readGetResult',
                    'url'        => '/workstation/',
                    'parameters' => ['resolveReferences' => 1],
                    'response'   => $this->readFixture("GET_Workstation_Resolved1.json")
                ],
                [
                    'function'   => 'readGetResult',
                    'url'        => '/applicationregister/',
                    'parameters' => ['type' => 'calldisplay', 'scopeIds' => '141,142'],
                    'response'   => $this->readFixture("GET_applicationRegister_list.json")
                ],
                [
                    'function' => 'readGetResult',
                    'url'      => '/scope/',
                    'response' => $this->readFixture("GET_scope_list.json")
                ],
            ]
        );

        $response = $this->render([], ['scopeIds' => '141,142']);

        self::assertStringContainsString('list-calldisplay', (string)$response->getBody());
        self::assertStringContainsString('Parameter', (string)$response->getBody());
    }

    public function testScopeIdsMissed(): void
    {
        $this->setApiCalls([
            [
                'function'   => 'readGetResult',
                'url'        => '/workstation/',
                'parameters' => ['resolveReferences' => 1],
                'response'   => $this->readFixture("GET_Workstation_Resolved1.json")
            ],
        ]);

        $response = $this->render([], ['scopeIds' => '']);

        self::assertSame(StatusCodeInterface::STATUS_BAD_REQUEST, $response->getStatusCode());
        self::assertStringContainsString('wrong', $response->getReasonPhrase());
    }
}
