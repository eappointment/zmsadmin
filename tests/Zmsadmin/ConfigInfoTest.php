<?php

namespace BO\Zmsadmin\Tests;

use BO\Zmsadmin\Helper\GraphDefaults;

class ConfigInfoTest extends Base
{
    protected $arguments = [];

    protected $parameters = [];

    protected $classname = "ConfigInfo";

    public function testRendering()
    {
        $this->setApiCalls(
            [
                [
                    'function' => 'readGetResult',
                    'url' => '/workstation/',
                    'parameters' => ['resolveReferences' => 1],
                    'response' => $this->readFixture("GET_Workstation_Resolved2.json")
                ],
                [
                    'function' => 'readGetResult',
                    'url' => '/config/',
                    'parameters' => [
                        'withDescriptions' => true,
                        'gql' => GraphDefaults::getConfig(),
                    ],
                    'xtoken' => \App::CONFIG_SECURE_TOKEN,
                    'response' => $this->readFixture("GET_config.json")
                ]
            ]
        );
        $response = $this->render($this->arguments, $this->parameters, []);
        $this->assertStringContainsString('Konfiguration System', (string)$response->getBody());
        $this->assertStringContainsString("Sie sind in Kürze an der Reihe.", (string)$response->getBody());
        $this->assertStringContainsString(
            'Here you set the openid connect service provider',
            (string)$response->getBody()
        );
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testUpdate()
    {
        $this->setApiCalls(
            [
                [
                    'function' => 'readGetResult',
                    'url' => '/workstation/',
                    'parameters' => ['resolveReferences' => 1],
                    'response' => $this->readFixture("GET_Workstation_Resolved2.json")
                ],
                [
                    'function' => 'readGetResult',
                    'url' => '/config/',
                    'parameters' => [
                        'withDescriptions' => true,
                        'gql' => GraphDefaults::getConfig(),
                    ],
                    'xtoken' => \App::CONFIG_SECURE_TOKEN,
                    'response' => $this->readFixture("GET_config.json")
                ],
                [
                    'function' => 'readPostResult',
                    'url' => '/config/',
                    'response' => $this->readFixture("GET_config.json")
                ]
            ]
        );
        $response = $this->render($this->arguments, [
            'key' => 'cron',
            'property' => 'sendMailReminder',
            'value' => 'dev,stage',
            'description' => 'UnitTest description',
            'save' => 'save'
        ], [], 'POST');
        $this->assertRedirect($response, '/config/?success=config_saved');
        $this->assertEquals(302, $response->getStatusCode());
    }

    public function testTemplatePath()
    {
        $path = \BO\Zmsadmin\Helper\TemplateFinder::getTemplatePath();
        $this->assertStringContainsString('src/Zmsadmin/Helper/../../../templates', $path);
    }
}
