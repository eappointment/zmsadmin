<?php

namespace BO\Zmsadmin\Tests;

class HealthcheckTest extends Base
{
    protected $arguments = [];

    protected $parameters = [];

    protected $classname = "Healthcheck";

    public function testRendering()
    {
        $this->setApiCalls(
            [
                [
                    'function' => 'readGetResult',
                    'url' => '/status/',
                    'parameters' => ['includeProcessStats' => 0, 'callingFrom' => 'zmsadmin'],
                    'response' => $this->readFixture("GET_status_200.json")
                ]
            ]
        );
        $response = $this->render($this->arguments, $this->parameters, []);
        $this->assertStringNotContainsString('OK', (string)$response->getBody());
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testWarning()
    {
        $this->setApiCalls(
            [
                [
                    'function' => 'readGetResult',
                    'url' => '/status/',
                    'parameters' => ['includeProcessStats' => 0, 'callingFrom' => 'zmsadmin'],
                    'response' => $this->readFixture("GET_status.json")
                ]
            ]
        );
        $response = $this->render($this->arguments, $this->parameters, []);
        $this->assertStringContainsString('WARN', (string)$response->getBody());
        $this->assertEquals(200, $response->getStatusCode());
    }
}
