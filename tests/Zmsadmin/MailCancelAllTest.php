<?php

namespace BO\Zmsadmin\Tests;

class MailCancelAllTest extends Base
{
    protected $arguments = [];

    protected $parameters = [
        'scope' => 141,
        'date' => "2016-04-01",
        'subject' => 'This is a test subject',
        'message' => 'This is a test body message',
        'availabilities_accepted' => 1,
        'cancel_accepted' => 1,
    ];

    protected $classname = "MailCancelAll";

    public function testRendering()
    {
        $this->setApiCalls(
            [
                [
                    'function' => 'readGetResult',
                    'url' => '/workstation/',
                    'parameters' => ['resolveReferences' => 1],
                    'response' => $this->readFixture("GET_Workstation_Resolved2.json")
                ],
            ]
        );
        $response = $this->render($this->arguments, $this->parameters, []);
        $this->assertStringContainsString('Alle Termine absagen mit frei formulierbarer E-Mail', (string)$response->getBody());
        $this->assertStringContainsString('action="/mail/cancel/all/"', (string)$response->getBody());
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testPost()
    {
        $this->setApiCalls(
            [
                [
                    'function' => 'readGetResult',
                    'url' => '/workstation/',
                    'parameters' => ['resolveReferences' => 1],
                    'response' => $this->readFixture("GET_Workstation_Resolved2.json")
                ],
                [
                    'function' => 'readGetResult',
                    'url' => '/scope/141/department/',
                    'response' => $this->readFixture("GET_department_74.json")
                ],
                [
                    'function' => 'readGetResult',
                    'url' => '/scope/141/process/2016-04-01/',
                    'parameters' => [
                        'resolveReferences' => 2,
                        'gql' => \BO\Zmsadmin\Helper\GraphDefaults::getProcess()
                    ],
                    'response' => $this->readFixture("GET_processList_141_20160401_with_mailfrom.json")
                ],
                [
                    'function' => 'readPostResult',
                    'url' => '/mails/',
                    'parameters' => ['count' => 1],
                    'response' => $this->readFixture("POST_mail.json")
                ],
                [
                    'function' => 'readDeleteResult',
                    'url' => '/process/82252/',
                    'parameters' => ['initiator' => 'admin-cancel-all'],
                    'response' => $this->readFixture("GET_process_82252_12a2.json")
                ],
            ]
        );
        $response = $this->render($this->arguments, $this->parameters, [], 'POST');
        $this->assertRedirect($response, '/mail/cancel/all/?success=mail_canceled_all');
        $this->assertEquals(302, $response->getStatusCode());
    }

    public function testFormFailed()
    {
        $this->setApiCalls(
            [
                [
                    'function' => 'readGetResult',
                    'url' => '/workstation/',
                    'parameters' => ['resolveReferences' => 1],
                    'response' => $this->readFixture("GET_Workstation_Resolved2.json")
                ],
            ]
        );
        $response = $this->render($this->arguments, [
            'scope' => 141,
            'date' => "2016-04-01",
            'subject' => '',
            'message' => '',
            'availabilites_accepted' => 0,
            'cancel_accepted' => 0,
        ], [], 'POST');
        $this->assertStringContainsString('has-error', (string)$response->getBody());
        $this->assertStringContainsString('Es muss eine aussagekräftige Nachricht eingegeben werden', (string)$response->getBody());
        $this->assertStringContainsString('Es muss ein aussagekräftiger Betreff eingegeben werden', (string)$response->getBody());
        $this->assertStringContainsString('Fehler: Diese Bedingung muss zwingend ausgeführt werden', (string)$response->getBody());
        $this->assertStringContainsString('Fehler: Dieser Bedingung muss zwingend zugestimmt werden', (string)$response->getBody());

        $this->assertEquals(400, $response->getStatusCode());
    }

    public function testMailFromMissing()
    {
        $this->expectException('BO\Zmsadmin\Exception\MailFromMissing');
        $this->setApiCalls(
            [
                [
                    'function' => 'readGetResult',
                    'url' => '/workstation/',
                    'parameters' => ['resolveReferences' => 1],
                    'response' => $this->readFixture("GET_Workstation_Resolved2.json")
                ],
                [
                    'function' => 'readGetResult',
                    'url' => '/scope/141/department/',
                    'response' => $this->readFixture("GET_department_74.json")
                ],
                [
                    'function' => 'readGetResult',
                    'url' => '/scope/141/process/2016-04-01/',
                    'parameters' => [
                        'resolveReferences' => 2,
                        'gql' => \BO\Zmsadmin\Helper\GraphDefaults::getProcess()
                    ],
                    'response' => $this->readFixture("GET_processList_141_20160401.json")
                ],
            ]
        );
        $response = $this->render($this->arguments, $this->parameters, [], 'POST');
        $this->assertRedirect($response, '/mail/?success=mail_sent');
        $this->assertEquals(302, $response->getStatusCode());
    }

    public function testBadRequest()
    {
        $this->expectException('BO\Zmsadmin\Exception\BadRequest');
        $this->setApiCalls(
            [
                [
                    'function' => 'readGetResult',
                    'url' => '/workstation/',
                    'parameters' => ['resolveReferences' => 1],
                    'response' => $this->readFixture("GET_Workstation_Resolved2.json")
                ],
            ]
        );
        $this->render($this->arguments, [], [], 'POST');
    }
}
