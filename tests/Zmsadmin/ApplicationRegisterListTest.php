<?php

declare(strict_types=1);

namespace BO\Zmsadmin\Tests;

class ApplicationRegisterListTest extends Base
{
    protected $classname = "ApplicationRegisterList";

    public function testRendering()
    {
        $this->setApiCalls(
            [
                [
                    'function' => 'readGetResult',
                    'url' => '/workstation/',
                    'parameters' => ['resolveReferences' => 1],
                    'response' => $this->readFixture("GET_Workstation_Resolved2.json")
                ],
                [
                    'function' => 'readGetResult',
                    'url' => '/applicationregister/',
                    'response' => $this->readFixture("GET_applicationRegister_list.json")
                ],
                [
                    'function' => 'readGetResult',
                    'url' => '/scope/',
                    'parameters' => ['resolveReferences' => 0],
                    'response' => $this->readFixture("GET_scope_list.json")
                ],
            ]
        );

        $response = $this->render([], []);

        self::assertStringContainsString('Aktive Anwendungen', (string) $response->getBody());
        self::assertStringContainsString('calldisplay', (string) $response->getBody());
    }
}
